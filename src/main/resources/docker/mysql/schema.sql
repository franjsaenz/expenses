-- MySQL dump 10.13  Distrib 5.7.37, for osx10.17 (x86_64)
--
-- Host: 127.0.0.1    Database: expenses
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Current Database: `expenses`
--
USE `expenses`;

--
-- Table structure for table `budget`
--

DROP TABLE IF EXISTS `budget`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `budget`
(
    `id`          bigint    NOT NULL AUTO_INCREMENT,
    `created`     timestamp NULL DEFAULT NULL,
    `deleted`     timestamp NULL DEFAULT NULL,
    `updated`     timestamp NULL DEFAULT NULL,
    `from_date`   timestamp NULL DEFAULT NULL,
    `name`        text,
    `to_date`     timestamp NULL DEFAULT NULL,
    `owner_id`    bigint         DEFAULT NULL,
    `currency_id` bigint         DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FKenhjknmladswx6pg3ln5vg0y4` (`owner_id`),
    KEY `FKfi2aqc26e35xw6jxa8a78lutn` (`currency_id`),
    CONSTRAINT `FKenhjknmladswx6pg3ln5vg0y4` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`),
    CONSTRAINT `FKfi2aqc26e35xw6jxa8a78lutn` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budget`
--

LOCK TABLES `budget` WRITE;
/*!40000 ALTER TABLE `budget`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `budget`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `budget_item`
--

DROP TABLE IF EXISTS `budget_item`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `budget_item`
(
    `id`          bigint    NOT NULL AUTO_INCREMENT,
    `created`     timestamp NULL DEFAULT NULL,
    `deleted`     timestamp NULL DEFAULT NULL,
    `updated`     timestamp NULL DEFAULT NULL,
    `amount`      decimal(19, 2) DEFAULT NULL,
    `owner_id`    bigint         DEFAULT NULL,
    `budget_id`   bigint         DEFAULT NULL,
    `category_id` bigint         DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FKo4t7863a1rhc1s7ptnga3ln66` (`owner_id`),
    KEY `FKmjr3de30xmfjwg3ej5y6qh1qx` (`budget_id`),
    KEY `FKr14t3owetuy8pjhwux3sfgdl8` (`category_id`),
    CONSTRAINT `FKmjr3de30xmfjwg3ej5y6qh1qx` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`),
    CONSTRAINT `FKo4t7863a1rhc1s7ptnga3ln66` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`),
    CONSTRAINT `FKr14t3owetuy8pjhwux3sfgdl8` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budget_item`
--

LOCK TABLES `budget_item` WRITE;
/*!40000 ALTER TABLE `budget_item`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `budget_item`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `business`
--

DROP TABLE IF EXISTS `business`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business`
(
    `id`       bigint    NOT NULL AUTO_INCREMENT,
    `created`  timestamp NULL DEFAULT NULL,
    `deleted`  timestamp NULL DEFAULT NULL,
    `updated`  timestamp NULL DEFAULT NULL,
    `name`     text,
    `owner_id` bigint         DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FKb4maq7rjfml4gm6ydg4djjyb6` (`owner_id`),
    CONSTRAINT `FKb4maq7rjfml4gm6ydg4djjyb6` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business`
--

LOCK TABLES `business` WRITE;
/*!40000 ALTER TABLE `business`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `business`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category`
(
    `id`          bigint    NOT NULL AUTO_INCREMENT,
    `created`     timestamp NULL DEFAULT NULL,
    `deleted`     timestamp NULL DEFAULT NULL,
    `updated`     timestamp NULL DEFAULT NULL,
    `description` text,
    `name`        text,
    `owner_id`    bigint         DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FK7j5dlrhthv9y4pee8ftyd4df2` (`owner_id`),
    CONSTRAINT `FK7j5dlrhthv9y4pee8ftyd4df2` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `category`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency`
(
    `id`         bigint    NOT NULL AUTO_INCREMENT,
    `created`    timestamp NULL DEFAULT NULL,
    `deleted`    timestamp NULL DEFAULT NULL,
    `updated`    timestamp NULL DEFAULT NULL,
    `code`       text,
    `identifier` text,
    `name`       text,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `currency`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detail`
--

DROP TABLE IF EXISTS `detail`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detail`
(
    `id`               bigint    NOT NULL AUTO_INCREMENT,
    `created`          timestamp NULL DEFAULT NULL,
    `deleted`          timestamp NULL DEFAULT NULL,
    `updated`          timestamp NULL DEFAULT NULL,
    `amount`           decimal(19, 2) DEFAULT NULL,
    `description`      text,
    `discount`         decimal(19, 2) DEFAULT NULL,
    `subtotal`         decimal(19, 2) DEFAULT NULL,
    `without_discount` bit(1)    NOT NULL,
    `owner_id`         bigint         DEFAULT NULL,
    `category_id`      bigint         DEFAULT NULL,
    `expense_id`       bigint         DEFAULT NULL,
    `product_id`       bigint         DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FKlqckj38519ratod042yrc8qgf` (`owner_id`),
    KEY `FKqpnjhssa5jkyq049c4537rais` (`category_id`),
    KEY `FKbp9knhesud815ab5s629gc2ok` (`expense_id`),
    KEY `FKnogkshb9tv6mtc9em7efogdni` (`product_id`),
    CONSTRAINT `FKbp9knhesud815ab5s629gc2ok` FOREIGN KEY (`expense_id`) REFERENCES `expense` (`id`),
    CONSTRAINT `FKlqckj38519ratod042yrc8qgf` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`),
    CONSTRAINT `FKnogkshb9tv6mtc9em7efogdni` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
    CONSTRAINT `FKqpnjhssa5jkyq049c4537rais` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detail`
--

LOCK TABLES `detail` WRITE;
/*!40000 ALTER TABLE `detail`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `detail`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discount`
--

DROP TABLE IF EXISTS `discount`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discount`
(
    `id`          bigint    NOT NULL AUTO_INCREMENT,
    `created`     timestamp NULL DEFAULT NULL,
    `deleted`     timestamp NULL DEFAULT NULL,
    `updated`     timestamp NULL DEFAULT NULL,
    `amount`      decimal(19, 2) DEFAULT NULL,
    `description` text,
    `owner_id`    bigint         DEFAULT NULL,
    `expense_id`  bigint         DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FKh7uic0wrcuyvcv055vk1pbduh` (`owner_id`),
    KEY `FKr9cic4gq3u91dl4ho0ujkwkt1` (`expense_id`),
    CONSTRAINT `FKh7uic0wrcuyvcv055vk1pbduh` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`),
    CONSTRAINT `FKr9cic4gq3u91dl4ho0ujkwkt1` FOREIGN KEY (`expense_id`) REFERENCES `expense` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discount`
--

LOCK TABLES `discount` WRITE;
/*!40000 ALTER TABLE `discount`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `discount`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exchange_rate`
--

DROP TABLE IF EXISTS `exchange_rate`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exchange_rate`
(
    `id`         bigint    NOT NULL AUTO_INCREMENT,
    `created`    timestamp NULL DEFAULT NULL,
    `deleted`    timestamp NULL DEFAULT NULL,
    `updated`    timestamp NULL DEFAULT NULL,
    `multiplier` decimal(19, 2) DEFAULT NULL,
    `registered` timestamp NULL DEFAULT NULL,
    `owner_id`   bigint         DEFAULT NULL,
    `from_id`    bigint         DEFAULT NULL,
    `to_id`      bigint         DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FKatg99861d7un2e4ex7ttsaa60` (`owner_id`),
    KEY `FKj04hbjutpwqh32sgxvqibj0x2` (`from_id`),
    KEY `FKete00mr32tm1q2obgrwc7kbkx` (`to_id`),
    CONSTRAINT `FKatg99861d7un2e4ex7ttsaa60` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`),
    CONSTRAINT `FKete00mr32tm1q2obgrwc7kbkx` FOREIGN KEY (`to_id`) REFERENCES `currency` (`id`),
    CONSTRAINT `FKj04hbjutpwqh32sgxvqibj0x2` FOREIGN KEY (`from_id`) REFERENCES `currency` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exchange_rate`
--

LOCK TABLES `exchange_rate` WRITE;
/*!40000 ALTER TABLE `exchange_rate`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `exchange_rate`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `expense`
--

DROP TABLE IF EXISTS `expense`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `expense`
(
    `id`               bigint    NOT NULL AUTO_INCREMENT,
    `created`          timestamp NULL DEFAULT NULL,
    `deleted`          timestamp NULL DEFAULT NULL,
    `updated`          timestamp NULL DEFAULT NULL,
    `registered`       timestamp NULL DEFAULT NULL,
    `status`           varchar(255)   DEFAULT NULL,
    `owner_id`         bigint         DEFAULT NULL,
    `business_id`      bigint         DEFAULT NULL,
    `currency_id`      bigint         DEFAULT NULL,
    `exchange_rate_id` bigint         DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FKobq3je21kexsm043wg1jqv0v` (`owner_id`),
    KEY `FK2owlygqu4lwk9ny55gridroa8` (`business_id`),
    KEY `FKfkdvf1ivs3l31iy5tf5ap3kul` (`currency_id`),
    KEY `FKjarp3dtoygk4ijrwhh0bni596` (`exchange_rate_id`),
    CONSTRAINT `FK2owlygqu4lwk9ny55gridroa8` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`),
    CONSTRAINT `FKfkdvf1ivs3l31iy5tf5ap3kul` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`),
    CONSTRAINT `FKjarp3dtoygk4ijrwhh0bni596` FOREIGN KEY (`exchange_rate_id`) REFERENCES `exchange_rate` (`id`),
    CONSTRAINT `FKobq3je21kexsm043wg1jqv0v` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `expense`
--

LOCK TABLES `expense` WRITE;
/*!40000 ALTER TABLE `expense`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `expense`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product`
(
    `id`          bigint    NOT NULL AUTO_INCREMENT,
    `created`     timestamp NULL DEFAULT NULL,
    `deleted`     timestamp NULL DEFAULT NULL,
    `updated`     timestamp NULL DEFAULT NULL,
    `name`        text,
    `owner_id`    bigint         DEFAULT NULL,
    `category_id` bigint         DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `FKn3le0a5l0wmq4gs20rn3ty36x` (`owner_id`),
    KEY `FK1mtsbur82frn64de7balymq9s` (`category_id`),
    CONSTRAINT `FK1mtsbur82frn64de7balymq9s` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
    CONSTRAINT `FKn3le0a5l0wmq4gs20rn3ty36x` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `product`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user`
(
    `id`          bigint    NOT NULL AUTO_INCREMENT,
    `created`     timestamp NULL DEFAULT NULL,
    `deleted`     timestamp NULL DEFAULT NULL,
    `updated`     timestamp NULL DEFAULT NULL,
    `date_format` text,
    `email`       text,
    `password`    text,
    `role`        varchar(255)   DEFAULT NULL,
    `zone_id`     text,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2022-09-03 19:56:15
