INSERT INTO user (id, created, email, password, role, zone_id, date_format)
VALUES (1, NOW(), 'super@expenses.com', 'fZZh2NuuCFxCzB5GumpLfsDeExGt/ncnV3Wnt4HAuk/dTqgABRocqlJwfiOkALkV', 'SUPER', null, null),
       (2, NOW(), 'demo@expenses.com', 'e7vHiM30+IT+DnXU9cwnA7ZrQfpADRztPAJ3Kx2JkUmQbuCM5/N0XVITgMU9Z0/d', 'USER', 'UTC-03:00', 'DEFAULT_24');

INSERT INTO currency (id, created, code, identifier, name)
VALUES (1, NOW(), 'USD', '$', 'Dolar Estadounidense'),
       (2, NOW(), 'EUR', '€', 'Euro'),
       (3, NOW(), 'CHF', 'Fr.', 'Franco Suizo'),
       (4, NOW(), 'GBP', '£', 'Libra Esterlina'),
       (5, NOW(), 'ARS', '$', 'Peso Argentino'),
       (6, NOW(), 'CLP', '$', 'Peso Chileno'),
       (7, NOW(), 'UYU', '$', 'Peso Uruguayo');

INSERT INTO business (id, created, name, owner_id)
VALUES (1, NOW(), 'Supermercado', 2);

INSERT INTO category (id, created, description, name, owner_id)
VALUES (1, NOW(), '', 'Alimentos', 2),
       (2, NOW(), '', 'Bebidas', 2);

INSERT INTO product (id, created, name, owner_id, category_id)
VALUES (1, NOW(), 'Hamburguesas', 2, 1),
       (2, NOW(), 'Milanesas', 2, 1),
       (3, NOW(), 'Fideos', 2, 1),
       (4, NOW(), 'Arroz', 2, 1),
       (5, NOW(), 'Jugo Instantáneo', 2, 2),
       (6, NOW(), 'Agua en Botella', 2, 2),
       (7, NOW(), 'Refresco Cola', 2, 2);

INSERT INTO expense (id, created, registered, status, owner_id, business_id, currency_id, exchange_rate_id)
VALUES (1, NOW(), NOW(), 'CLOSED', 2, 1, 7, null);

INSERT INTO detail (id, created, amount, description, discount, subtotal, without_discount, owner_id, category_id, expense_id, product_id)
VALUES (1, NOW(), 50.00, null, 0.50, 49.50, false, 2, 1, 1, 4),
       (2, NOW(), 420.00, null, 4.20, 415.80, false, 2, 1, 1, 1),
       (3, NOW(), 120.00, null, 1.20, 118.80, false, 2, 2, 1, 7);

INSERT INTO discount (id, created, amount, description, owner_id, expense_id)
VALUES (1, NOW(), 5.90, 'Descuento 10%', 2, 1);