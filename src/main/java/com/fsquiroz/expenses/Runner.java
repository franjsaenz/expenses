package com.fsquiroz.expenses;

import com.fsquiroz.expenses.entity.db.Role;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
@Profile("!test")
@Slf4j
public class Runner implements ApplicationRunner {

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    public Runner(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(ApplicationArguments args) {
        checkSuper();
    }

    private void checkSuper() {
        long count = userRepository.count();
        if (count > 0) {
            return;
        }
        log.info("Creating first super user 'super@expenses.com'");
        User u = new User();
        u.setEmail("super@expenses.com");
        u.setCreated(Instant.now());
        u.setRole(Role.SUPER);
        u.setPassword(passwordEncoder.encode("123456"));
        userRepository.save(u);
    }

}
