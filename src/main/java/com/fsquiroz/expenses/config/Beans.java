package com.fsquiroz.expenses.config;

import com.fasterxml.classmate.TypeResolver;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fsquiroz.expenses.entity.json.MStatus;
import com.fsquiroz.expenses.security.SecurityCheck;
import com.fsquiroz.expenses.service.TimeUtils;
import com.google.common.base.Predicates;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;

@Configuration
@Profile("!test")
@EnableSwagger2
public class Beans {

    private Instant startUp = Instant.now();

    @Autowired(required = false)
    private BuildProperties buildProperties;

    @Autowired
    private ObjectMapper mapper;

    private MStatus status;

    private BigDecimalSerializer bigDecimalSerializer;

    private Docket docket;

    private OperationBuilderPlugin operationBuilderPlugin;

    private PasswordEncoder encoder;

    private ServletRegistrationBean servletRegistrationBean;

    @Autowired
    private LoggerDispatcherServlet loggerDispatcherServlet;

    @Autowired
    private SecurityCheck securityCheck;

    @Bean
    public MStatus status() {
        if (status == null) {
            String title;
            Instant build;
            String version;
            if (buildProperties != null) {
                title = buildProperties.getName();
                build = buildProperties.getTime();
                version = buildProperties.getVersion();
            } else {
                title = "Expenses";
                build = startUp;
                version = "DEVELOPMENT";
            }
            status = new MStatus(title, version, build, startUp, "Up");
        }
        return status;
    }

    @Bean
    public BigDecimalSerializer bigDecimalSerializer() {
        if (bigDecimalSerializer == null) {
            bigDecimalSerializer = new BigDecimalSerializer();
        }
        SimpleModule sm = new SimpleModule();
        sm.addSerializer(BigDecimal.class, bigDecimalSerializer);
        sm.addSerializer(Instant.class, new InstantSerializer(securityCheck));
        sm.addDeserializer(Instant.class, new InstantDeserializer());
        mapper.registerModule(sm);
        return bigDecimalSerializer;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        if (encoder == null) {
            encoder = new PasswordEncoder() {
                private StrongPasswordEncryptor spe = new StrongPasswordEncryptor();

                @Override
                public String encode(CharSequence rawPassword) {
                    return spe.encryptPassword(rawPassword.toString());
                }

                @Override
                public boolean matches(CharSequence rawPassword, String encodedPassword) {
                    return spe.checkPassword(rawPassword.toString(), encodedPassword);
                }
            };
        }
        return encoder;
    }

    @Bean
    public ServletRegistrationBean dispatcherRegistration() {
        if (this.servletRegistrationBean == null) {
            this.servletRegistrationBean = new ServletRegistrationBean(loggerDispatcherServlet);
        }
        return this.servletRegistrationBean;
    }

    /**
     * Because of the new Actuator implementation in Spring Boot 2, all actuator endpoints are now dynamically mapped
     * to a single handler method: {@link org.springframework.boot.actuate.endpoint.web.servlet.AbstractWebMvcEndpointHandlerMapping.OperationHandler#handle(javax.servlet.http.HttpServletRequest, java.util.Map)}
     * <p>
     * This causes 2 issues:
     * - Because the handler method has an @RequestBody annotated 'body' parameter, this parameter appears in all actuator
     * endpoints as body parameter, even for GET and HEAD requests (which cannot have a request body). These endpoints
     * cannot be executed from the Swagger UI page.
     * - If an Actuator endpoint contains path parameters, these are not available as input fields on the Swagger UI page,
     * because no @PathParam annotated arguments are present on the handler method.
     * <p>
     * The Swagger OperationBuilderPlugin below fixes these issues in a somewhat dirty, but effective way.
     * <p>
     *
     * @author Riccardo Lippolis (https://github.com/rlippolis/spring-boot2-actuator-swagger)
     */
    @Bean
    public OperationBuilderPlugin operationBuilderPluginForCorrectingActuatorEndpoints(final TypeResolver typeResolver) {
        if (operationBuilderPlugin == null) {
            operationBuilderPlugin = new OperationBuilderPluginForCorrectingActuatorEndpoints(typeResolver);
        }
        return operationBuilderPlugin;
    }

    @Bean
    @SuppressWarnings("unchecked")
    public Docket api() {
        if (docket == null) {
            MStatus status = status();
            docket = new Docket(DocumentationType.SWAGGER_2)
                    .select()
                    .apis(Predicates.or(
                            Predicates.and(
                                    RequestHandlerSelectors.withClassAnnotation(RestController.class),
                                    RequestHandlerSelectors.basePackage("com.fsquiroz.expenses.controller")
                            ),
                            RequestHandlerSelectors.basePackage("org.springframework.boot.actuate")
                    ))
                    .build()
                    .apiInfo(
                            new ApiInfo(
                                    status.getName(),
                                    String.format("Built on %s - Started up on %s", TimeUtils.standard(status.getBuild()), TimeUtils.standard(status.getStartUp())),
                                    status.getVersion(),
                                    null,
                                    new Contact("fsQuiroz", "https://fsquiroz.com/", "dev@fsquiroz.com"),
                                    null,
                                    null,
                                    Collections.EMPTY_LIST
                            )
                    )
                    .securitySchemes(Arrays.asList(authorizationBearer()))
                    .securityContexts(Arrays.asList(securityContext()));
        }
        return docket;
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(
                        Arrays.asList(new SecurityReference(
                                "apiKey",
                                new AuthorizationScope[]{new AuthorizationScope("global", "Global access")}
                        ))
                )
                .forPaths(
                        Predicates.or(
                                PathSelectors.ant("/actuator/**"),
                                PathSelectors.ant("/users/**"),
                                PathSelectors.ant("/categories/**"),
                                PathSelectors.ant("/products/**"),
                                PathSelectors.ant("/businesses/**"),
                                PathSelectors.ant("/currencies/**"),
                                PathSelectors.ant("/exchangeRates/**"),
                                PathSelectors.ant("/expenses/**"),
                                PathSelectors.ant("/stats/**")
                        )
                ).build();
    }

    private ApiKey authorizationBearer() {
        return new ApiKey("apiKey", "Authorization", "header");
    }

}
