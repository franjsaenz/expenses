package com.fsquiroz.expenses.config;

import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.security.SecurityCheck;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerExecutionChain;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class LoggerDispatcherServlet extends DispatcherServlet {

    private SecurityCheck securityCheck;

    public LoggerDispatcherServlet(SecurityCheck securityCheck) {
        this.securityCheck = securityCheck;
    }

    @Override
    protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long start = System.currentTimeMillis();
        HandlerExecutionChain handler = getHandler(request);
        try {
            super.doDispatch(request, response);
        } finally {
            if (handler.toString().contains("fsquiroz")) {
                long end = System.currentTimeMillis();
                debug(request, response, handler, end - start);
            }
        }
    }

    private void debug(HttpServletRequest request,
                       HttpServletResponse response,
                       HandlerExecutionChain handler,
                       long elapsed) {
        if (log.isInfoEnabled()) {
            User u;
            try {
                u = securityCheck.getAuthentication();
            } catch (Exception e) {
                u = null;
            }
            String user;
            String role;
            if (u != null) {
                user = u.getEmail();
            } else {
                user = "ANONYMOUS";
            }
            if (u != null && u.getRole() != null) {
                role = u.getRole().getName();
            } else {
                role = "ANONYMOUS";
            }
            log.info("\n"
                            + "HTTP Request:\n"
                            + "\turl: {}\n"
                            + "\tmethod: {}\n"
                            + "\tclient: {}\n"
                            + "\tuserId: {}\n"
                            + "\tuserEmail: {}\n"
                            + "\tuserRole: {}\n"
                            + "Handler:\n"
                            + "\tmethod: {}\n"
                            + "\ttook: {}ms\n"
                            + "HTTP Response:\n"
                            + "\tstatus: {}\n",
                    request.getRequestURL(),
                    request.getMethod(),
                    request.getRemoteAddr(),
                    u != null ? u.getId() : "ANONYMOUS",
                    user,
                    role,
                    handler.toString(),
                    elapsed,
                    response.getStatus());
        }
    }

}
