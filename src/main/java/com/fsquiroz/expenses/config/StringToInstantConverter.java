package com.fsquiroz.expenses.config;

import com.fsquiroz.expenses.service.TimeUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class StringToInstantConverter implements Converter<String, Instant> {

    @Override
    public Instant convert(String s) {
        return TimeUtils.standard(s);
    }

}
