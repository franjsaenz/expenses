package com.fsquiroz.expenses.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fsquiroz.expenses.security.SecurityCheck;
import com.fsquiroz.expenses.service.TimeUtils;

import java.io.IOException;
import java.time.Instant;

public class InstantSerializer extends StdSerializer<Instant> {

    private SecurityCheck securityCheck;

    public InstantSerializer(SecurityCheck securityCheck) {
        super(Instant.class);
        this.securityCheck = securityCheck;
    }

    @Override
    public void serialize(Instant value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        String s = TimeUtils.standard(value);
        gen.writeString(s);
    }

}
