package com.fsquiroz.expenses.exception;

import com.fsquiroz.expenses.entity.db.*;
import com.fsquiroz.expenses.service.TimeUtils;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends AppException {

    private BadRequestException(ErrorCode errorCode, Map<String, Object> meta, String message) {
        super(errorCode, meta, message);
    }

    public static BadRequestException byMissingParam(String param) {
        Assert.hasText(param, "'param' must not be empty");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("param", param);
        return new BadRequestException(ErrorCode.BR_MISSING_PARAM, meta, "Missing param");
    }

    public static BadRequestException byInvalidId() {
        return new BadRequestException(ErrorCode.BR_INVALID_ID_VALUE, null, "Invalid id value");
    }

    public static BadRequestException byEditingDeleted(Entity entity) {
        Map<String, Object> meta = ofEntity(entity);
        meta.put("deleted", entity.getDeleted());
        return new BadRequestException(ErrorCode.BR_EDIT_DELETED, meta, "Can not edit a deleted entity");
    }

    public static BadRequestException byDeletingDeleted(Entity entity) {
        Map<String, Object> meta = ofEntity(entity);
        meta.put("deleted", entity.getDeleted());
        return new BadRequestException(ErrorCode.BR_DELETE_DELETED, meta, "Entity already deleted");
    }

    public static BadRequestException byUsingDeleted(Entity deleted, Entity into) {
        Map<String, Object> d = ofEntity(deleted);
        d.put("deleted", deleted.getDeleted());
        Map<String, Object> i = ofEntity(into);
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("deleted", d);
        meta.put("useInto", i);
        return new BadRequestException(ErrorCode.BR_USING_DELETED, meta, "Can not use deleted entity");
    }

    public static BadRequestException byMissmatchExpense(Detail detail, Expense expense) {
        Map<String, Object> d = ofEntity(detail);
        d.put("expenseId", detail.getExpense() != null ? detail.getExpense().getId() : null);
        Map<String, Object> e = ofEntity(expense);
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("detail", d);
        meta.put("expense", e);
        return new BadRequestException(ErrorCode.BR_MISMATCH_EXPENSE, meta, "Can not edit detail under different expense");
    }

    public static BadRequestException byMismatchBudget(BudgetItem item, Budget budget) {
        Map<String, Object> bi = ofEntity(item);
        bi.put("budgetId", item.getBudget() != null ? item.getBudget().getId() : null);
        Map<String, Object> b = ofEntity(budget);
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("budgetItem", bi);
        meta.put("budget", b);
        return new BadRequestException(ErrorCode.BR_MISMATCH_BUDGET, meta, "Can not edit detail budget item different budget");
    }

    public static BadRequestException byExistingBudgetItem(Budget budget, Category category) {
        Map<String, Object> b = ofEntity(budget);
        Map<String, Object> c = ofEntity(category);
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("budget", b);
        meta.put("category", c);
        return new BadRequestException(ErrorCode.BR_EXISTING_BUDGET_ITEM, meta, "Already exists a budget item for the given budget and category");
    }

    public static BadRequestException byMissmatchExpense(Discount discount, Expense expense) {
        Map<String, Object> d = ofEntity(discount);
        d.put("expenseId", discount.getExpense() != null ? discount.getExpense().getId() : null);
        Map<String, Object> e = ofEntity(expense);
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("discount", d);
        meta.put("expense", e);
        return new BadRequestException(ErrorCode.BR_MISMATCH_EXPENSE, meta, "Can not edit discount under different expense");
    }

    public static BadRequestException byNotOpenedExpense(Expense expense) {
        Map<String, Object> e = ofEntity(expense);
        e.put("status", expense.getStatus());
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("expense", e);
        return new BadRequestException(ErrorCode.BR_NOT_OPENED, meta, "Can not edit expense, is not open");
    }

    public static BadRequestException byNotClosedExpense(Expense expense) {
        Map<String, Object> e = ofEntity(expense);
        e.put("id", expense.getId());
        e.put("status", expense.getStatus());
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("originalExpense", e);
        return new BadRequestException(ErrorCode.BR_NOT_CLOSED, meta, "Can not repeat an open expense");
    }

    public static BadRequestException byExpirationBefore(Instant expiration, Instant notBefore) {
        Assert.notNull(expiration, "'expiration' must not be null");
        Assert.notNull(notBefore, "'notBefore' must not be null");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("expiration", TimeUtils.standard(expiration));
        meta.put("notBefore", TimeUtils.standard(notBefore));
        return new BadRequestException(ErrorCode.BR_EXPIRATION_BEFORE_NOT_BEFORE, meta, "'expiration' can not be before from 'notBefore'");
    }

    public static BadRequestException byFromAfterTo(Instant from, Instant to) {
        Assert.notNull(from, "'from' must not be null");
        Assert.notNull(to, "'to' must not be null");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("notBefore", TimeUtils.standard(from));
        meta.put("to", TimeUtils.standard(to));
        return new BadRequestException(ErrorCode.BR_FROM_AFTER_TO, meta, "'from' can not be after from 'to'");
    }

    public static BadRequestException byPasswordlessAccount(User user) {
        return new BadRequestException(ErrorCode.BR_CLIENT_PASSWORD_UPDATE, ofUser(user), "Can not edit password of user with role 'client'");
    }

    public static BadRequestException byPasswordlessTokenOnSuper(User user) {
        return new BadRequestException(ErrorCode.BR_PASSWORDLESS_TOKEN_ON_SUPER, ofUser(user), "Can not generate passwordless token for users with role 'super'");
    }

    public static BadRequestException byCreatingSuperUser() {
        return new BadRequestException(ErrorCode.BR_ASSIGN_SUPER, null, "Can not create user with role 'super'");
    }

    public static BadRequestException byExistingEmail(String email) {
        Assert.hasText(email, "'email' must not be empty");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("email", email);
        return new BadRequestException(ErrorCode.BR_EXISTING_EMAIL, meta, "Already exist a user with this email");
    }

    public static BadRequestException bySuspendedUser(User user) {
        Map<String, Object> u = ofUser(user);
        u.put("deleted", user.getDeleted());
        return new BadRequestException(ErrorCode.BR_SUSPENDED_ACCOUNT, u, "This user has been suspended");
    }

    public static BadRequestException byDeletingSuper() {
        return new BadRequestException(ErrorCode.BR_DELETE_SUPER, null, "Users with role 'super' can not be deleted");
    }

    public static BadRequestException byInvalidSortParam(Class clazz, String param) {
        Assert.notNull(clazz, "'clazz' can not be null");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("entity", clazz.getSimpleName());
        meta.put("param", param);
        return new BadRequestException(ErrorCode.BR_INVALID_SORT_PARAM, meta, "Invalid sort param");
    }

    public static BadRequestException byMalformedBody() {
        return new BadRequestException(ErrorCode.BR_MALFORMED_BODY, null, "Unable to parse body content");
    }

    public static BadRequestException byMalformedParam(MethodArgumentTypeMismatchException e) {
        Map<String, Object> meta = null;
        if (e != null) {
            meta = new LinkedHashMap<>();
            meta.put("param", e.getName() != null ? e.getName() : e.getPropertyName());
            meta.put("value", e.getValue());
            meta.put("requiredType", e.getRequiredType() != null ? e.getRequiredType().getSimpleName() : "UNKNOWN");
            meta.put("message", e.getMessage());
        }
        return new BadRequestException(ErrorCode.BR_MALFORMED_PARAM, meta, "Unable to parse param");
    }

    public static BadRequestException byMalformedSortingParam(String message) {
        Map<String, Object> meta = null;
        if (message != null) {
            meta = new LinkedHashMap<>();
            meta.put("param", "sort");
            meta.put("message", message);
        }
        return new BadRequestException(ErrorCode.BR_MALFORMED_PARAM, meta, "Unable to parse param");
    }

    private static Map<String, Object> ofEntity(Entity entity) {
        Assert.notNull(entity, "'entity' must not be null");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("entity", entity.getClass().getSimpleName());
        meta.put("id", entity.getId());
        return meta;
    }

    private static Map<String, Object> ofUser(User user) {
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("id", user.getId());
        meta.put("email", user.getEmail());
        meta.put("role", user.getRole());
        return meta;
    }

}
