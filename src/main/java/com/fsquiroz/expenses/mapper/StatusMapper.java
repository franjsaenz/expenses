package com.fsquiroz.expenses.mapper;

import com.fsquiroz.expenses.entity.json.MStatus;
import com.fsquiroz.expenses.security.SecurityCheck;
import org.springframework.stereotype.Service;

@Service
public class StatusMapper extends Mapper<MStatus, MStatus> {

    private final SecurityCheck securityCheck;

    public StatusMapper(SecurityCheck securityCheck) {
        this.securityCheck = securityCheck;
    }

    @Override
    public MStatus map(MStatus status) {
        try {
            securityCheck.getAuthentication();
            return status;
        } catch (Exception e) {
            MStatus s = new MStatus();
            s.setMessage(status.getMessage());
            return s;
        }
    }
}
