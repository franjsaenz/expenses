package com.fsquiroz.expenses.mapper;

public abstract class Mapper<SRC, DST> {

    public abstract DST map(SRC src);
}
