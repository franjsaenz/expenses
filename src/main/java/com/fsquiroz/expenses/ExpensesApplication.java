package com.fsquiroz.expenses;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@PropertySources({
        @PropertySource(value = "classpath:application.properties")
        ,
        @PropertySource(value = "file:${user.home}${file.separator}.expenses${file.separator}application.properties")
})
public class ExpensesApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ExpensesApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(ExpensesApplication.class, args);
    }

}
