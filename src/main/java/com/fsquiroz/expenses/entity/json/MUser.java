package com.fsquiroz.expenses.entity.json;

import com.fsquiroz.expenses.entity.db.Role;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "User")
public class MUser {

    @ApiModelProperty(example = "1")
    private Long id;

    @ApiModelProperty(hidden = true)
    private Instant created;

    @ApiModelProperty(hidden = true)
    private Instant updated;

    @ApiModelProperty(hidden = true)
    private Instant deleted;

    @ApiModelProperty(example = "USER")
    private Role role;

    @ApiModelProperty(example = "some@email.com")
    private String email;

    @ApiModelProperty(example = "some-password")
    private String password;

    @ApiModelProperty(example = "UTC")
    private String zoneId;

    @ApiModelProperty(example = "yyyy/MM/dd hh:mm:ss")
    private String dateFormat;

    @ApiModelProperty(hidden = true)
    private String token;

    @ApiModelProperty(hidden = true)
    private Instant expiration;

    @ApiModelProperty(hidden = true)
    private Instant notBefore;

}
