package com.fsquiroz.expenses.entity.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Detail")
public class MDetail {

    @ApiModelProperty(example = "1")
    private Long id;

    @ApiModelProperty(hidden = true)
    private Instant created;

    @ApiModelProperty(hidden = true)
    private Instant updated;

    @ApiModelProperty(hidden = true)
    private Instant deleted;

    @ApiModelProperty(hidden = true)
    private MProduct product;

    @ApiModelProperty(hidden = true)
    private MCategory category;

    @ApiModelProperty(example = "105.99")
    private BigDecimal amount;

    @ApiModelProperty(hidden = true)
    private BigDecimal discount;

    @ApiModelProperty(hidden = true)
    private BigDecimal subtotal;

    @ApiModelProperty(example = "360gr")
    private String description;

    @ApiModelProperty(example = "false")
    private boolean withoutDiscount;

}
