package com.fsquiroz.expenses.entity.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "BudgetReportDetail")
public class MBudgetReportDetail implements Comparable<MBudgetReportDetail> {

    @ApiModelProperty(example = "1")
    private Long id;

    @ApiModelProperty(example = "Category name")
    private String name;

    @ApiModelProperty(example = "1000.00")
    private BigDecimal expected;

    @ApiModelProperty(example = "700.00")
    private BigDecimal expended;

    @ApiModelProperty(example = "300.00")
    private BigDecimal remainedToExpend;

    @ApiModelProperty(example = "300.00")
    private BigDecimal completed;

    @Override
    public int compareTo(MBudgetReportDetail o) {
        if (o == null || o.completed == null) {
            return 1;
        } else if (completed == null) {
            return -1;
        } else {
            return o.completed.compareTo(completed);
        }
    }

}
