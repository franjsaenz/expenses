package com.fsquiroz.expenses.entity.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Budget")
public class MBudget {

    @ApiModelProperty(example = "1")
    private Long id;

    @ApiModelProperty(hidden = true)
    private Instant created;

    @ApiModelProperty(hidden = true)
    private Instant updated;

    @ApiModelProperty(hidden = true)
    private Instant deleted;

    @ApiModelProperty(hidden = true)
    private MCurrency currency;

    @ApiModelProperty(example = "2019-01-01T00:00:00.000Z")
    private Instant from;

    @ApiModelProperty(example = "2019-01-31T23:59:59.999Z")
    private Instant to;

    @ApiModelProperty(example = "Some name")
    private String name;

}
