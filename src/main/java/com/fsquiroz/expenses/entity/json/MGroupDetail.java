package com.fsquiroz.expenses.entity.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "GroupDetail")
public class MGroupDetail {

    @ApiModelProperty(example = "1")
    private Long id;

    @ApiModelProperty(example = "Some name")
    private String name;

    @ApiModelProperty(example = "2019-01-25T12:57:01.999Z")
    private Instant date;

    @ApiModelProperty(example = "150.00")
    private BigDecimal subtotal;

    @ApiModelProperty(example = "10.00")
    private BigDecimal discounts;

    @ApiModelProperty(example = "140.00")
    private BigDecimal total;

    public MGroupDetail(Long id, String name, BigDecimal subtotal, BigDecimal discounts, BigDecimal total) {
        this.id = id;
        this.name = name;
        this.subtotal = subtotal;
        this.discounts = discounts;
        this.total = total;
    }

}
