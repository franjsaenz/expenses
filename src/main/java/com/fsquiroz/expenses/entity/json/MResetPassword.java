package com.fsquiroz.expenses.entity.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "ResetPassword")
public class MResetPassword {

    @ApiModelProperty(example = "some-old-password")
    private String oldPassword;

    @ApiModelProperty(example = "some-new-password")
    private String newPassword;

}
