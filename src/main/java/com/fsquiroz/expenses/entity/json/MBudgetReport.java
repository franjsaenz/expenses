package com.fsquiroz.expenses.entity.json;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "BudgetReport")
public class MBudgetReport {

    private MBudget budget;

    private List<MBudgetReportDetail> details;

    private List<MGroupDetail> unaccounted;

    private BigDecimal totalExpected;

    private BigDecimal totalExpended;

    private BigDecimal totalRemainedToExpend;

    private BigDecimal totalUnaccounted;

    private BigDecimal totalCompleted;

}
