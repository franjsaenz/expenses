package com.fsquiroz.expenses.entity.json;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Group")
public class MGroup {

    private Instant from;

    private Instant to;

    private MCurrency currency;

    private MBusiness business;

    private MProduct product;

    private MCategory category;

    private List<MGroupDetail> details;

    private BigDecimal subtotal;

    private BigDecimal discounts;

    private BigDecimal total;

}
