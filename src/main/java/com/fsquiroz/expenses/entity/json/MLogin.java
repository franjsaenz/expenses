package com.fsquiroz.expenses.entity.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Login")
public class MLogin {

    @ApiModelProperty(example = "some@email.com")
    private String email;

    @ApiModelProperty(example = "some-password123")
    private String password;

}
