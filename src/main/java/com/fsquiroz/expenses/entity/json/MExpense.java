package com.fsquiroz.expenses.entity.json;

import com.fsquiroz.expenses.entity.db.ExpenseStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Expense")
public class MExpense {

    @ApiModelProperty(example = "1")
    private Long id;

    @ApiModelProperty(hidden = true)
    private Instant created;

    @ApiModelProperty(hidden = true)
    private Instant updated;

    @ApiModelProperty(hidden = true)
    private Instant deleted;

    @ApiModelProperty(example = "2019-01-25T12:57:01.999Z")
    private Instant registered;

    @ApiModelProperty(hidden = true)
    private MBusiness business;

    @ApiModelProperty(hidden = true)
    private MCurrency currency;

    @ApiModelProperty(hidden = true)
    private MExchangeRate exchangeRate;

    @ApiModelProperty(hidden = true)
    private List<MGroupDetail> totalByCategory;

    @ApiModelProperty(hidden = true)
    private BigDecimal subtotal;

    @ApiModelProperty(hidden = true)
    private BigDecimal discounts;

    @ApiModelProperty(hidden = true)
    private BigDecimal total;

    @ApiModelProperty(hidden = true)
    private ExpenseStatus status;

}
