package com.fsquiroz.expenses.entity.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Discount")
public class MDiscount {

    @ApiModelProperty(example = "1")
    private Long id;

    @ApiModelProperty(hidden = true)
    private Instant created;

    @ApiModelProperty(hidden = true)
    private Instant updated;

    @ApiModelProperty(hidden = true)
    private Instant deleted;

    @ApiModelProperty(example = "105.99")
    private BigDecimal amount;

    @ApiModelProperty(example = "Bank discount")
    private String description;

}
