package com.fsquiroz.expenses.entity.json;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fsquiroz.expenses.config.RatioSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "ExchangeRate")
public class MExchangeRate {

    @ApiModelProperty(example = "1")
    private Long id;

    @ApiModelProperty(hidden = true)
    private Instant created;

    @ApiModelProperty(hidden = true)
    private Instant updated;

    @ApiModelProperty(hidden = true)
    private Instant deleted;

    @ApiModelProperty(example = "2019-01-25")
    private Instant registered;

    @ApiModelProperty(hidden = true)
    private MCurrency from;

    @ApiModelProperty(hidden = true)
    private MCurrency to;

    @ApiModelProperty(example = "0.89")
    @JsonSerialize(using = RatioSerializer.class)
    private BigDecimal multiplier;

}
