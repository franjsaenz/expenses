package com.fsquiroz.expenses.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

@javax.persistence.Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Discount extends Filterable {

    @ManyToOne
    @JoinColumn
    private Expense expense;

    @Column(scale = 2)
    private BigDecimal amount;

    @Column(columnDefinition = "TEXT")
    private String description;

}
