package com.fsquiroz.expenses.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.Instant;

@javax.persistence.Entity
@Getter
@Setter
@ToString(callSuper = true)
public class ExchangeRate extends Filterable {

    @Column(columnDefinition = "TIMESTAMP")
    private Instant registered;

    @ManyToOne
    @JoinColumn
    private Currency from;

    @ManyToOne
    @JoinColumn
    private Currency to;

    @Column(scale = 8)
    private BigDecimal multiplier;

}
