package com.fsquiroz.expenses.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@javax.persistence.Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Product extends Filterable {

    @Column(columnDefinition = "TEXT")
    private String name;

    @ManyToOne
    @JoinColumn
    private Category category;

}
