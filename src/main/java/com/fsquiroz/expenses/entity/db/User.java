package com.fsquiroz.expenses.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@javax.persistence.Entity
@Getter
@Setter
@ToString(callSuper = true)
public class User extends Entity implements UserDetails {

    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(columnDefinition = "TEXT")
    private String email;

    @Column(columnDefinition = "TEXT")
    private String password;

    @Column(columnDefinition = "TEXT")
    private String zoneId;

    @Column(columnDefinition = "TEXT")
    private String dateFormat;

    @Transient
    private String token;

    @Transient
    private Instant expiration;

    @Transient
    private Instant notBefore;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        if (role != null) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
        }
        return authorities;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.deleted == null;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.deleted == null;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.deleted == null;
    }

    @Override
    public boolean isEnabled() {
        return this.deleted == null;
    }

}
