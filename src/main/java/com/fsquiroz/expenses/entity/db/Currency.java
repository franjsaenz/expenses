package com.fsquiroz.expenses.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;

@javax.persistence.Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Currency extends Entity {

    @Column(columnDefinition = "TEXT")
    private String code;

    @Column(columnDefinition = "TEXT")
    private String name;

    @Column(columnDefinition = "TEXT")
    private String identifier;

}
