package com.fsquiroz.expenses.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.Instant;

@javax.persistence.Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Expense extends Filterable {

    @Column(columnDefinition = "TIMESTAMP")
    private Instant registered;

    @ManyToOne
    @JoinColumn
    private Business business;

    @ManyToOne
    @JoinColumn
    private Currency currency;

    @ManyToOne
    @JoinColumn
    private ExchangeRate exchangeRate;

    @Enumerated(EnumType.STRING)
    private ExpenseStatus status;

}
