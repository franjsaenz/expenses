package com.fsquiroz.expenses.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.Instant;

@javax.persistence.Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Budget extends Filterable {

    @ManyToOne
    @JoinColumn
    private Currency currency;

    @Column(name = "from_date", columnDefinition = "TIMESTAMP")
    protected Instant from;

    @Column(name = "to_date", columnDefinition = "TIMESTAMP")
    protected Instant to;

    @Column(columnDefinition = "TEXT")
    private String name;

}
