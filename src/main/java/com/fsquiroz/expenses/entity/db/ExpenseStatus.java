package com.fsquiroz.expenses.entity.db;

public enum ExpenseStatus {

    OPENED("OPENED"),
    CLOSED("CLOSED"),
    IGNORED("IGNORED");

    private String name;

    ExpenseStatus(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
