package com.fsquiroz.expenses.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;

@javax.persistence.Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Category extends Filterable {

    @Column(columnDefinition = "TEXT")
    private String name;

    @Column(columnDefinition = "TEXT")
    private String description;

}
