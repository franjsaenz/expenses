package com.fsquiroz.expenses.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

@javax.persistence.Entity
@Getter
@Setter
@ToString(callSuper = true)
public class BudgetItem extends Filterable {

    @ManyToOne
    @JoinColumn
    private Budget budget;

    @ManyToOne
    @JoinColumn
    private Category category;

    @Column(scale = 2)
    private BigDecimal amount;

}
