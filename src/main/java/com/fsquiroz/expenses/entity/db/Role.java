package com.fsquiroz.expenses.entity.db;

public enum Role {

    SUPER("SUPER"),
    USER("USER"),
    CLIENT("CLIENT");

    private String name;

    Role(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
