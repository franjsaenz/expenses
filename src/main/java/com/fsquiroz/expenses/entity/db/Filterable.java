package com.fsquiroz.expenses.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@ToString(callSuper = true)
@MappedSuperclass
public abstract class Filterable extends Entity {

    @ManyToOne
    @JoinColumn
    protected User owner;

}
