package com.fsquiroz.expenses.entity.util;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class DateRange {

    private Instant from;

    private Instant to;

}
