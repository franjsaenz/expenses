package com.fsquiroz.expenses.security;

import com.fsquiroz.expenses.entity.db.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RestAuthorizationFilter extends BasicAuthenticationFilter {

    private SecurityCheck securityCheck;

    public RestAuthorizationFilter(SecurityCheck securityCheck) {
        super((Authentication authentication) -> {
            throw new UnsupportedOperationException("Not supported yet.");
        });
        this.securityCheck = securityCheck;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader("Authorization");
        if (header == null) {
            header = request.getHeader("X-Gitlab-Token");
        }
        String token = null;
        if (header != null && header.startsWith("Bearer ")) {
            token = header.substring(7);
        }
        if (token == null) {
            chain.doFilter(request, response);
            return;
        }
        UsernamePasswordAuthenticationToken auth = getAuth(token);
        if (auth == null) {
            SecurityContextHolder.clearContext();
            chain.doFilter(request, response);
            return;
        }
        SecurityContextHolder.getContext().setAuthentication(auth);
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuth(String token) {
        Assert.hasText(token, "'token' must not be empty");
        try {
            User u = securityCheck.authenticate(token);
            if (u != null) {
                return new UsernamePasswordAuthenticationToken(u, null, u.getAuthorities());
            }
        } catch (Exception ignored) {
        }
        return null;
    }

}
