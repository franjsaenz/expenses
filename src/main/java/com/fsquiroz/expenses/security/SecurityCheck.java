package com.fsquiroz.expenses.security;

import com.fsquiroz.expenses.entity.db.Filterable;
import com.fsquiroz.expenses.entity.db.Role;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.exception.UnauthorizedException;
import com.fsquiroz.expenses.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

@Component
@Slf4j
public class SecurityCheck {

    private UserRepository userRepository;

    private String jwtSecret;

    public SecurityCheck(
            UserRepository userRepository,
            @Value("${security.jwt.secret}") final String jwtSecret
    ) {
        this.userRepository = userRepository;
        this.jwtSecret = jwtSecret;
    }

    public User get(Long id) {
        User u = userRepository.findById(id).orElseThrow(() -> NotFoundException.byId(User.class, id));
        if (u.getDeleted() != null) {
            throw UnauthorizedException.byDeletedUser(u);
        }
        return u;
    }

    public User authenticate(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();
        Long id = claims.get("userId", Long.class);
        User u = get(id);
        Date expiration = claims.getExpiration();
        Date notBefore = claims.getNotBefore();
        u.setExpiration(expiration != null ? expiration.toInstant() : null);
        u.setNotBefore(notBefore != null ? notBefore.toInstant() : null);
        return u;
    }

    public String generateToken(User issuer, Instant expiration, Instant notBefore, User authorized) {
        Claims claims = Jwts.claims().setSubject(authorized.getEmail());
        claims.put("userId", authorized.getId());
        claims.setIssuedAt(new Date());
        if (issuer != null) {
            claims.setIssuer(issuer.getEmail());
        }
        if (expiration != null) {
            claims.setExpiration(Date.from(expiration));
        }
        if (notBefore != null) {
            claims.setNotBefore(Date.from(notBefore));
        }
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public User getAuthentication() {
        return getAuthenticated();
    }

    public static User getAuthenticated() {
        User u = null;
        SecurityContext context = SecurityContextHolder.getContext();
        if (context != null) {
            Authentication a = context.getAuthentication();
            if (a != null) {
                Object o = a.getPrincipal();
                if (o instanceof User) {
                    u = (User) o;
                }
            }
        }
        if (u == null) {
            throw UnauthorizedException.byInsufficientPermission();
        }
        return u;
    }

    public boolean canAccess(Filterable object) {
        String id = requestId();
        if (log.isDebugEnabled()) {
            log.debug(
                    "[{}] Attempting to access entity '{}' with id {}",
                    id,
                    object != null ? object.getClass().getSimpleName() : "null",
                    object != null ? object.getId() : "null"
            );
        }
        return hasAccess(object, id);
    }

    private boolean hasAccess(Filterable object, String id) {
        try {
            User u = getRedacted();
            if (log.isTraceEnabled()) {
                log.trace("[{}] Access requested by: {}", id, u);
            }
            if (u.getRole() == Role.SUPER) {
                if (log.isTraceEnabled()) {
                    log.trace("[{}] Access granted by user's role", id);
                }
                return true;
            }
            if (object == null) {
                if (log.isTraceEnabled()) {
                    log.trace("[{}] Access granted by entity null", id);
                }
                return true;
            }
            if (object.getOwner() == null) {
                if (log.isDebugEnabled()) {
                    log.debug("[{}] Access denied by entity's owner null", id);
                }
                return false;
            }
            boolean isOwner = object.getOwner().equals(u);
            if (isOwner && log.isTraceEnabled()) {
                log.trace("[{}] Access granted by being owner", id);
            } else if (!isOwner && log.isDebugEnabled()) {
                log.debug("[{}] Access denied by mismatch owner", id);
            }
            return isOwner;
        } catch (UnauthorizedException ue) {
            if (log.isDebugEnabled()) {
                log.debug("[{}] Access denied by unauthorized", id);
            }
            return false;
        }
    }

    public boolean canCreate(String resource) {
        String id = requestId();
        if (log.isDebugEnabled()) {
            log.debug("[{}] Attempting to create a resource '{}'", id, resource);
        }
        try {
            User u = getRedacted();
            if (log.isTraceEnabled()) {
                log.trace("[{}] Access requested by: {}", id, u);
            }
            boolean canCreate = u.getRole() == Role.SUPER || u.getRole() == Role.USER;
            if (canCreate && log.isTraceEnabled()) {
                log.trace("[{}] Access granted by user's role", id);
            } else if (!canCreate && log.isDebugEnabled()) {
                log.debug("[{}] Access denied by user's role", id);
            }
            return canCreate;
        } catch (UnauthorizedException ue) {
            if (log.isDebugEnabled()) {
                log.debug("[{}] Access denied by unauthorized", id);
            }
            return false;
        }
    }

    private String requestId() {
        return UUID.randomUUID().toString().substring(32, 36);
    }

    private User getRedacted() {
        User u = getAuthentication();
        User _u = new User();
        _u.setId(u.getId());
        _u.setEmail(u.getEmail());
        _u.setRole(u.getRole());
        _u.setExpiration(u.getExpiration());
        _u.setNotBefore(u.getNotBefore());
        return _u;
    }

}
