package com.fsquiroz.expenses.repository;

import com.fsquiroz.expenses.entity.db.Discount;
import com.fsquiroz.expenses.entity.db.Expense;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Repository
public interface DiscountRepository extends JpaRepository<Discount, Long> {

    @Transactional
    @Modifying
    @Query("update Discount d set d.deleted = ?2 where d.deleted is null and d.expense = ?1")
    void deleteByExpense(Expense e, Instant deleted);

    @Query("select sum(d.amount) from Discount d where d.deleted is null and d.expense = ?1")
    BigDecimal getTotal(Expense e);

    List<Discount> findByDeletedIsNullAndExpense(Expense e);

    @Query("select d from Discount d where d.deleted is null and d.expense = ?1")
    Page<Discount> search(Expense e, Pageable page);

    @Query("select d from Discount d where d.deleted is null")
    Page<Discount> search(Pageable page);

}
