package com.fsquiroz.expenses.repository;

import com.fsquiroz.expenses.entity.db.Budget;
import com.fsquiroz.expenses.entity.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;

@Repository
public interface BudgetRepository extends JpaRepository<Budget, Long> {

    @Query("select b from Budget b where b.deleted is null and b.owner = ?1")
    Page<Budget> search(User owner, Pageable page);

    @Query("select b from Budget b where b.deleted is null and b.owner = ?2 and b.name like %?1%")
    Page<Budget> search(String term, User owner, Pageable page);

    @Query("select b from Budget b where b.deleted is null and b.owner = ?2 and b.from <= ?1 and b.to >= ?1")
    Page<Budget> search(Instant within, User owner, Pageable page);

    @Query("select b from Budget b where b.deleted is null and b.owner = ?3 and b.from <= ?2 and b.to >= ?2 and b.name like %?1%")
    Page<Budget> search(String term, Instant within, User owner, Pageable page);

}
