package com.fsquiroz.expenses.repository;

import com.fsquiroz.expenses.entity.db.Budget;
import com.fsquiroz.expenses.entity.db.BudgetItem;
import com.fsquiroz.expenses.entity.db.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BudgetItemRepository extends JpaRepository<BudgetItem, Long> {

    Optional<BudgetItem> findByDeletedIsNullAndBudgetAndCategory(Budget budget, Category category);

    List<BudgetItem> findByDeletedIsNullAndBudget(Budget budget);

    @Query("select bi from BudgetItem bi where bi.deleted is null and bi.budget = ?1")
    Page<BudgetItem> list(Budget budget, Pageable page);

}
