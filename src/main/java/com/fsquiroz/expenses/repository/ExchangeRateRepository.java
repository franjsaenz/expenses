package com.fsquiroz.expenses.repository;

import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.ExchangeRate;
import com.fsquiroz.expenses.entity.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ExchangeRateRepository extends JpaRepository<ExchangeRate, Long> {

    ExchangeRate findFirstByDeletedIsNullAndFromAndToOrderByRegisteredDesc(Currency from, Currency to);

    @Query("select er from ExchangeRate er where er.deleted is null and (er.from = ?1 or er.to = ?1)")
    Page<ExchangeRate> search(Currency c, Pageable page);

    @Query("select er from ExchangeRate er where er.deleted is null and er.owner = ?1")
    Page<ExchangeRate> search(User owner, Pageable page);

}
