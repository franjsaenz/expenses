package com.fsquiroz.expenses.repository;

import com.fsquiroz.expenses.entity.db.Detail;
import com.fsquiroz.expenses.entity.db.Expense;
import com.fsquiroz.expenses.entity.db.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Repository
public interface DetailRepository extends JpaRepository<Detail, Long> {

    @Transactional
    @Modifying
    @Query("update Detail d set d.deleted = ?2 where d.deleted is null and d.expense = ?1")
    void deleteByExpense(Expense e, Instant deleted);

    @Query("select sum(d.amount) from Detail d where d.deleted is null and d.expense = ?1")
    BigDecimal getTotal(Expense e);

    List<Detail> findByDeletedIsNullAndExpense(Expense e);

    @Query("select d from Detail d where d.deleted is null and d.expense = ?1")
    Page<Detail> search(Expense e, Pageable page);

    @Query("select d from Detail d where d.deleted is null and d.product = ?1")
    Page<Detail> search(Product p, Pageable page);

    @Query("select d from Detail d where d.deleted is null")
    Page<Detail> search(Pageable page);

}
