package com.fsquiroz.expenses.repository;

import com.fsquiroz.expenses.entity.db.Category;
import com.fsquiroz.expenses.entity.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query("select c from Category c where c.deleted is null and c.owner = ?2 and c.name like %?1%")
    Page<Category> search(String term, User user, Pageable page);

    Page<Category> findByDeletedIsNullAndOwner(User owner, Pageable page);

}
