package com.fsquiroz.expenses.repository;

import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.Instant;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Long> {

    @Query("SELECT SUM(d.amount) FROM com.fsquiroz.expenses.entity.db.Discount d LEFT JOIN d.expense e WHERE d.deleted IS NULL AND e.deleted IS NULL AND e.status <> com.fsquiroz.expenses.entity.db.ExpenseStatus.IGNORED AND e.currency = ?1 AND e.registered >= ?2 AND e.registered <= ?3")
    BigDecimal totalDiscountsByCurrency(Currency currency, Instant from, Instant to);

    @Query("select c from Currency c where c.deleted is null and (c.name like %?1% or c.code like %?1%)")
    Page<Currency> search(String term, Pageable page);

    Page<Currency> findByDeletedIsNull(Pageable page);

}
