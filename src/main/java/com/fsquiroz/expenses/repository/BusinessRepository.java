package com.fsquiroz.expenses.repository;

import com.fsquiroz.expenses.entity.db.Business;
import com.fsquiroz.expenses.entity.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BusinessRepository extends JpaRepository<Business, Long> {

    @Query("select b from Business b where b.deleted is null and b.owner = ?2 and b.name like %?1%")
    Page<Business> search(String term, User owner, Pageable page);

    Page<Business> findByDeletedIsNullAndOwner(User owner, Pageable page);

}
