package com.fsquiroz.expenses.repository;

import com.fsquiroz.expenses.entity.db.*;
import com.fsquiroz.expenses.entity.json.MGroupDetail;
import com.fsquiroz.expenses.service.MathUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.List;

@Repository
public class StatRepository {

    private EntityManager em;

    public StatRepository(EntityManager em) {
        this.em = em;
    }

    public List<MGroupDetail> totalsByCategoryAndBusiness(Business business, Currency currency, Instant from, Instant to) {
        List<MGroupDetail> details = em.createQuery(
                "SELECT " +
                        "NEW com.fsquiroz.expenses.entity.json.MGroupDetail(c.id, c.name, SUM(d.amount), SUM(d.discount), SUM(d.subtotal)) " +
                        "FROM com.fsquiroz.expenses.entity.db.Detail d " +
                        "LEFT JOIN d.expense e " +
                        "LEFT JOIN d.category c " +
                        "WHERE " +
                        "d.deleted IS NULL AND " +
                        "e.deleted IS NULL AND " +
                        "e.status <> com.fsquiroz.expenses.entity.db.ExpenseStatus.IGNORED AND " +
                        "e.currency = :currency AND " +
                        "e.business = :business AND " +
                        "e.registered >= :fromDate AND " +
                        "e.registered <= :toDate " +
                        "GROUP BY c.id " +
                        "ORDER BY 5 DESC",
                MGroupDetail.class
        )
                .setParameter("business", business)
                .setParameter("currency", currency)
                .setParameter("fromDate", from)
                .setParameter("toDate", to)
                .getResultList();
        return round(details);
    }

    public List<MGroupDetail> totalsByBusiness(Currency currency, Instant from, Instant to, User user) {
        List<MGroupDetail> details = em.createQuery(
                "SELECT " +
                        "NEW com.fsquiroz.expenses.entity.json.MGroupDetail(b.id, b.name, SUM(d.amount), SUM(d.discount), SUM(d.subtotal)) " +
                        "FROM com.fsquiroz.expenses.entity.db.Detail d " +
                        "LEFT JOIN d.expense e " +
                        "LEFT JOIN e.business b " +
                        "WHERE " +
                        "d.owner = :user AND " +
                        "d.deleted IS NULL AND " +
                        "e.deleted IS NULL AND " +
                        "e.status <> com.fsquiroz.expenses.entity.db.ExpenseStatus.IGNORED AND " +
                        "e.currency = :currency AND " +
                        "e.registered >= :fromDate AND " +
                        "e.registered <= :toDate " +
                        "GROUP BY b.id " +
                        "ORDER BY 5 DESC",
                MGroupDetail.class
        )
                .setParameter("currency", currency)
                .setParameter("fromDate", from)
                .setParameter("toDate", to)
                .setParameter("user", user)
                .getResultList();
        return round(details);
    }

    public List<MGroupDetail> totalsByCategoryAndCurrency(Currency currency, Instant from, Instant to, User user) {
        List<MGroupDetail> details = em.createQuery(
                "SELECT " +
                        "NEW com.fsquiroz.expenses.entity.json.MGroupDetail(c.id, c.name, SUM(d.amount), SUM(d.discount), SUM(d.subtotal)) " +
                        "FROM com.fsquiroz.expenses.entity.db.Detail d " +
                        "LEFT JOIN d.expense e " +
                        "LEFT JOIN d.category c " +
                        "WHERE " +
                        "d.owner = :user AND " +
                        "d.deleted IS NULL AND " +
                        "e.deleted IS NULL AND " +
                        "e.status <> com.fsquiroz.expenses.entity.db.ExpenseStatus.IGNORED AND " +
                        "e.currency = :currency AND " +
                        "e.registered >= :fromDate AND " +
                        "e.registered <= :toDate " +
                        "GROUP BY c.id " +
                        "ORDER BY 5 DESC",
                MGroupDetail.class
        )
                .setParameter("currency", currency)
                .setParameter("fromDate", from)
                .setParameter("toDate", to)
                .setParameter("user", user)
                .getResultList();
        return round(details);
    }

    public List<MGroupDetail> historyByProductAndCurrency(Product pro, Currency currency, Instant from, Instant to) {
        List<MGroupDetail> details = em.createQuery(
                "SELECT " +
                        "NEW com.fsquiroz.expenses.entity.json.MGroupDetail(b.id, b.name, e.registered, d.amount, d.discount, d.subtotal) " +
                        "FROM com.fsquiroz.expenses.entity.db.Detail d " +
                        "LEFT JOIN d.expense e " +
                        "LEFT JOIN e.business b " +
                        "WHERE " +
                        "d.deleted IS NULL AND " +
                        "d.product = :product AND " +
                        "e.deleted IS NULL AND " +
                        "e.currency = :currency AND " +
                        "e.registered >= :fromDate AND " +
                        "e.registered <= :toDate " +
                        "ORDER BY e.registered ASC",
                MGroupDetail.class
        )
                .setParameter("product", pro)
                .setParameter("currency", currency)
                .setParameter("fromDate", from)
                .setParameter("toDate", to)
                .getResultList();
        return round(details);
    }

    public List<MGroupDetail> historyByCategoryAndCurrency(Category category, Currency currency, Instant from, Instant to) {
        List<MGroupDetail> details = em.createQuery(
                "SELECT " +
                        "NEW com.fsquiroz.expenses.entity.json.MGroupDetail(b.id, b.name, e.registered, MAX(d.amount), MAX(d.discount), MAX(d.subtotal)) " +
                        "FROM com.fsquiroz.expenses.entity.db.Detail d " +
                        "LEFT JOIN d.expense e " +
                        "LEFT JOIN e.business b " +
                        "LEFT JOIN d.category c " +
                        "WHERE " +
                        "d.deleted IS NULL AND " +
                        "d.category = :category AND " +
                        "e.deleted IS NULL AND " +
                        "e.currency = :currency AND " +
                        "e.registered >= :fromDate AND " +
                        "e.registered <= :toDate " +
                        "GROUP BY d.expense " +
                        "ORDER BY e.registered DESC",
                MGroupDetail.class
        )
                .setParameter("category", category)
                .setParameter("currency", currency)
                .setParameter("fromDate", from)
                .setParameter("toDate", to)
                .getResultList();
        return round(details);
    }

    public List<MGroupDetail> totalsByCategoryAndCurrency(Expense expense) {
        List<MGroupDetail> details = em.createQuery(
                "SELECT " +
                        "NEW com.fsquiroz.expenses.entity.json.MGroupDetail(c.id, c.name, SUM(d.amount), SUM(d.discount), SUM(d.subtotal)) " +
                        "FROM com.fsquiroz.expenses.entity.db.Detail d " +
                        "LEFT JOIN d.category c " +
                        "WHERE " +
                        "d.deleted IS NULL AND " +
                        "d.expense = :expense " +
                        "GROUP BY c.id " +
                        "ORDER BY 5 DESC",
                MGroupDetail.class
        )
                .setParameter("expense", expense)
                .getResultList();
        return round(details);
    }

    private List<MGroupDetail> round(List<MGroupDetail> details) {
        if (details == null) {
            return null;
        }
        for (MGroupDetail mgd : details) {
            mgd.setDiscounts(MathUtils.round(mgd.getDiscounts()));
            mgd.setSubtotal(MathUtils.round(mgd.getSubtotal()));
            mgd.setTotal(MathUtils.round(mgd.getTotal()));
        }
        return details;
    }

}
