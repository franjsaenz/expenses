package com.fsquiroz.expenses.repository;

import com.fsquiroz.expenses.entity.db.Business;
import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.Expense;
import com.fsquiroz.expenses.entity.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ExpenseRepository extends JpaRepository<Expense, Long> {

    @Query("select e from Expense e where e.deleted is null and e.business = ?1 and e.currency = ?2")
    Page<Expense> search(Business b, Currency c, Pageable page);

    @Query("select e from Expense e where e.deleted is null and e.business = ?1")
    Page<Expense> searchByBusiness(Business b, Pageable page);

    @Query("select e from Expense e where e.deleted is null and e.currency = ?1")
    Page<Expense> searchByCurrency(Currency c, Pageable page);

    @Query("select e from Expense e where e.deleted is null and e.owner = ?1")
    Page<Expense> searchByUser(User owner, Pageable page);

}
