package com.fsquiroz.expenses.repository;

import com.fsquiroz.expenses.entity.db.Category;
import com.fsquiroz.expenses.entity.db.Product;
import com.fsquiroz.expenses.entity.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query("select p from Product p where p.deleted is null and p.owner = ?2 and p.name like %?1%")
    Page<Product> search(String term, User owner, Pageable page);

    @Query("select p from Product p where p.deleted is null and p.category = ?1 and p.name like %?2%")
    Page<Product> search(Category category, String term, Pageable page);

    Page<Product> findByDeletedIsNullAndCategory(Category category, Pageable page);

    Page<Product> findByDeletedIsNullAndOwner(User owner, Pageable page);

}
