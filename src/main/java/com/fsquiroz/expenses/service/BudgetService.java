package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.Budget;
import com.fsquiroz.expenses.entity.db.BudgetItem;
import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MBudget;
import com.fsquiroz.expenses.entity.json.MBudgetReport;
import com.fsquiroz.expenses.entity.json.MBudgetReportDetail;
import com.fsquiroz.expenses.entity.json.MGroupDetail;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.repository.BudgetRepository;
import com.fsquiroz.expenses.repository.StatRepository;
import com.fsquiroz.expenses.service.declaration.IBudgetItemService;
import com.fsquiroz.expenses.service.declaration.IBudgetService;
import com.fsquiroz.expenses.service.declaration.ICurrencyService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.*;

@Service
public class BudgetService implements IBudgetService {

    private IBudgetItemService budgetItemService;

    private ICurrencyService currencyService;

    private BudgetRepository budgetRepository;

    private StatRepository statRepository;

    public BudgetService(IBudgetItemService budgetItemService, ICurrencyService currencyService, BudgetRepository budgetRepository, StatRepository statRepository) {
        this.budgetItemService = budgetItemService;
        this.currencyService = currencyService;
        this.budgetRepository = budgetRepository;
        this.statRepository = statRepository;
    }

    @Override
    public Budget get(Long id) {
        if (id == null) {
            throw BadRequestException.byInvalidId();
        }
        return budgetRepository.findById(id).orElseThrow(() -> NotFoundException.byId(Budget.class, id));
    }

    @Override
    public MBudgetReport report(Budget budget, User user) {
        BigDecimal zero = new BigDecimal("0.00").setScale(2, RoundingMode.HALF_DOWN);
        List<MBudgetReportDetail> reportDetails = new ArrayList<>();
        List<BudgetItem> items = budgetItemService.list(budget);
        MBudgetReport report = new MBudgetReport();
        report.setBudget(build(budget));
        report.setDetails(reportDetails);
        Map<Long, BudgetItem> grouped = new HashMap<>();
        BigDecimal expected = zero;
        for (BudgetItem bi : items) {
            grouped.put(bi.getCategory().getId(), bi);
            expected = expected.add(bi.getAmount()).setScale(2, RoundingMode.HALF_DOWN);
        }
        List<MGroupDetail> details = statRepository.totalsByCategoryAndCurrency(budget.getCurrency(), budget.getFrom(), budget.getTo(), user);
        List<MGroupDetail> unaccounted = new ArrayList<>();
        BigDecimal expended = zero;
        BigDecimal notAccounted = zero;
        for (MGroupDetail d : details) {
            BudgetItem bi = grouped.get(d.getId());
            expended = expended.add(d.getTotal()).setScale(2, RoundingMode.HALF_DOWN);
            if (bi == null) {
                notAccounted = notAccounted.add(d.getTotal()).setScale(2, RoundingMode.HALF_DOWN);
                unaccounted.add(d);
                continue;
            }
            MBudgetReportDetail rd = calculate(bi, d);
            reportDetails.add(rd);
            grouped.remove(d.getId());
        }
        for (BudgetItem bi : grouped.values()) {
            MBudgetReportDetail rd = calculate(bi, null);
            reportDetails.add(rd);
        }
        Collections.sort(reportDetails);
        report.setUnaccounted(unaccounted);
        report.setTotalExpected(expected);
        report.setTotalExpended(expended);
        report.setTotalRemainedToExpend(expected.subtract(expended).setScale(2, RoundingMode.HALF_DOWN));
        report.setTotalUnaccounted(notAccounted);
        if (BigDecimal.ZERO.compareTo(report.getTotalExpected()) != 0) {
            report.setTotalCompleted(
                    report.getTotalExpended().divide(report.getTotalExpected(), 2, RoundingMode.FLOOR)
            );
        } else {
            report.setTotalCompleted(new BigDecimal("0.00"));
        }
        return report;
    }

    private MBudgetReportDetail calculate(BudgetItem bi, MGroupDetail d) {
        BigDecimal expended = d != null ? d.getTotal() : new BigDecimal("0.00");
        MBudgetReportDetail rd = new MBudgetReportDetail();
        rd.setExpected(bi.getAmount());
        rd.setExpended(expended);
        rd.setId(bi.getCategory().getId());
        rd.setName(bi.getCategory().getName());
        BigDecimal remain = bi.getAmount().subtract(expended).setScale(2, RoundingMode.HALF_DOWN);
        rd.setRemainedToExpend(remain);
        BigDecimal completed;
        if (BigDecimal.ZERO.compareTo(rd.getExpected()) != 0) {
            completed = rd.getExpended().divide(rd.getExpected(), 2, RoundingMode.FLOOR);
        } else {
            completed = new BigDecimal("0.00");
        }
        rd.setCompleted(completed);
        return rd;
    }

    @Override
    public Budget create(MBudget mb, Currency currency, User owner) {
        Budget b = validate(mb, new Budget(), currency);
        b.setCreated(Instant.now());
        b.setOwner(owner);
        return budgetRepository.save(b);
    }

    @Override
    public Budget update(Budget original, MBudget edition, Currency currency) {
        validate(edition, original, currency);
        original.setUpdated(Instant.now());
        return budgetRepository.save(original);
    }

    private Budget validate(MBudget mb, Budget b, Currency c) {
        if (b.getDeleted() != null) {
            throw BadRequestException.byEditingDeleted(b);
        }
        if (c == null) {
            throw BadRequestException.byMissingParam("currencyId");
        } else if (c.getDeleted() != null) {
            throw BadRequestException.byUsingDeleted(c, b);
        }
        if (mb.getFrom() == null) {
            throw BadRequestException.byMissingParam("from");
        }
        if (mb.getTo() == null) {
            throw BadRequestException.byMissingParam("to");
        }
        if (mb.getFrom().isAfter(mb.getTo())) {
            throw BadRequestException.byFromAfterTo(mb.getFrom(), mb.getTo());
        }
        b.setCurrency(c);
        b.setFrom(mb.getFrom());
        b.setTo(mb.getTo());
        b.setName(mb.getName());
        return b;
    }

    @Override
    public Budget repeat(MBudget mb, Budget original, Currency currency, User owner) {
        Budget b = create(mb, currency, owner);
        budgetItemService.repeat(original, b);
        return b;
    }

    @Override
    public Budget delete(Budget budget) {
        if (budget.getDeleted() != null) {
            throw BadRequestException.byDeletingDeleted(budget);
        }
        budget.setDeleted(Instant.now());
        return budgetRepository.save(budget);
    }

    @Override
    public Page<Budget> search(String term, Instant within, Pageable page, User user) {
        page = SortUtils.defaultSorting(page, Sort.Direction.DESC, "id");
        boolean byTerm = term != null && !term.isEmpty();
        boolean byDate = within != null;
        if (byTerm && byDate) {
            return budgetRepository.search(term, within, user, page);
        } else if (byTerm) {
            return budgetRepository.search(term, user, page);
        } else if (byDate) {
            return budgetRepository.search(within, user, page);
        } else {
            return budgetRepository.search(user, page);
        }
    }

    @Override
    public MBudget build(Budget budget) {
        MBudget mb = new MBudget();
        mb.setId(budget.getId());
        mb.setCreated(budget.getCreated());
        mb.setUpdated(budget.getUpdated());
        mb.setDeleted(budget.getDeleted());
        mb.setFrom(budget.getFrom());
        mb.setTo(budget.getTo());
        mb.setName(budget.getName());
        if (budget.getCurrency() != null) {
            mb.setCurrency(currencyService.build(budget.getCurrency()));
        }
        return mb;
    }

}
