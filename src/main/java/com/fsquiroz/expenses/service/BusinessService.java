package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.Business;
import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MBusiness;
import com.fsquiroz.expenses.entity.json.MGroup;
import com.fsquiroz.expenses.entity.json.MGroupDetail;
import com.fsquiroz.expenses.entity.util.DateRange;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.repository.BusinessRepository;
import com.fsquiroz.expenses.repository.StatRepository;
import com.fsquiroz.expenses.service.declaration.IBusinessService;
import com.fsquiroz.expenses.service.declaration.ICurrencyService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.List;

@Service
public class BusinessService implements IBusinessService {

    private ICurrencyService currencyService;

    private BusinessRepository businessRepository;

    private StatRepository statRepository;

    public BusinessService(ICurrencyService currencyService, BusinessRepository businessRepository, StatRepository statRepository) {
        this.currencyService = currencyService;
        this.businessRepository = businessRepository;
        this.statRepository = statRepository;
    }

    @Override
    public Business get(Long id) {
        if (id == null) {
            throw BadRequestException.byInvalidId();
        }
        return businessRepository.findById(id).orElseThrow(() -> NotFoundException.byId(Business.class, id));
    }

    @Override
    public Business create(MBusiness mb, User owner) {
        if (mb.getName() == null || mb.getName().isEmpty()) {
            throw BadRequestException.byMissingParam("name");
        }
        Business c = new Business();
        c.setCreated(Instant.now());
        c.setName(mb.getName());
        c.setOwner(owner);
        return businessRepository.save(c);
    }

    @Override
    public Business update(Business original, MBusiness edition) {
        if (original.getDeleted() != null) {
            throw BadRequestException.byEditingDeleted(original);
        }
        if (edition.getName() == null || edition.getName().isEmpty()) {
            throw BadRequestException.byMissingParam("name");
        }
        original.setName(edition.getName());
        return businessRepository.save(original);
    }

    @Override
    public Business delete(Business business) {
        if (business.getDeleted() != null) {
            throw BadRequestException.byDeletingDeleted(business);
        }
        business.setDeleted(Instant.now());
        return businessRepository.save(business);
    }

    @Override
    public Page<Business> search(String term, Pageable page, User user) {
        if (term == null || term.isEmpty()) {
            return businessRepository.findByDeletedIsNullAndOwner(user, page);
        } else {
            return businessRepository.search(term, user, page);
        }
    }

    @Override
    public MBusiness build(Business domain) {
        return new MBusiness(
                domain.getId(),
                domain.getCreated(),
                domain.getUpdated(),
                domain.getDeleted(),
                domain.getName()
        );
    }

    @Override
    public MGroup totalsByBusinessAndCurrency(Business business, Currency currency, Instant from, Instant to) {
        DateRange range = TimeUtils.dateRange(from, to);
        List<MGroupDetail> details = statRepository.totalsByCategoryAndBusiness(business, currency, range.getFrom(), range.getTo());
        return buildGroup(details, business, currency, range.getFrom(), range.getTo());
    }

    @Override
    public MGroup totalsByBusiness(Currency currency, Instant from, Instant to, User user) {
        DateRange range = TimeUtils.dateRange(from, to);
        List<MGroupDetail> details = statRepository.totalsByBusiness(currency, range.getFrom(), range.getTo(), user);
        return buildGroup(details, null, currency, range.getFrom(), range.getTo());
    }

    private MGroup buildGroup(List<MGroupDetail> details, Business business, Currency currency, Instant from, Instant to) {
        MathContext mc = new MathContext(2, RoundingMode.HALF_DOWN);
        BigDecimal subtotal = new BigDecimal(0.0, mc);
        BigDecimal discounts = new BigDecimal(0.0, mc);
        for (MGroupDetail d : details) {
            subtotal = subtotal.add(d.getSubtotal());
            discounts = discounts.add(d.getDiscounts());
        }
        MGroup mg = new MGroup();
        mg.setFrom(from);
        mg.setTo(to);
        mg.setCurrency(currency != null ? currencyService.build(currency) : null);
        mg.setBusiness(business != null ? build(business) : null);
        mg.setDetails(details);
        mg.setSubtotal(subtotal);
        mg.setDiscounts(discounts);
        mg.setTotal(subtotal.subtract(discounts));
        return mg;
    }

}
