package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.ExchangeRate;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MCurrency;
import com.fsquiroz.expenses.entity.json.MExchangeRate;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.repository.ExchangeRateRepository;
import com.fsquiroz.expenses.service.declaration.ICurrencyService;
import com.fsquiroz.expenses.service.declaration.IExchangeRateService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class ExchangeRateService implements IExchangeRateService {

    private ICurrencyService currencyService;

    private ExchangeRateRepository exchangeRateRepository;

    public ExchangeRateService(ICurrencyService currencyService, ExchangeRateRepository exchangeRateRepository) {
        this.currencyService = currencyService;
        this.exchangeRateRepository = exchangeRateRepository;
    }

    @Override
    public ExchangeRate get(Long id) {
        if (id == null) {
            throw BadRequestException.byInvalidId();
        }
        return exchangeRateRepository.findById(id).orElseThrow(() -> NotFoundException.byId(ExchangeRate.class, id));
    }

    @Override
    public ExchangeRate get(Currency from, Currency to) {
        ExchangeRate er = exchangeRateRepository.findFirstByDeletedIsNullAndFromAndToOrderByRegisteredDesc(from, to);
        if (er == null) {
            er = exchangeRateRepository.findFirstByDeletedIsNullAndFromAndToOrderByRegisteredDesc(to, from);
        }
        return er;
    }

    @Override
    public ExchangeRate create(MExchangeRate mer, Currency from, Currency to, User owner) {
        if (mer.getRegistered() == null) {
            mer.setRegistered(Instant.now());
        }
        if (mer.getMultiplier() == null) {
            throw BadRequestException.byMissingParam("multiplier");
        }
        ExchangeRate er = new ExchangeRate();
        er.setCreated(Instant.now());
        er.setFrom(from);
        er.setTo(to);
        er.setMultiplier(mer.getMultiplier());
        er.setRegistered(mer.getRegistered());
        er.setOwner(owner);
        return exchangeRateRepository.save(er);
    }

    @Override
    public ExchangeRate update(ExchangeRate original, MExchangeRate edition) {
        if (original.getDeleted() != null) {
            throw BadRequestException.byEditingDeleted(original);
        }
        if (edition.getMultiplier() == null) {
            throw BadRequestException.byMissingParam("multiplier");
        }
        original.setUpdated(Instant.now());
        original.setMultiplier(edition.getMultiplier());
        return exchangeRateRepository.save(original);
    }

    @Override
    public ExchangeRate delete(ExchangeRate exchangeRate) {
        if (exchangeRate.getDeleted() != null) {
            throw BadRequestException.byDeletingDeleted(exchangeRate);
        }
        exchangeRate.setDeleted(Instant.now());
        return exchangeRateRepository.save(exchangeRate);
    }

    @Override
    public Page<ExchangeRate> search(Currency currency, Pageable page, User user) {
        page = SortUtils.defaultSorting(page, Sort.Direction.DESC, "created");
        if (currency == null) {
            return exchangeRateRepository.search(user, page);
        } else {
            return exchangeRateRepository.search(currency, page);
        }
    }

    @Override
    public MExchangeRate build(ExchangeRate er) {
        MCurrency from = currencyService.build(er.getFrom());
        MCurrency to = currencyService.build(er.getTo());
        MExchangeRate mer = new MExchangeRate();
        mer.setId(er.getId());
        mer.setCreated(er.getCreated());
        mer.setUpdated(er.getUpdated());
        mer.setDeleted(er.getDeleted());
        mer.setRegistered(er.getRegistered());
        mer.setMultiplier(er.getMultiplier());
        mer.setFrom(from);
        mer.setTo(to);
        return mer;
    }

}
