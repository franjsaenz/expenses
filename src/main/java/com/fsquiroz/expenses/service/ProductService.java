package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.Category;
import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.Product;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MCategory;
import com.fsquiroz.expenses.entity.json.MGroup;
import com.fsquiroz.expenses.entity.json.MGroupDetail;
import com.fsquiroz.expenses.entity.json.MProduct;
import com.fsquiroz.expenses.entity.util.DateRange;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.repository.ProductRepository;
import com.fsquiroz.expenses.repository.StatRepository;
import com.fsquiroz.expenses.service.declaration.ICategoryService;
import com.fsquiroz.expenses.service.declaration.ICurrencyService;
import com.fsquiroz.expenses.service.declaration.IProductService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService implements IProductService {

    private StatRepository statRepository;

    private ProductRepository productRepository;

    private ICategoryService categoryService;

    private ICurrencyService currencyService;

    public ProductService(StatRepository statRepository, ProductRepository productRepository, ICategoryService categoryService, ICurrencyService currencyService) {
        this.statRepository = statRepository;
        this.productRepository = productRepository;
        this.categoryService = categoryService;
        this.currencyService = currencyService;
    }

    @Override
    public Product get(Long id) {
        if (id == null) {
            throw BadRequestException.byInvalidId();
        }
        return productRepository.findById(id).orElseThrow(() -> NotFoundException.byId(Product.class, id));
    }

    @Override
    public Product create(MProduct mp, Category c, User owner) {
        if (c == null) {
            throw BadRequestException.byMissingParam("categoryId");
        }
        if (mp.getName() == null || mp.getName().isEmpty()) {
            throw BadRequestException.byMissingParam("name");
        }
        Product p = new Product();
        p.setCreated(Instant.now());
        p.setName(mp.getName());
        p.setCategory(c);
        p.setOwner(owner);
        return productRepository.save(p);
    }

    @Override
    public Product update(Product original, Category c, MProduct edition) {
        if (original.getDeleted() != null) {
            throw BadRequestException.byEditingDeleted(original);
        }
        if (c == null) {
            throw BadRequestException.byMissingParam("categoryId");
        }
        if (edition.getName() == null || edition.getName().isEmpty()) {
            throw BadRequestException.byMissingParam("name");
        }
        original.setName(edition.getName());
        original.setCategory(c);
        original.setUpdated(Instant.now());
        return productRepository.save(original);
    }

    @Override
    public Product delete(Product product) {
        if (product.getDeleted() != null) {
            throw BadRequestException.byDeletingDeleted(product);
        }
        product.setDeleted(Instant.now());
        return productRepository.save(product);
    }

    @Override
    public Page<Product> search(Category category, String term, Pageable page, User user) {
        if (term == null || term.isEmpty()) {
            return category == null ?
                    productRepository.findByDeletedIsNullAndOwner(user, page) :
                    productRepository.findByDeletedIsNullAndCategory(category, page);
        } else {
            return category == null ?
                    productRepository.search(term, user, page) :
                    productRepository.search(category, term, page);
        }
    }

    @Override
    public Page<Product> search(Category category, String term, Pageable page) {
        if (term == null || term.isEmpty()) {
            return productRepository.findByDeletedIsNullAndCategory(category, page);
        } else {
            return productRepository.search(category, term, page);
        }
    }

    @Override
    public MGroup historyByProductAndCurrency(Product product, Currency currency, Instant from, Instant to) {
        if (currency == null) {
            throw BadRequestException.byMissingParam("currencyId");
        }
        DateRange range = TimeUtils.dateRange(from, to);
        List<MGroupDetail> details = statRepository.historyByProductAndCurrency(product, currency, range.getFrom(), range.getTo());
        MathContext mc = new MathContext(2, RoundingMode.HALF_DOWN);
        BigDecimal subtotal = new BigDecimal("0.0", mc);
        BigDecimal discounts = new BigDecimal("0.0", mc);
        BigDecimal total = new BigDecimal("0.0", mc);
        Instant lastDate = null;
        List<MGroupDetail> newDetails = new ArrayList<>();
        for (MGroupDetail d : details) {
            if (lastDate != null && d.getDate() != null) {
                if (d.getDate().getEpochSecond() - lastDate.getEpochSecond() < 1000) {
                    continue;
                }
            }
            subtotal = subtotal.add(d.getSubtotal());
            discounts = discounts.add(d.getDiscounts());
            total = total.add(d.getTotal());
            lastDate = d.getDate();
            newDetails.add(d);
        }
        MGroup g = new MGroup();
        g.setFrom(range.getFrom());
        g.setTo(range.getTo());
        g.setCurrency(currencyService.build(currency));
        g.setProduct(build(product));
        g.setDetails(newDetails);
        g.setDiscounts(discounts);
        g.setSubtotal(subtotal);
        g.setTotal(total);
        return g;
    }

    @Override
    public MProduct build(Product product) {
        MCategory mc = categoryService.build(product.getCategory());
        return new MProduct(
                product.getId(),
                product.getCreated(),
                product.getUpdated(),
                product.getDeleted(),
                product.getName(),
                mc
        );
    }


}
