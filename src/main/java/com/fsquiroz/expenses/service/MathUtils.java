package com.fsquiroz.expenses.service;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MathUtils {

    public static BigDecimal round(BigDecimal bg) {
        if (bg == null) {
            return BigDecimal.ZERO.setScale(2, RoundingMode.HALF_DOWN);
        }
        return bg.setScale(2, RoundingMode.HALF_DOWN);
    }

}
