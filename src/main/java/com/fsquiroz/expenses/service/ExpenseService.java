package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.*;
import com.fsquiroz.expenses.entity.json.*;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.repository.ExpenseRepository;
import com.fsquiroz.expenses.service.declaration.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Service
public class ExpenseService implements IExpenseService {

    private IBusinessService businessService;

    private ICurrencyService currencyService;

    private IExchangeRateService exchangeRateService;

    private IDetailService detailService;

    private IDiscountService discountService;

    private ExpenseRepository expenseRepository;

    public ExpenseService(IBusinessService businessService, ICurrencyService currencyService, IExchangeRateService exchangeRateService, IDetailService detailService, IDiscountService discountService, ExpenseRepository expenseRepository) {
        this.businessService = businessService;
        this.currencyService = currencyService;
        this.exchangeRateService = exchangeRateService;
        this.detailService = detailService;
        this.discountService = discountService;
        this.expenseRepository = expenseRepository;
    }

    @Override
    public Expense get(Long id) {
        if (id == null) {
            throw BadRequestException.byInvalidId();
        }
        return expenseRepository.findById(id).orElseThrow(() -> NotFoundException.byId(Expense.class, id));
    }

    @Override
    public Expense create(MExpense me, Business b, Currency c, ExchangeRate er, User owner) {
        Expense e = validate(new Expense(), b, c, er);
        e.setCreated(Instant.now());
        e.setStatus(ExpenseStatus.OPENED);
        if (me.getRegistered() == null) {
            e.setRegistered(e.getCreated());
        } else {
            e.setRegistered(me.getRegistered());
        }
        e.setOwner(owner);
        return expenseRepository.save(e);
    }

    @Override
    public Expense repeat(MExpense me, Expense original, Business b, Currency c, ExchangeRate er, User owner) {
        if (original.getStatus() == null || original.getStatus().equals(ExpenseStatus.OPENED)) {
            throw BadRequestException.byNotClosedExpense(original);
        }
        Expense e = create(me, b, c, er, owner);
        detailService.repeat(original, e, owner);
        discountService.repeat(original, e, owner);
        updateAmounts(e);
        return e;
    }

    @Override
    public Expense update(Expense original, Business b, Currency c, ExchangeRate er, MExpense edition) {
        validate(original, b, c, er);
        if (edition.getRegistered() == null) {
            edition.setRegistered(original.getRegistered());
        }
        original.setRegistered(edition.getRegistered());
        original.setUpdated(Instant.now());
        return expenseRepository.save(original);
    }

    @Override
    public Expense updateStatus(Expense original, ExpenseStatus status) {
        if (original.getDeleted() != null) {
            throw BadRequestException.byEditingDeleted(original);
        }
        original.setStatus(status);
        original.setUpdated(Instant.now());
        return expenseRepository.save(original);
    }

    @Override
    public void updateAmounts(Expense e) {
        BigDecimal discounts = discountService.getTotal(e);
        detailService.applyDiscounts(e, discounts);
    }

    private Expense validate(Expense e, Business b, Currency c, ExchangeRate er) {
        if (e.getDeleted() != null) {
            throw BadRequestException.byEditingDeleted(e);
        }
        if (e.getStatus() != null && e.getStatus() != ExpenseStatus.OPENED) {
            throw BadRequestException.byNotOpenedExpense(e);
        }
        if (b == null) {
            throw BadRequestException.byMissingParam("businessId");
        } else if (b.getDeleted() != null) {
            throw BadRequestException.byUsingDeleted(b, e);
        }
        if (c == null) {
            throw BadRequestException.byMissingParam("currencyId");
        } else if (c.getDeleted() != null) {
            throw BadRequestException.byUsingDeleted(c, e);
        }
        if (er != null && !(c.equals(er.getFrom()) || c.equals(er.getTo()))) {
            er = null;
        }
        if (er != null && er.getDeleted() != null) {
            throw BadRequestException.byUsingDeleted(er, e);
        }
        e.setBusiness(b);
        e.setCurrency(c);
        e.setExchangeRate(er);
        return e;
    }

    @Override
    public Expense delete(Expense expense) {
        if (expense.getDeleted() != null) {
            throw BadRequestException.byDeletingDeleted(expense);
        }
        Instant deleted = Instant.now();
        discountService.deleteAll(expense, deleted);
        expense.setDeleted(deleted);
        expense.setStatus(ExpenseStatus.IGNORED);
        return expenseRepository.save(expense);
    }

    @Override
    public Page<Expense> search(Business b, Currency c, Pageable page, User user) {
        page = SortUtils.defaultSorting(page, Sort.Direction.DESC, "registered");
        if (b != null && c != null) {
            return expenseRepository.search(b, c, page);
        } else if (b != null) {
            return expenseRepository.searchByBusiness(b, page);
        } else if (c != null) {
            return expenseRepository.searchByCurrency(c, page);
        } else {
            return expenseRepository.searchByUser(user, page);
        }
    }

    @Override
    public MExpense build(Expense expense, boolean extended) {
        List<MGroupDetail> totals = null;
        MBusiness mb = null;
        MCurrency mc = null;
        MExchangeRate mer = null;
        BigDecimal discounts = discountService.getTotal(expense);
        BigDecimal details = detailService.getTotal(expense);
        if (extended && details.compareTo(BigDecimal.ZERO) > 0) {
            totals = detailService.totalsByCategory(expense);
        }
        if (expense.getBusiness() != null) {
            mb = businessService.build(expense.getBusiness());
        }
        if (expense.getCurrency() != null) {
            mc = currencyService.build(expense.getCurrency());
        }
        if (expense.getExchangeRate() != null) {
            mer = exchangeRateService.build(expense.getExchangeRate());
        }
        MExpense me = new MExpense();
        me.setId(expense.getId());
        me.setCreated(expense.getCreated());
        me.setUpdated(expense.getUpdated());
        me.setDeleted(expense.getDeleted());
        me.setRegistered(expense.getRegistered());
        me.setStatus(expense.getStatus());
        me.setBusiness(mb);
        me.setCurrency(mc);
        me.setExchangeRate(mer);
        me.setTotalByCategory(totals);
        me.setSubtotal(MathUtils.round(details));
        me.setDiscounts(MathUtils.round(discounts));
        me.setTotal(MathUtils.round(details.subtract(discounts)));
        return me;
    }

}
