package com.fsquiroz.expenses.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class SortUtils {

    public static Pageable defaultSorting(Pageable page, Sort.Direction direction, String property) {
        if (page.getSort() == null || page.getSort().isUnsorted()) {
            page = PageRequest.of(page.getPageNumber(), page.getPageSize(), Sort.by(direction, property));
        }
        return page;
    }

}
