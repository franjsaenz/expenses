package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.util.DateRange;
import com.fsquiroz.expenses.security.SecurityCheck;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoUnit;

public class TimeUtils {

    private static final DateTimeFormatter STANDARD = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSxx");

    private static final DateTimeFormatter STANDARD_ZULU = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX");

    private static final DateTimeFormatter DESERIALIZE_FORMATTER = new DateTimeFormatterBuilder()
            .append(DateTimeFormatter.ISO_DATE_TIME)
            .appendPattern("[x][X]")
            .toFormatter();

    public static String standard(Instant i) {
        return standard(i, getZoneId());
    }

    public static String standard(Instant i, ZoneId zoneId) {
        return zoneId == null || ZoneOffset.UTC.equals(zoneId) ?
                STANDARD_ZULU.format(ZonedDateTime.ofInstant(i, ZoneOffset.UTC)) :
                STANDARD.format(ZonedDateTime.ofInstant(i, zoneId));
    }

    public static Instant standard(String s) {
        return s == null ? null : OffsetDateTime.parse(s, DESERIALIZE_FORMATTER).toInstant();
    }

    public static DateRange dateRange(Instant from, Instant to) {
        if (from == null && to == null) {
            from = getThisMonthFirst();
            to = getThisMonthLast();
        } else if (from == null) {
            from = minusOneMonth(to);
        } else if (to == null) {
            to = plusOneMonth(from);
        }
        return DateRange.builder()
                .from(from)
                .to(to)
                .build();
    }

    public static Instant getThisMonthFirst() {
        ZoneId zoneId = getZoneId();
        return ZonedDateTime.now(zoneId)
                .truncatedTo(ChronoUnit.DAYS)
                .withDayOfMonth(1)
                .toInstant();
    }

    public static Instant getThisMonthLast() {
        return getThisMonthFirst().atZone(getZoneId())
                .plus(1, ChronoUnit.MONTHS)
                .minus(1, ChronoUnit.MILLIS)
                .toInstant();
    }

    public static ZoneId getZoneId() {
        ZoneId zoneId;
        try {
            User u = SecurityCheck.getAuthenticated();
            if (u.getZoneId() != null) {
                zoneId = ZoneId.of(u.getZoneId());
            } else {
                zoneId = ZoneOffset.UTC;
            }
        } catch (Exception e) {
            zoneId = ZoneOffset.UTC;
        }
        return zoneId;
    }

    public static Instant minusOneMonth(Instant when) {
        return when.atZone(getZoneId())
                .minus(1, ChronoUnit.MONTHS)
                .plus(1, ChronoUnit.MILLIS)
                .toInstant();
    }

    public static Instant plusOneMonth(Instant when) {
        return when.atZone(getZoneId())
                .plus(1, ChronoUnit.MONTHS)
                .minus(1, ChronoUnit.MILLIS)
                .toInstant();
    }

}
