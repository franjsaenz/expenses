package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.*;
import com.fsquiroz.expenses.entity.json.MDiscount;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.repository.DiscountRepository;
import com.fsquiroz.expenses.service.declaration.IDiscountService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
public class DiscountService implements IDiscountService {

    private DiscountRepository discountRepository;

    public DiscountService(DiscountRepository discountRepository) {
        this.discountRepository = discountRepository;
    }

    @Override
    public Discount get(Long id) {
        if (id == null) {
            throw BadRequestException.byInvalidId();
        }
        return discountRepository.findById(id).orElseThrow(() -> NotFoundException.byId(Discount.class, id));
    }

    @Override
    public BigDecimal getTotal(Expense e) {
        BigDecimal sum = discountRepository.getTotal(e);
        return sum == null ? BigDecimal.ZERO : sum;
    }

    @Override
    public Discount create(MDiscount md, Expense e, User owner) {
        Discount d = validate(new Discount(), md, e);
        d.setCreated(Instant.now());
        d.setExpense(e);
        d.setOwner(owner);
        return discountRepository.save(d);
    }

    @Override
    public List<Discount> repeat(Expense original, Expense repeat, User owner) {
        List<Discount> originalDiscounts = discountRepository.findByDeletedIsNullAndExpense(original);
        List<Discount> discounts = new ArrayList<>();
        for (Discount od : originalDiscounts) {
            Discount d = new Discount();
            d.setCreated(repeat.getCreated());
            d.setOwner(owner);
            d.setExpense(repeat);
            d.setAmount(od.getAmount().setScale(2, RoundingMode.HALF_DOWN));
            d.setDescription(od.getDescription());
            discounts.add(d);
        }
        if (!discounts.isEmpty()) {
            discountRepository.saveAll(discounts);
        }
        return discounts;
    }

    @Override
    public Discount update(Discount original, Expense e, MDiscount edition) {
        original = validate(original, edition, e);
        original.setUpdated(Instant.now());
        return discountRepository.save(original);
    }

    private Discount validate(Discount discount, MDiscount md, Expense e) {
        deletedValidation(discount, e);
        if (md.getAmount() == null) {
            throw BadRequestException.byMissingParam("amount");
        }
        if (md.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            md.setAmount(md.getAmount().multiply(new BigDecimal(-1)));
        }
        discount.setDescription(md.getDescription());
        discount.setAmount(md.getAmount().setScale(2, RoundingMode.HALF_DOWN));
        return discount;
    }

    private void deletedValidation(Discount d, Expense e) {
        if (e.getDeleted() != null) {
            throw BadRequestException.byEditingDeleted(e);
        }
        if (d.getDeleted() != null) {
            throw BadRequestException.byEditingDeleted(d);
        }
        if (d.getExpense() != null && !d.getExpense().equals(e)) {
            throw BadRequestException.byMissmatchExpense(d, e);
        }
        if (e.getStatus() != null && e.getStatus() != ExpenseStatus.OPENED) {
            throw BadRequestException.byNotOpenedExpense(e);
        }
    }

    @Override
    public Discount delete(Discount discount, Expense e) {
        deletedValidation(discount, e);
        discount.setDeleted(Instant.now());
        return discountRepository.save(discount);
    }

    @Override
    public void deleteAll(Expense e, Instant deleted) {
        discountRepository.deleteByExpense(e, deleted);
    }

    @Override
    public Page<Discount> search(Expense e, Pageable page) {
        page = SortUtils.defaultSorting(page, Sort.Direction.DESC, "created");
        if (e == null) {
            return discountRepository.search(page);
        } else {
            return discountRepository.search(e, page);
        }
    }

    @Override
    public MDiscount build(Discount domain) {
        MDiscount md = new MDiscount();
        md.setId(domain.getId());
        md.setCreated(domain.getCreated());
        md.setUpdated(domain.getUpdated());
        md.setDeleted(domain.getDeleted());
        md.setAmount(domain.getAmount());
        md.setDescription(domain.getDescription());
        return md;
    }

}
