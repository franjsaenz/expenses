package com.fsquiroz.expenses.service.declaration;

import com.fsquiroz.expenses.entity.db.Business;
import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MBusiness;
import com.fsquiroz.expenses.entity.json.MGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import java.time.Instant;

public interface IBusinessService extends IBuildiableService<Business, MBusiness> {

    @PostAuthorize("@securityCheck.canAccess(returnObject)")
    Business get(Long id);

    @PreAuthorize("@securityCheck.canCreate('Business')")
    Business create(MBusiness mb, User owner);

    @PreAuthorize("@securityCheck.canAccess(#original)")
    Business update(Business original, MBusiness edition);

    @PreAuthorize("@securityCheck.canAccess(#business)")
    Business delete(Business business);

    Page<Business> search(String term, Pageable page, User user);

    MGroup totalsByBusinessAndCurrency(Business business, Currency currency, Instant from, Instant to);

    MGroup totalsByBusiness(Currency currency, Instant from, Instant to, User user);

}
