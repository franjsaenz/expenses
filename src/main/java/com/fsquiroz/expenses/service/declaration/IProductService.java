package com.fsquiroz.expenses.service.declaration;

import com.fsquiroz.expenses.entity.db.Category;
import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.Product;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MGroup;
import com.fsquiroz.expenses.entity.json.MProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import java.time.Instant;

public interface IProductService extends IBuildiableService<Product, MProduct> {

    @PostAuthorize("@securityCheck.canAccess(returnObject)")
    Product get(Long id);

    @PreAuthorize("@securityCheck.canCreate('Product')")
    Product create(MProduct mc, Category c, User owner);

    @PreAuthorize("@securityCheck.canAccess(#original)")
    Product update(Product original, Category c, MProduct edition);

    @PreAuthorize("@securityCheck.canAccess(#product)")
    Product delete(Product product);

    Page<Product> search(Category category, String term, Pageable page, User owner);

    Page<Product> search(Category category, String term, Pageable page);

    MGroup historyByProductAndCurrency(Product product, Currency currency, Instant from, Instant to);

}
