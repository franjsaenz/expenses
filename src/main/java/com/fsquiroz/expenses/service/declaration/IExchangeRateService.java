package com.fsquiroz.expenses.service.declaration;

import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.ExchangeRate;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MExchangeRate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

public interface IExchangeRateService extends IBuildiableService<ExchangeRate, MExchangeRate> {

    @PostAuthorize("@securityCheck.canAccess(returnObject)")
    ExchangeRate get(Long id);

    ExchangeRate get(Currency from, Currency to);

    @PreAuthorize("@securityCheck.canCreate('EchangeRate')")
    ExchangeRate create(MExchangeRate mer, Currency from, Currency to, User owner);

    @PreAuthorize("@securityCheck.canAccess(#original)")
    ExchangeRate update(ExchangeRate original, MExchangeRate edition);

    @PreAuthorize("@securityCheck.canAccess(#exchangeRate)")
    ExchangeRate delete(ExchangeRate exchangeRate);

    Page<ExchangeRate> search(Currency currency, Pageable page, User owner);

}
