package com.fsquiroz.expenses.service.declaration;

import com.fsquiroz.expenses.entity.db.Budget;
import com.fsquiroz.expenses.entity.db.BudgetItem;
import com.fsquiroz.expenses.entity.db.Category;
import com.fsquiroz.expenses.entity.json.MBudgetItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface IBudgetItemService extends IBuildiableService<BudgetItem, MBudgetItem> {

    @PostAuthorize("@securityCheck.canAccess(returnObject)")
    BudgetItem get(Long id);

    @PreAuthorize("@securityCheck.canCreate('BudgetItem')")
    BudgetItem setIem(MBudgetItem mbi, Budget budget, Category category);

    @PreAuthorize("@securityCheck.canCreate('BudgetItem')")
    List<BudgetItem> repeat(Budget original, Budget repeat);

    @PreAuthorize("@securityCheck.canAccess(#budgetItem)")
    BudgetItem delete(BudgetItem budgetItem, Budget budget);

    List<BudgetItem> list(Budget budget);

    Page<BudgetItem> search(Budget budget, Pageable page);

}
