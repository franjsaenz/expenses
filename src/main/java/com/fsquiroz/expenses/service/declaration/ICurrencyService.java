package com.fsquiroz.expenses.service.declaration;

import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.json.MCurrency;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

public interface ICurrencyService extends IBuildiableService<Currency, MCurrency> {

    @PreAuthorize("isAuthenticated()")
    Currency get(Long id);

    @PreAuthorize("hasRole('SUPER')")
    Currency create(MCurrency mb);

    @PreAuthorize("hasRole('SUPER')")
    Currency update(Currency original, MCurrency edition);

    @PreAuthorize("hasRole('SUPER')")
    Currency delete(Currency currency);

    @PreAuthorize("isAuthenticated()")
    Page<Currency> search(String term, Pageable page);

}
