package com.fsquiroz.expenses.service.declaration;

import com.fsquiroz.expenses.entity.db.Category;
import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MCategory;
import com.fsquiroz.expenses.entity.json.MGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import java.time.Instant;

public interface ICategoryService extends IBuildiableService<Category, MCategory> {

    @PostAuthorize("@securityCheck.canAccess(returnObject)")
    Category get(Long id);

    @PreAuthorize("@securityCheck.canCreate('Category')")
    Category create(MCategory mc, User user);

    @PreAuthorize("@securityCheck.canAccess(#original)")
    Category update(Category original, MCategory edition);

    @PreAuthorize("@securityCheck.canAccess(#category)")
    Category delete(Category category);

    Page<Category> search(String term, Pageable page, User owner);

    MGroup historyByCategoryAndCurrency(Category category, Currency currency, Instant from, Instant to);

}
