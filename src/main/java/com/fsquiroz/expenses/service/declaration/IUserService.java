package com.fsquiroz.expenses.service.declaration;

import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MLogin;
import com.fsquiroz.expenses.entity.json.MResetPassword;
import com.fsquiroz.expenses.entity.json.MUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;

import java.time.Instant;

public interface IUserService extends IBuildiableService<User, MUser> {

    User login(MLogin login, Instant expiration, Instant notBefore);

    User getByEmail(String email);

    @PreAuthorize("isAuthenticated() and (hasRole('SUPER') or principal.id == #original.id)")
    User changePassword(User original, MResetPassword edit);

    @PreAuthorize("hasRole('SUPER')")
    User generateToken(User user, Instant expiration, Instant notBefore);

    @PreAuthorize("hasRole('SUPER')")
    User create(MUser model);

    @PreAuthorize("isAuthenticated() and (hasRole('SUPER') or (principal.id == #original.id  and !hasRole('CLIENT')))")
    User update(User original, MUser edit);

    @PreAuthorize("hasRole('SUPER')")
    User delete(User original);

    User get(Long id);

    @PreAuthorize("hasRole('SUPER')")
    Page<User> search(String term, Pageable pageable);

}
