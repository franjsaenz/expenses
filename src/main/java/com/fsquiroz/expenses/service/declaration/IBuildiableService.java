package com.fsquiroz.expenses.service.declaration;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

public interface IBuildiableService<D, M> {

    M build(D domain);

    default Page<M> build(Page<D> domains) {
        List<M> models = new ArrayList<>();
        for (D d : domains) {
            models.add(build(d));
        }
        return new PageImpl<>(models, domains.getPageable(), domains.getTotalElements());
    }

}
