package com.fsquiroz.expenses.service.declaration;

import com.fsquiroz.expenses.entity.db.*;
import com.fsquiroz.expenses.entity.json.MExpense;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

public interface IExpenseService extends IBuildiableService<Expense, MExpense> {

    @PostAuthorize("@securityCheck.canAccess(returnObject)")
    Expense get(Long id);

    @PreAuthorize("@securityCheck.canCreate('Expense')")
    Expense create(MExpense me, Business b, Currency c, ExchangeRate er, User owner);

    @PreAuthorize("@securityCheck.canCreate('Expense')")
    Expense repeat(MExpense me, Expense original, Business b, Currency c, ExchangeRate er, User owner);

    @PreAuthorize("@securityCheck.canAccess(#original)")
    Expense update(Expense original, Business b, Currency c, ExchangeRate er, MExpense edition);

    @PreAuthorize("@securityCheck.canAccess(#original)")
    Expense updateStatus(Expense original, ExpenseStatus status);

    @PreAuthorize("@securityCheck.canAccess(#e)")
    void updateAmounts(Expense e);

    @PreAuthorize("@securityCheck.canAccess(#expense)")
    Expense delete(Expense expense);

    Page<Expense> search(Business b, Currency c, Pageable page, User owner);

    @Override
    default MExpense build(Expense expense) {
        return build(expense, false);
    }

    MExpense build(Expense expense, boolean extended);

}
