package com.fsquiroz.expenses.service.declaration;

import com.fsquiroz.expenses.entity.db.Budget;
import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MBudget;
import com.fsquiroz.expenses.entity.json.MBudgetReport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import java.time.Instant;

public interface IBudgetService extends IBuildiableService<Budget, MBudget> {

    @PostAuthorize("@securityCheck.canAccess(returnObject)")
    Budget get(Long id);

    MBudgetReport report(Budget budget, User user);

    @PreAuthorize("@securityCheck.canCreate('Budget')")
    Budget create(MBudget mb, Currency currency, User owner);

    @PreAuthorize("@securityCheck.canAccess(#original)")
    Budget update(Budget original, MBudget edition, Currency currency);

    @PreAuthorize("@securityCheck.canCreate('Budget')")
    Budget repeat(MBudget mb, Budget original, Currency currency, User owner);

    @PreAuthorize("@securityCheck.canAccess(#budget)")
    Budget delete(Budget budget);

    Page<Budget> search(String term, Instant within, Pageable page, User user);

}
