package com.fsquiroz.expenses.service.declaration;

import com.fsquiroz.expenses.entity.db.Discount;
import com.fsquiroz.expenses.entity.db.Expense;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MDiscount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

public interface IDiscountService extends IBuildiableService<Discount, MDiscount> {

    @PostAuthorize("@securityCheck.canAccess(returnObject)")
    Discount get(Long id);

    BigDecimal getTotal(Expense e);

    @PreAuthorize("@securityCheck.canCreate('Discount')")
    Discount create(MDiscount md, Expense e, User owner);

    @PreAuthorize("@securityCheck.canCreate('Discount')")
    List<Discount> repeat(Expense original, Expense repeat, User owner);

    @PreAuthorize("@securityCheck.canAccess(#original)")
    Discount update(Discount original, Expense e, MDiscount edition);

    @PreAuthorize("@securityCheck.canAccess(#discount)")
    Discount delete(Discount discount, Expense e);

    @PreAuthorize("@securityCheck.canAccess(#e)")
    void deleteAll(Expense e, Instant deleted);

    Page<Discount> search(Expense e, Pageable page);

}
