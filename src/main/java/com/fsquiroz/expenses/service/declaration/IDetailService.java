package com.fsquiroz.expenses.service.declaration;

import com.fsquiroz.expenses.entity.db.*;
import com.fsquiroz.expenses.entity.json.MDetail;
import com.fsquiroz.expenses.entity.json.MGroupDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import java.math.BigDecimal;
import java.util.List;

public interface IDetailService extends IBuildiableService<Detail, MDetail> {

    @PostAuthorize("@securityCheck.canAccess(returnObject)")
    Detail get(Long id);

    BigDecimal getTotal(Expense e);

    @PreAuthorize("@securityCheck.canAccess(#e)")
    List<Detail> applyDiscounts(Expense e, BigDecimal discounts);

    @PreAuthorize("@securityCheck.canCreate('Detail')")
    List<Detail> create(MDetail md, Expense e, Category c, Product p, int repetitions, User owner);

    @PreAuthorize("@securityCheck.canCreate('Detail')")
    List<Detail> repeat(Expense original, Expense repeat, User owner);

    @PreAuthorize("@securityCheck.canAccess(#original)")
    Detail update(Detail original, Expense e, Category c, Product p, MDetail edition);

    @PreAuthorize("@securityCheck.canAccess(#detail)")
    Detail delete(Detail detail, Expense e);

    @PreAuthorize("@securityCheck.canAccess(#e)")
    List<MGroupDetail> totalsByCategory(Expense e);

    Page<Detail> search(Expense e, Pageable page);

    void consolidate(Product toKeep, Product toRemove);

    Page<Detail> search(Product p, Pageable page);

}
