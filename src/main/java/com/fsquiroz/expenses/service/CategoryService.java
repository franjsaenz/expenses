package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.Category;
import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MCategory;
import com.fsquiroz.expenses.entity.json.MGroup;
import com.fsquiroz.expenses.entity.json.MGroupDetail;
import com.fsquiroz.expenses.entity.util.DateRange;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.repository.CategoryRepository;
import com.fsquiroz.expenses.repository.StatRepository;
import com.fsquiroz.expenses.service.declaration.ICategoryService;
import com.fsquiroz.expenses.service.declaration.ICurrencyService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.List;

@Service
public class CategoryService implements ICategoryService {

    private ICurrencyService currencyService;

    private CategoryRepository categoryRepository;

    private StatRepository statRepository;

    public CategoryService(ICurrencyService currencyService, CategoryRepository categoryRepository, StatRepository statRepository) {
        this.currencyService = currencyService;
        this.categoryRepository = categoryRepository;
        this.statRepository = statRepository;
    }

    @Override
    public Category get(Long id) {
        if (id == null) {
            throw BadRequestException.byInvalidId();
        }
        return categoryRepository.findById(id).orElseThrow(() -> NotFoundException.byId(Category.class, id));
    }

    @Override
    public Category create(MCategory mc, User user) {
        if (mc.getName() == null || mc.getName().isEmpty()) {
            throw BadRequestException.byMissingParam("name");
        }
        Category c = new Category();
        c.setOwner(user);
        c.setCreated(Instant.now());
        c.setName(mc.getName());
        c.setDescription(mc.getDescription());
        return categoryRepository.save(c);
    }

    @Override
    public Category update(Category original, MCategory edition) {
        if (original.getDeleted() != null) {
            throw BadRequestException.byEditingDeleted(original);
        }
        if (edition.getName() == null || edition.getName().isEmpty()) {
            throw BadRequestException.byMissingParam("name");
        }
        original.setName(edition.getName());
        original.setDescription(edition.getDescription());
        original.setUpdated(Instant.now());
        return categoryRepository.save(original);
    }

    @Override
    public Category delete(Category category) {
        if (category.getDeleted() != null) {
            throw BadRequestException.byDeletingDeleted(category);
        }
        category.setDeleted(Instant.now());
        return categoryRepository.save(category);
    }

    @Override
    public Page<Category> search(String term, Pageable page, User user) {
        if (term == null || term.isEmpty()) {
            return categoryRepository.findByDeletedIsNullAndOwner(user, page);
        } else {
            return categoryRepository.search(term, user, page);
        }
    }

    @Override
    public MCategory build(Category domain) {
        return new MCategory(
                domain.getId(),
                domain.getCreated(),
                domain.getUpdated(),
                domain.getDeleted(),
                domain.getName(),
                domain.getDescription()
        );
    }

    @Override
    public MGroup historyByCategoryAndCurrency(Category category, Currency currency, Instant from, Instant to) {
        DateRange range = TimeUtils.dateRange(from, to);
        List<MGroupDetail> details = statRepository.historyByCategoryAndCurrency(category, currency, range.getFrom(), range.getTo());
        return buildGroup(details, category, currency, range.getFrom(), range.getTo());
    }

    private MGroup buildGroup(List<MGroupDetail> details, Category category, Currency currency, Instant from, Instant to) {
        MathContext mc = new MathContext(2, RoundingMode.HALF_DOWN);
        BigDecimal subtotal = new BigDecimal(0.0, mc);
        BigDecimal discounts = new BigDecimal(0.0, mc);
        for (MGroupDetail d : details) {
            subtotal = subtotal.add(d.getSubtotal());
            discounts = discounts.add(d.getDiscounts());
        }
        MGroup mg = new MGroup();
        mg.setFrom(from);
        mg.setTo(to);
        mg.setCurrency(currency != null ? currencyService.build(currency) : null);
        mg.setCategory(category != null ? build(category) : null);
        mg.setDetails(details);
        mg.setSubtotal(subtotal);
        mg.setDiscounts(discounts);
        mg.setTotal(subtotal.subtract(discounts));
        return mg;
    }

}
