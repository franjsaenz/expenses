package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MCurrency;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.repository.CurrencyRepository;
import com.fsquiroz.expenses.service.declaration.ICurrencyService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class CurrencyService implements ICurrencyService {

    private CurrencyRepository currencyRepository;

    public CurrencyService(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public Currency get(Long id) {
        if (id == null) {
            throw BadRequestException.byInvalidId();
        }
        return currencyRepository.findById(id).orElseThrow(() -> NotFoundException.byId(Currency.class, id));
    }

    @Override
    public Currency create(MCurrency mc) {
        validateParams(mc);
        Currency c = new Currency();
        c.setCreated(Instant.now());
        c.setName(mc.getName());
        c.setCode(mc.getCode());
        c.setIdentifier(mc.getIdentifier());
        return currencyRepository.save(c);
    }

    @Override
    public Currency update(Currency original, MCurrency edition) {
        if (original.getDeleted() != null) {
            throw BadRequestException.byEditingDeleted(original);
        }
        validateParams(edition);
        original.setUpdated(Instant.now());
        original.setName(edition.getName());
        original.setCode(edition.getCode());
        original.setIdentifier(edition.getIdentifier());
        return currencyRepository.save(original);
    }

    private void validateParams(MCurrency mc) {
        if (mc.getName() == null || mc.getName().isEmpty()) {
            throw BadRequestException.byMissingParam("name");
        }
        if (mc.getCode() == null || mc.getCode().isEmpty()) {
            throw BadRequestException.byMissingParam("code");
        }
        if (mc.getIdentifier() == null || mc.getIdentifier().isEmpty()) {
            throw BadRequestException.byMissingParam("identifier");
        }
    }

    @Override
    public Currency delete(Currency currency) {
        if (currency.getDeleted() != null) {
            throw BadRequestException.byDeletingDeleted(currency);
        }
        currency.setDeleted(Instant.now());
        return currencyRepository.save(currency);
    }

    @Override
    public Page<Currency> search(String term, Pageable page) {
        if (term == null || term.isEmpty()) {
            return currencyRepository.findByDeletedIsNull(page);
        } else {
            return currencyRepository.search(term, page);
        }
    }

    @Override
    public MCurrency build(Currency domain) {
        return new MCurrency(
                domain.getId(),
                domain.getCreated(),
                domain.getUpdated(),
                domain.getDeleted(),
                domain.getName(),
                domain.getIdentifier(),
                domain.getCode()
        );
    }

}
