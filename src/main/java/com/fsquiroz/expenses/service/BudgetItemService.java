package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.Budget;
import com.fsquiroz.expenses.entity.db.BudgetItem;
import com.fsquiroz.expenses.entity.db.Category;
import com.fsquiroz.expenses.entity.json.MBudgetItem;
import com.fsquiroz.expenses.entity.json.MCategory;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.repository.BudgetItemRepository;
import com.fsquiroz.expenses.service.declaration.IBudgetItemService;
import com.fsquiroz.expenses.service.declaration.ICategoryService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
public class BudgetItemService implements IBudgetItemService {

    private ICategoryService categoryService;

    private BudgetItemRepository budgetItemRepository;

    public BudgetItemService(ICategoryService categoryService, BudgetItemRepository budgetItemRepository) {
        this.categoryService = categoryService;
        this.budgetItemRepository = budgetItemRepository;
    }

    @Override
    public BudgetItem get(Long id) {
        if (id == null) {
            throw BadRequestException.byInvalidId();
        }
        return budgetItemRepository.findById(id).orElseThrow(() -> NotFoundException.byId(BudgetItem.class, id));
    }

    @Override
    public BudgetItem setIem(MBudgetItem mbi, Budget budget, Category category) {
        BudgetItem bi = budgetItemRepository.findByDeletedIsNullAndBudgetAndCategory(budget, category).orElse(null);
        if (bi == null) {
            bi = new BudgetItem();
            bi.setCreated(Instant.now());
            bi.setOwner(budget.getOwner());
            bi.setBudget(budget);
            bi.setCategory(category);
        } else {
            bi.setUpdated(Instant.now());
        }
        validate(bi, budget, category, mbi);
        return budgetItemRepository.save(bi);
    }

    private void validate(BudgetItem bi, Budget budget, Category category, MBudgetItem mbi) {
        if (budget.getDeleted() != null) {
            throw BadRequestException.byUsingDeleted(budget, bi);
        }
        if (category == null) {
            throw BadRequestException.byMissingParam("categoryId");
        } else if (category.getDeleted() != null) {
            throw BadRequestException.byUsingDeleted(category, bi);
        }
        if (mbi.getAmount() == null) {
            throw BadRequestException.byMissingParam("amount");
        }
        if (mbi.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            mbi.setAmount(mbi.getAmount().multiply(new BigDecimal(-1)));
        }
        bi.setAmount(mbi.getAmount().setScale(2, RoundingMode.HALF_DOWN));
    }

    @Override
    public List<BudgetItem> repeat(Budget original, Budget repeat) {
        List<BudgetItem> items = budgetItemRepository.findByDeletedIsNullAndBudget(original);
        List<BudgetItem> repeated = new ArrayList<>();
        Instant created = Instant.now();
        for (BudgetItem i : items) {
            if (i.getCategory() == null || i.getCategory().getDeleted() != null) {
                continue;
            }
            BudgetItem bi = new BudgetItem();
            bi.setCreated(created);
            bi.setOwner(i.getOwner());
            bi.setBudget(repeat);
            bi.setCategory(i.getCategory());
            bi.setAmount(i.getAmount().setScale(2, RoundingMode.HALF_DOWN));
            repeated.add(bi);
        }
        if (!repeated.isEmpty()) {
            budgetItemRepository.saveAll(repeated);
        }
        return repeated;
    }

    @Override
    public BudgetItem delete(BudgetItem budgetItem, Budget budget) {
        if (budgetItem.getBudget() != null && !budget.equals(budgetItem.getBudget())) {
            throw BadRequestException.byMismatchBudget(budgetItem, budget);
        }
        if (budgetItem.getDeleted() != null) {
            throw BadRequestException.byDeletingDeleted(budgetItem);
        }
        budgetItem.setDeleted(Instant.now());
        return budgetItemRepository.save(budgetItem);
    }

    @Override
    public List<BudgetItem> list(Budget budget) {
        return budgetItemRepository.findByDeletedIsNullAndBudget(budget);
    }

    @Override
    public Page<BudgetItem> search(Budget budget, Pageable page) {
        return budgetItemRepository.list(budget, page);
    }

    @Override
    public MBudgetItem build(BudgetItem domain) {
        MCategory mc = null;
        if (domain.getCategory() != null) {
            mc = categoryService.build(domain.getCategory());
        }
        MBudgetItem mbi = new MBudgetItem();
        mbi.setId(domain.getId());
        mbi.setCreated(domain.getCreated());
        mbi.setUpdated(domain.getUpdated());
        mbi.setDeleted(domain.getDeleted());
        mbi.setCategory(mc);
        mbi.setAmount(domain.getAmount());
        return mbi;
    }

}
