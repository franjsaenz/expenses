package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.Role;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MLogin;
import com.fsquiroz.expenses.entity.json.MResetPassword;
import com.fsquiroz.expenses.entity.json.MUser;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.exception.UnauthorizedException;
import com.fsquiroz.expenses.repository.UserRepository;
import com.fsquiroz.expenses.security.SecurityCheck;
import com.fsquiroz.expenses.service.declaration.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

@Service
@Slf4j
public class UserService implements IUserService {

    private SecurityCheck securityCheck;

    private PasswordEncoder passwordEncoder;

    private UserRepository userRepository;

    public UserService(
            SecurityCheck securityCheck,
            PasswordEncoder passwordEncoder,
            UserRepository userRepository
    ) {
        this.securityCheck = securityCheck;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    @Override
    public User login(MLogin login, Instant expiration, Instant notBefore) {
        if (login.getEmail() == null || login.getEmail().isEmpty()) {
            throw BadRequestException.byMissingParam("email");
        }
        if (login.getPassword() == null || login.getPassword().isEmpty()) {
            throw BadRequestException.byMissingParam("password");
        }
        UnauthorizedException ue = UnauthorizedException.byInvalidCredentials();
        User u;
        try {
            u = getByEmail(login.getEmail());
        } catch (NotFoundException nfe) {
            throw ue;
        }
        if (u.getRole() == Role.CLIENT || u.getRole() == null) {
            throw UnauthorizedException.byInsufficientPermission();
        }
        if (!passwordEncoder.matches(login.getPassword(), u.getPassword())) {
            throw ue;
        }
        if (u.getDeleted() != null) {
            throw UnauthorizedException.byDeletedUser(u);
        }
        if (expiration == null && notBefore == null && u.getRole() == Role.SUPER) {
            expiration = Instant.now().plus(30, ChronoUnit.MINUTES);
        } else if (expiration == null && u.getRole() == Role.SUPER) {
            expiration = notBefore.plus(30, ChronoUnit.MINUTES);
        } else if (expiration != null && notBefore != null && notBefore.isAfter(expiration)) {
            throw BadRequestException.byExpirationBefore(expiration, notBefore);
        }
        String token = securityCheck.generateToken(null, expiration, notBefore, u);
        u.setToken(token);
        u.setExpiration(expiration);
        u.setNotBefore(notBefore);
        return u;
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(() -> NotFoundException.byEmail(User.class, email));
    }

    @Override
    public User changePassword(User original, MResetPassword edit) {
        if (edit.getNewPassword() == null || edit.getNewPassword().isEmpty()) {
            throw BadRequestException.byMissingParam("newPassword");
        }
        if (edit.getOldPassword() == null || edit.getOldPassword().isEmpty()) {
            throw BadRequestException.byMissingParam("oldPassword");
        }
        if (original.getRole() == null || Role.CLIENT.equals(original.getRole())) {
            throw BadRequestException.byPasswordlessAccount(original);
        }
        if (!passwordEncoder.matches(edit.getOldPassword(), original.getPassword())) {
            throw UnauthorizedException.byInvalidCredentials();
        }
        original.setPassword(passwordEncoder.encode(edit.getNewPassword()));
        original.setUpdated(Instant.now());
        return userRepository.save(original);

    }

    @Override
    public User generateToken(User user, Instant expiration, Instant notBefore) {
        if (expiration != null && notBefore != null && notBefore.isAfter(expiration)) {
            throw BadRequestException.byExpirationBefore(expiration, notBefore);
        }
        if (user.getRole() == Role.SUPER) {
            throw BadRequestException.byPasswordlessTokenOnSuper(user);
        }
        String token = securityCheck.generateToken(securityCheck.getAuthentication(), expiration, notBefore, user);
        user.setToken(token);
        user.setExpiration(expiration);
        user.setNotBefore(notBefore);
        return user;
    }

    @Override
    public MUser build(User domain) {
        MUser mu = new MUser();
        mu.setId(domain.getId());
        mu.setCreated(domain.getCreated());
        mu.setUpdated(domain.getUpdated());
        mu.setDeleted(domain.getDeleted());
        mu.setEmail(domain.getEmail());
        mu.setRole(domain.getRole());
        mu.setZoneId(domain.getZoneId());
        mu.setDateFormat(domain.getDateFormat());
        mu.setToken(domain.getToken());
        mu.setExpiration(domain.getExpiration());
        mu.setNotBefore(domain.getNotBefore());
        return mu;
    }

    @Override
    public User create(MUser model) {
        boolean isClient;
        if (model.getRole() == null) {
            throw BadRequestException.byMissingParam("role");
        } else if (model.getRole() == Role.SUPER) {
            throw BadRequestException.byCreatingSuperUser();
        } else {
            isClient = model.getRole() == Role.CLIENT;
        }
        if (model.getEmail() == null || model.getEmail().isEmpty()) {
            throw BadRequestException.byMissingParam("email");
        }
        if (!isClient && (model.getPassword() == null || model.getPassword().isEmpty())) {
            throw BadRequestException.byMissingParam("password");
        }
        User u = null;
        String pass = null;
        try {
            u = getByEmail(model.getEmail());
            if (u != null && u.getDeleted() == null) {
                throw BadRequestException.byExistingEmail(model.getEmail());
            } else if (u != null && u.getDeleted() != null) {
                u.setDeleted(null);
                u.setUpdated(Instant.now());
            }
        } catch (NotFoundException e) {
            log.trace(e.getMessage(), e);
        }
        if (!isClient) {
            pass = passwordEncoder.encode(model.getPassword());
        }
        if (u == null) {
            u = new User();
            u.setCreated(Instant.now());
            u.setEmail(model.getEmail());
        }
        u.setPassword(pass);
        u.setRole(model.getRole());
        u.setDateFormat(model.getDateFormat());
        setZoneId(u, model);
        return userRepository.save(u);
    }

    @Override
    public User update(User original, MUser edit) {
        if (original.getDeleted() != null) {
            throw BadRequestException.bySuspendedUser(original);
        }
        setZoneId(original, edit);
        original.setDateFormat(edit.getDateFormat());
        original.setUpdated(Instant.now());
        return userRepository.save(original);
    }

    private void setZoneId(User u, MUser mu) {
        try {
            u.setZoneId(ZoneId.of(mu.getZoneId()).getId());
        } catch (Exception ignored) {
            u.setZoneId(ZoneOffset.UTC.getId());
        }
    }

    @Override
    public User delete(User entity) {
        if (entity.getRole() == Role.SUPER) {
            throw BadRequestException.byDeletingSuper();
        }
        if (entity.getDeleted() != null) {
            throw BadRequestException.bySuspendedUser(entity);
        }
        entity.setDeleted(Instant.now());
        return userRepository.save(entity);
    }

    @Override
    public User get(Long id) {
        if (id == null) {
            throw BadRequestException.byInvalidId();
        }
        return userRepository.findById(id).orElseThrow(() -> NotFoundException.byId(User.class, id));
    }

    @Override
    public Page<User> search(String term, Pageable pageable) {
        if (term == null || term.isEmpty()) {
            return userRepository.findByDeletedIsNull(pageable);
        } else {
            return userRepository.searchUsers(term, pageable);
        }
    }

}
