package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.*;
import com.fsquiroz.expenses.entity.json.MCategory;
import com.fsquiroz.expenses.entity.json.MDetail;
import com.fsquiroz.expenses.entity.json.MGroupDetail;
import com.fsquiroz.expenses.entity.json.MProduct;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.repository.DetailRepository;
import com.fsquiroz.expenses.repository.StatRepository;
import com.fsquiroz.expenses.service.declaration.ICategoryService;
import com.fsquiroz.expenses.service.declaration.IDetailService;
import com.fsquiroz.expenses.service.declaration.IProductService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DetailService implements IDetailService {

    private final int consolidationChunkSize;
    private final ICategoryService categoryService;

    private final IProductService productService;

    private final DetailRepository detailRepository;

    private final StatRepository statRepository;

    public DetailService(
            @Value("${detail.consolidation.chunkSize:20}") int consolidationChunkSize,
            ICategoryService categoryService,
            IProductService productService,
            DetailRepository detailRepository,
            StatRepository statRepository
    ) {
        this.consolidationChunkSize = consolidationChunkSize;
        this.categoryService = categoryService;
        this.productService = productService;
        this.detailRepository = detailRepository;
        this.statRepository = statRepository;
    }

    @Override
    public Detail get(Long id) {
        if (id == null) {
            throw BadRequestException.byInvalidId();
        }
        return detailRepository.findById(id).orElseThrow(() -> NotFoundException.byId(Detail.class, id));
    }

    @Override
    public BigDecimal getTotal(Expense e) {
        BigDecimal sum = detailRepository.getTotal(e);
        return sum == null ? new BigDecimal(0) : sum;
    }

    @Override
    public List<Detail> applyDiscounts(Expense e, BigDecimal discounts) {
        BigDecimal subtotal = BigDecimal.valueOf(0.00).setScale(2, RoundingMode.HALF_DOWN);
        List<Detail> details = detailRepository.findByDeletedIsNullAndExpense(e);
        if (details.isEmpty()) {
            return details;
        }
        for (Detail d : details) {
            if (!d.isWithoutDiscount()) {
                subtotal = subtotal.add(d.getAmount());
            }
        }
        BigDecimal ratio;
        if (subtotal.compareTo(BigDecimal.ZERO) == 0) {
            ratio = BigDecimal.valueOf(0.00).setScale(2, RoundingMode.HALF_DOWN);
        } else {
            ratio = subtotal.subtract(discounts).divide(subtotal, new MathContext(10, RoundingMode.HALF_DOWN));
        }
        BigDecimal sum = discounts.setScale(2, RoundingMode.HALF_DOWN);
        for (Detail d : details) {
            if (d.isWithoutDiscount()) {
                continue;
            }
            BigDecimal detailAmount = d.getAmount();
            BigDecimal detailSubtotal = detailAmount.multiply(ratio).setScale(2, RoundingMode.HALF_DOWN);
            BigDecimal detailDiscount = detailAmount.subtract(detailSubtotal).setScale(2, RoundingMode.HALF_DOWN);
            sum = sum.subtract(detailDiscount).setScale(2, RoundingMode.HALF_DOWN);
            d.setDiscount(detailDiscount);
            d.setSubtotal(detailSubtotal);
        }
        if (sum.compareTo(BigDecimal.ZERO) != 0) {
            Detail d = details.get(0);
            BigDecimal detailAmount = d.getAmount();
            BigDecimal detailDiscount = d.getDiscount().add(sum);
            BigDecimal detailSubtotal = detailAmount.subtract(detailDiscount);
            d.setDiscount(detailDiscount);
            d.setSubtotal(detailSubtotal);
        }
        return detailRepository.saveAll(details);
    }

    @Override
    public List<Detail> create(MDetail md, Expense e, Category c, Product p, int repetitions, User owner) {
        Detail detail = validate(new Detail(), md, e, c, p);
        detail.setCreated(Instant.now());
        detail.setExpense(e);
        detail.setOwner(owner);
        List<Detail> details = new ArrayList<>();
        for (int i = 0; i < repetitions; i++) {
            Detail d = new Detail();
            d.setCreated(detail.getCreated());
            d.setOwner(detail.getOwner());
            d.setExpense(detail.getExpense());
            d.setProduct(detail.getProduct());
            d.setCategory(detail.getCategory());
            d.setAmount(detail.getAmount());
            d.setDiscount(detail.getDiscount());
            d.setSubtotal(detail.getSubtotal());
            d.setDescription(detail.getDescription());
            d.setWithoutDiscount(detail.isWithoutDiscount());
            details.add(d);
        }
        return detailRepository.saveAll(details);
    }

    @Override
    public List<Detail> repeat(Expense original, Expense repeat, User owner) {
        List<Detail> originalDetails = detailRepository.findByDeletedIsNullAndExpense(original);
        List<Detail> repeated = new ArrayList<>();
        for (Detail d : originalDetails) {
            if (d.getProduct() == null || d.getProduct().getDeleted() != null) {
                continue;
            }
            Category c = d.getCategory() != null ? d.getCategory() : d.getProduct().getCategory();
            Detail detail = new Detail();
            detail.setCreated(Instant.now());
            detail.setOwner(owner);
            detail.setExpense(repeat);
            detail.setProduct(d.getProduct());
            detail.setCategory(c);
            detail.setAmount(d.getAmount().setScale(2, RoundingMode.HALF_DOWN));
            detail.setDiscount(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_DOWN));
            detail.setSubtotal(d.getAmount().setScale(2, RoundingMode.HALF_DOWN));
            detail.setDescription(d.getDescription());
            detail.setWithoutDiscount(d.isWithoutDiscount());
            repeated.add(detail);
        }
        if (!repeated.isEmpty()) {
            detailRepository.saveAll(repeated);
        }
        return repeated;
    }

    @Override
    public Detail update(Detail original, Expense e, Category c, Product p, MDetail edition) {
        validate(original, edition, e, c, p);
        original.setUpdated(Instant.now());
        return detailRepository.save(original);
    }

    private Detail validate(Detail detail, MDetail md, Expense e, Category c, Product p) {
        deletedValidation(detail, e, c, p);
        if (md.getAmount() == null) {
            throw BadRequestException.byMissingParam("amount");
        }
        if (md.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            md.setAmount(md.getAmount().multiply(new BigDecimal(-1)));
        }
        if (c == null) {
            c = p.getCategory();
        }
        detail.setProduct(p);
        detail.setCategory(c);
        detail.setAmount(md.getAmount().setScale(2, RoundingMode.HALF_DOWN));
        detail.setDiscount(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_DOWN));
        detail.setSubtotal(md.getAmount().setScale(2, RoundingMode.HALF_DOWN));
        detail.setDescription(md.getDescription());
        detail.setWithoutDiscount(md.isWithoutDiscount());
        return detail;
    }

    private void deletedValidation(Detail d, Expense e, Category c, Product p) {
        if (e.getDeleted() != null) {
            throw BadRequestException.byUsingDeleted(e, d);
        }
        if (d.getDeleted() != null) {
            throw BadRequestException.byEditingDeleted(d);
        }
        if (p != null && p.getDeleted() != null) {
            throw BadRequestException.byUsingDeleted(p, d);
        }
        if (c != null && c.getDeleted() != null) {
            throw BadRequestException.byUsingDeleted(c, d);
        }
        if (d.getExpense() != null && !d.getExpense().equals(e)) {
            throw BadRequestException.byMissmatchExpense(d, e);
        }
        if (e.getStatus() != null && e.getStatus() != ExpenseStatus.OPENED) {
            throw BadRequestException.byNotOpenedExpense(e);
        }
    }

    @Override
    public Detail delete(Detail detail, Expense e) {
        deletedValidation(detail, e, null, null);
        detail.setDeleted(Instant.now());
        return detailRepository.save(detail);
    }

    @Override
    public List<MGroupDetail> totalsByCategory(Expense e) {
        return statRepository.totalsByCategoryAndCurrency(e);
    }

    @Override
    public Page<Detail> search(Expense e, Pageable page) {
        page = SortUtils.defaultSorting(page, Sort.Direction.DESC, "created");
        if (e == null) {
            return detailRepository.search(page);
        } else {
            return detailRepository.search(e, page);
        }
    }

    @Override
    public Page<Detail> search(Product p, Pageable page) {
        if (p == null) {
            return detailRepository.search(page);
        } else {
            return detailRepository.search(p, page);
        }
    }

    @Override
    public void consolidate(Product toKeep, Product toRemove) {
        Page<Detail> details = detailRepository.search(toRemove, PageRequest.of(0, consolidationChunkSize));
        while (details != null && !details.isEmpty()) {
            detailRepository.saveAll(
                    details.stream()
                            .peek(d -> d.setProduct(toKeep))
                            .collect(Collectors.toList())
            );
            details = details.hasNext() ? detailRepository.search(toRemove, details.nextPageable()) : null;
        }
    }

    @Override
    public MDetail build(Detail domain) {
        MProduct mp = null;
        if (domain.getProduct() != null) {
            mp = productService.build(domain.getProduct());
        }
        MCategory mc = null;
        if (domain.getCategory() != null) {
            mc = categoryService.build(domain.getCategory());
        }
        MDetail md = new MDetail();
        md.setId(domain.getId());
        md.setProduct(mp);
        md.setCategory(mc);
        md.setCreated(domain.getCreated());
        md.setUpdated(domain.getUpdated());
        md.setDeleted(domain.getDeleted());
        md.setAmount(domain.getAmount());
        md.setDiscount(domain.getDiscount());
        md.setSubtotal(domain.getSubtotal());
        md.setDescription(domain.getDescription());
        md.setWithoutDiscount(domain.isWithoutDiscount());
        return md;
    }

}
