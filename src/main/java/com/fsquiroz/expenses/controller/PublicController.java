package com.fsquiroz.expenses.controller;

import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MException;
import com.fsquiroz.expenses.entity.json.MLogin;
import com.fsquiroz.expenses.entity.json.MStatus;
import com.fsquiroz.expenses.entity.json.MUser;
import com.fsquiroz.expenses.mapper.StatusMapper;
import com.fsquiroz.expenses.security.SecurityCheck;
import com.fsquiroz.expenses.service.declaration.IUserService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;

@RestController
@RequestMapping("/")
public class PublicController {

    private MStatus status;

    private SecurityCheck securityCheck;

    private IUserService userService;

    private StatusMapper statusMapper;

    public PublicController(MStatus status, SecurityCheck securityCheck, IUserService userService, StatusMapper statusMapper) {
        this.status = status;
        this.securityCheck = securityCheck;
        this.userService = userService;
        this.statusMapper = statusMapper;
    }

    @GetMapping
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MStatus.class)
    })
    public ResponseEntity<?> index() {
        MStatus status = statusMapper.map(this.status);
        return ResponseEntity.ok(status);
    }

    @PostMapping("/login")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MUser.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class)
    })
    public ResponseEntity<?> login(
            @RequestBody MLogin login,
            @RequestParam(required = false) Instant expiration,
            @RequestParam(required = false) Instant notBefore
    ) {
        User u = userService.login(login, expiration, notBefore);
        return ResponseEntity.ok(userService.build(u));
    }

    @GetMapping("/login")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MUser.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class)
    })
    public ResponseEntity<?> loggedIn() {
        User u = securityCheck.getAuthentication();
        return ResponseEntity.ok(userService.build(u));
    }

}
