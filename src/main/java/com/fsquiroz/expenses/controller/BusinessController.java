package com.fsquiroz.expenses.controller;

import com.fsquiroz.expenses.entity.db.Business;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MBusiness;
import com.fsquiroz.expenses.entity.json.MException;
import com.fsquiroz.expenses.entity.json.MResponse;
import com.fsquiroz.expenses.security.SecurityCheck;
import com.fsquiroz.expenses.service.declaration.IBusinessService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/businesses")
public class BusinessController {

    private SecurityCheck securityCheck;

    private IBusinessService businessService;

    public BusinessController(SecurityCheck securityCheck, IBusinessService businessService) {
        this.securityCheck = securityCheck;
        this.businessService = businessService;
    }

    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> search(@RequestParam(required = false) String term, @ApiIgnore Pageable pageable) {
        User u = securityCheck.getAuthentication();
        Page<Business> businesses = businessService.search(term, pageable, u);
        return ResponseEntity.ok(businessService.build(businesses));
    }

    @PostMapping
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = MBusiness.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> create(@RequestBody MBusiness business) {
        User u = securityCheck.getAuthentication();
        Business b = businessService.create(business, u);
        return new ResponseEntity(businessService.build(b), HttpStatus.CREATED);
    }

    @GetMapping("/{businessId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MBusiness.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> get(@PathVariable Long businessId) {
        Business b = businessService.get(businessId);
        return ResponseEntity.ok(businessService.build(b));
    }

    @PutMapping("/{businessId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MBusiness.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> update(@PathVariable Long businessId, @RequestBody MBusiness business) {
        Business b = businessService.get(businessId);
        b = businessService.update(b, business);
        return ResponseEntity.ok(businessService.build(b));
    }

    @DeleteMapping("/{businessId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> delete(@PathVariable Long businessId) {
        Business b = businessService.get(businessId);
        businessService.delete(b);
        return ResponseEntity.ok(MResponse.of("Business deleted"));
    }

}
