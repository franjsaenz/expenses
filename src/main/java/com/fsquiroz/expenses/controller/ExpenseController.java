package com.fsquiroz.expenses.controller;

import com.fsquiroz.expenses.entity.db.*;
import com.fsquiroz.expenses.entity.json.*;
import com.fsquiroz.expenses.security.SecurityCheck;
import com.fsquiroz.expenses.service.declaration.*;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/expenses")
public class ExpenseController {

    private SecurityCheck securityCheck;

    private IExpenseService expenseService;

    private IDiscountService discountService;

    private IDetailService detailService;

    private ICategoryService categoryService;

    private ICurrencyService currencyService;

    private IExchangeRateService exchangeRateService;

    private IBusinessService businessService;

    private IProductService productService;

    public ExpenseController(
            SecurityCheck securityCheck,
            IExpenseService expenseService,
            IDiscountService discountService,
            IDetailService detailService,
            ICategoryService categoryService,
            ICurrencyService currencyService,
            IExchangeRateService exchangeRateService,
            IBusinessService businessService,
            IProductService productService
    ) {
        this.securityCheck = securityCheck;
        this.expenseService = expenseService;
        this.discountService = discountService;
        this.detailService = detailService;
        this.categoryService = categoryService;
        this.currencyService = currencyService;
        this.exchangeRateService = exchangeRateService;
        this.businessService = businessService;
        this.productService = productService;
    }

    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> search(
            @RequestParam(required = false) Long businessId,
            @RequestParam(required = false) Long currencyId,
            @ApiIgnore Pageable pageable
    ) {
        User u = securityCheck.getAuthentication();
        Business b = null;
        Currency c = null;
        if (businessId != null) {
            b = businessService.get(businessId);
        }
        if (currencyId != null) {
            c = currencyService.get(currencyId);
        }
        Page<Expense> expenses = expenseService.search(b, c, pageable, u);
        return ResponseEntity.ok(expenseService.build(expenses));
    }

    @PostMapping
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = MExpense.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> create(
            @RequestBody MExpense expense,
            @RequestParam Long businessId,
            @RequestParam(required = false) Long currencyId,
            @RequestParam(required = false) Long exchangeRateId,
            @RequestParam(required = false) Long originalId
    ) {
        User u = securityCheck.getAuthentication();
        Business b = businessService.get(businessId);
        Currency c = null;
        ExchangeRate er = null;
        if (currencyId != null) {
            c = currencyService.get(currencyId);
        }
        if (exchangeRateId != null) {
            er = exchangeRateService.get(exchangeRateId);
        }
        Expense e;
        Expense original = null;
        if (originalId != null) {
            original = expenseService.get(originalId);
            e = expenseService.repeat(expense, original, b, c, er, u);
        } else {
            e = expenseService.create(expense, b, c, er, u);
        }
        return new ResponseEntity(expenseService.build(e, original != null), HttpStatus.CREATED);
    }

    @GetMapping("/{expenseId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MExpense.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> get(
            @PathVariable Long expenseId,
            @RequestParam(required = false, defaultValue = "true") boolean extended
    ) {
        Expense e = expenseService.get(expenseId);
        return ResponseEntity.ok(expenseService.build(e, extended));
    }

    @PutMapping("/{expenseId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MExpense.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> update(
            @PathVariable Long expenseId,
            @RequestBody MExpense expense,
            @RequestParam Long businessId,
            @RequestParam(required = false) Long currencyId,
            @RequestParam(required = false) Long exchangeRateId,
            @RequestParam(required = false, defaultValue = "false") boolean extended
    ) {
        Business b = businessService.get(businessId);
        Currency c = null;
        ExchangeRate er = null;
        if (currencyId != null) {
            c = currencyService.get(currencyId);
        }
        if (exchangeRateId != null) {
            er = exchangeRateService.get(exchangeRateId);
        }
        Expense e = expenseService.get(expenseId);
        e = expenseService.update(e, b, c, er, expense);
        return ResponseEntity.ok(expenseService.build(e, extended));
    }

    @PutMapping("/{expenseId}/status")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MExpense.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> updateStatus(
            @PathVariable Long expenseId,
            @RequestParam ExpenseStatus status,
            @RequestParam(required = false, defaultValue = "false") boolean extended
    ) {
        Expense e = expenseService.get(expenseId);
        e = expenseService.updateStatus(e, status);
        return ResponseEntity.ok(expenseService.build(e, extended));
    }

    @DeleteMapping("/{expenseId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> delete(@PathVariable Long expenseId) {
        Expense e = expenseService.get(expenseId);
        expenseService.delete(e);
        return ResponseEntity.ok(MResponse.of("Expense deleted"));
    }

    @GetMapping("/{expenseId}/discounts")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> listDiscounts(
            @PathVariable Long expenseId,
            @ApiIgnore Pageable pageable
    ) {
        Expense e = expenseService.get(expenseId);
        Page<Discount> discounts = discountService.search(e, pageable);
        return ResponseEntity.ok(discountService.build(discounts));
    }

    @PostMapping("/{expenseId}/discounts")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = MDiscount.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> addDiscount(
            @PathVariable Long expenseId,
            @RequestBody MDiscount discount
    ) {
        User u = securityCheck.getAuthentication();
        Expense e = expenseService.get(expenseId);
        Discount d = discountService.create(discount, e, u);
        expenseService.updateAmounts(e);
        return new ResponseEntity(discountService.build(d), HttpStatus.CREATED);
    }

    @PutMapping("/{expenseId}/discounts/{discountId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MDiscount.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> updateDiscount(
            @PathVariable Long expenseId,
            @PathVariable Long discountId,
            @RequestBody MDiscount discount
    ) {
        Discount d = discountService.get(discountId);
        Expense e = expenseService.get(expenseId);
        d = discountService.update(d, e, discount);
        expenseService.updateAmounts(e);
        return ResponseEntity.ok(discountService.build(d));
    }

    @DeleteMapping("/{expenseId}/discounts/{discountId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> deleteDiscount(
            @PathVariable Long expenseId,
            @PathVariable Long discountId
    ) {
        Discount d = discountService.get(discountId);
        Expense e = expenseService.get(expenseId);
        d = discountService.delete(d, e);
        expenseService.updateAmounts(e);
        return ResponseEntity.ok(MResponse.of("Discount deleted"));
    }

    @GetMapping("/{expenseId}/details")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> listDetails(
            @PathVariable Long expenseId,
            @ApiIgnore Pageable pageable
    ) {
        Expense e = expenseService.get(expenseId);
        Page<Detail> details = detailService.search(e, pageable);
        return ResponseEntity.ok(detailService.build(details));
    }

    @PostMapping("/{expenseId}/details")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = MDetail.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> addDetail(
            @PathVariable Long expenseId,
            @RequestParam Long productId,
            @RequestParam(required = false) Long categoryId,
            @RequestParam(required = false, defaultValue = "1") int repetitions,
            @RequestBody MDetail detail
    ) {
        User u = securityCheck.getAuthentication();
        Expense e = expenseService.get(expenseId);
        Product p = productService.get(productId);
        Category c = null;
        if (categoryId != null) {
            c = categoryService.get(categoryId);
        }
        detailService.create(detail, e, c, p, repetitions < 1 ? 1 : repetitions, u);
        expenseService.updateAmounts(e);
        return new ResponseEntity<>(MResponse.of("Details created"), HttpStatus.CREATED);
    }

    @PutMapping("/{expenseId}/details/{detailId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MDetail.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> updateDetail(
            @PathVariable Long expenseId,
            @PathVariable Long detailId,
            @RequestParam Long productId,
            @RequestParam(required = false) Long categoryId,
            @RequestBody MDetail detail
    ) {
        Detail d = detailService.get(detailId);
        Expense e = expenseService.get(expenseId);
        Product p = productService.get(productId);
        Category c = null;
        if (categoryId != null) {
            c = categoryService.get(categoryId);
        }
        d = detailService.update(d, e, c, p, detail);
        expenseService.updateAmounts(e);
        return ResponseEntity.ok(detailService.build(d));
    }

    @DeleteMapping("/{expenseId}/details/{detailId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> deleteDetail(
            @PathVariable Long expenseId,
            @PathVariable Long detailId
    ) {
        Detail d = detailService.get(detailId);
        Expense e = expenseService.get(expenseId);
        d = detailService.delete(d, e);
        expenseService.updateAmounts(e);
        return ResponseEntity.ok(MResponse.of("Detail deleted"));
    }

}
