package com.fsquiroz.expenses.controller;

import com.fsquiroz.expenses.entity.db.*;
import com.fsquiroz.expenses.entity.json.MException;
import com.fsquiroz.expenses.entity.json.MGroup;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.security.SecurityCheck;
import com.fsquiroz.expenses.service.declaration.IBusinessService;
import com.fsquiroz.expenses.service.declaration.ICategoryService;
import com.fsquiroz.expenses.service.declaration.ICurrencyService;
import com.fsquiroz.expenses.service.declaration.IProductService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

@RestController
@RequestMapping("/stats")
public class StatController {

    private SecurityCheck securityCheck;

    private IBusinessService businessService;

    private ICategoryService categoryService;

    private ICurrencyService currencyService;

    private IProductService productService;

    public StatController(SecurityCheck securityCheck, IBusinessService businessService, ICategoryService categoryService, ICurrencyService currencyService, IProductService productService) {
        this.securityCheck = securityCheck;
        this.businessService = businessService;
        this.categoryService = categoryService;
        this.currencyService = currencyService;
        this.productService = productService;
    }

    @GetMapping("/categories/history")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MGroup.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> historyByCategoryAndCurrency(
            @RequestParam Long categoryId,
            @RequestParam(required = false) Long currencyId,
            @RequestParam(required = false) Instant from,
            @RequestParam(required = false) Instant to
    ) {
        Currency c = getCurrencyOrDefault(currencyId);
        Category category = categoryService.get(categoryId);
        return ResponseEntity.ok(categoryService.historyByCategoryAndCurrency(category, c, from, to));
    }

    @GetMapping("/businesses/totals")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MGroup.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> totalsByCategoryAndCurrency(
            @RequestParam(required = false) Long businessId,
            @RequestParam(required = false) Long currencyId,
            @RequestParam(required = false) Instant from,
            @RequestParam(required = false) Instant to
    ) {
        Currency c = getCurrencyOrDefault(currencyId);
        if (businessId != null) {
            Business b = businessService.get(businessId);
            return ResponseEntity.ok(businessService.totalsByBusinessAndCurrency(b, c, from, to));
        } else {
            return ResponseEntity.ok(businessService.totalsByBusiness(c, from, to, securityCheck.getAuthentication()));
        }
    }

    @GetMapping("/products")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MGroup.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> totalsByProductAndCurrency(
            @RequestParam Long productId,
            @RequestParam(required = false) Long currencyId,
            @RequestParam(required = false) Instant from,
            @RequestParam(required = false) Instant to
    ) {
        Product p = productService.get(productId);
        Currency c = getCurrencyOrDefault(currencyId);
        return ResponseEntity.ok(productService.historyByProductAndCurrency(p, c, from, to));
    }

    private Currency getCurrencyOrDefault(Long currencyId) {
        Currency c = null;
        if (currencyId != null) {
            c = currencyService.get(currencyId);
        }
        if (c == null) {
            throw BadRequestException.byMissingParam("currencyId");
        }
        return c;
    }

}
