package com.fsquiroz.expenses.controller;

import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MException;
import com.fsquiroz.expenses.entity.json.MResetPassword;
import com.fsquiroz.expenses.entity.json.MResponse;
import com.fsquiroz.expenses.entity.json.MUser;
import com.fsquiroz.expenses.service.declaration.IUserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.time.Instant;

@RestController
@RequestMapping("/users")
public class UserController {

    private IUserService userService;

    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> search(@RequestParam(required = false) String term, @ApiIgnore Pageable pageable) {
        Page<User> users = userService.search(term, pageable);
        return ResponseEntity.ok(userService.build(users));
    }

    @PostMapping
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = MUser.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> create(@RequestBody MUser user) {
        User u = userService.create(user);
        return new ResponseEntity<>(userService.build(u), HttpStatus.CREATED);
    }

    @GetMapping("/{userId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MUser.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @PreAuthorize("isAuthenticated() and (hasRole('SUPER') or principal.id == #userId)")
    public ResponseEntity<?> get(@PathVariable Long userId) {
        User u = userService.get(userId);
        return ResponseEntity.ok(userService.build(u));
    }

    @PutMapping("/{userId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MUser.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> update(@PathVariable Long userId, @RequestBody MUser user) {
        User u = userService.get(userId);
        u = userService.update(u, user);
        return ResponseEntity.ok(userService.build(u));
    }

    @DeleteMapping("/{userId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MUser.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> delete(@PathVariable Long userId) {
        User u = userService.get(userId);
        u = userService.delete(u);
        return ResponseEntity.ok(userService.build(u));
    }

    @PutMapping("/{userId}/password")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> updatePassword(@PathVariable Long userId, @RequestBody MResetPassword password) {
        User u = userService.get(userId);
        userService.changePassword(u, password);
        return ResponseEntity.ok(MResponse.of("Password updated"));
    }

    @PostMapping("/{userId}/token")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MUser.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> generateToken(
            @PathVariable Long userId,
            @RequestParam(required = false) Instant expiration,
            @RequestParam(required = false) Instant notBefore
    ) {
        User u = userService.get(userId);
        u = userService.generateToken(u, expiration, notBefore);
        return ResponseEntity.ok(userService.build(u));
    }

}
