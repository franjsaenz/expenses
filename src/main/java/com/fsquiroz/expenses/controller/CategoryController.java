package com.fsquiroz.expenses.controller;

import com.fsquiroz.expenses.entity.db.Category;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MCategory;
import com.fsquiroz.expenses.entity.json.MException;
import com.fsquiroz.expenses.entity.json.MResponse;
import com.fsquiroz.expenses.security.SecurityCheck;
import com.fsquiroz.expenses.service.declaration.ICategoryService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private SecurityCheck securityCheck;

    private ICategoryService categoryService;

    public CategoryController(SecurityCheck securityCheck, ICategoryService categoryService) {
        this.securityCheck = securityCheck;
        this.categoryService = categoryService;
    }

    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> search(@RequestParam(required = false) String term, @ApiIgnore Pageable pageable) {
        User u = securityCheck.getAuthentication();
        Page<Category> categories = categoryService.search(term, pageable, u);
        return ResponseEntity.ok(categoryService.build(categories));
    }

    @PostMapping
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = MCategory.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> create(@RequestBody MCategory category) {
        User u = securityCheck.getAuthentication();
        Category c = categoryService.create(category, u);
        return new ResponseEntity(categoryService.build(c), HttpStatus.CREATED);
    }

    @GetMapping("/{categoryId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MCategory.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> get(@PathVariable Long categoryId) {
        Category c = categoryService.get(categoryId);
        return ResponseEntity.ok(categoryService.build(c));
    }

    @PutMapping("/{categoryId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MCategory.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> update(@PathVariable Long categoryId, @RequestBody MCategory category) {
        Category c = categoryService.get(categoryId);
        c = categoryService.update(c, category);
        return ResponseEntity.ok(categoryService.build(c));
    }

    @DeleteMapping("/{categoryId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> delete(@PathVariable Long categoryId) {
        Category c = categoryService.get(categoryId);
        categoryService.delete(c);
        return ResponseEntity.ok(MResponse.of("Category deleted"));
    }

}
