package com.fsquiroz.expenses.controller;

import com.fsquiroz.expenses.entity.db.*;
import com.fsquiroz.expenses.entity.json.*;
import com.fsquiroz.expenses.security.SecurityCheck;
import com.fsquiroz.expenses.service.declaration.IBudgetItemService;
import com.fsquiroz.expenses.service.declaration.IBudgetService;
import com.fsquiroz.expenses.service.declaration.ICategoryService;
import com.fsquiroz.expenses.service.declaration.ICurrencyService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.time.Instant;

@RestController
@RequestMapping("/budgets")
public class BudgetController {

    private SecurityCheck securityCheck;

    private IBudgetService budgetService;

    private IBudgetItemService budgetItemService;

    private ICategoryService categoryService;

    private ICurrencyService currencyService;

    public BudgetController(SecurityCheck securityCheck, IBudgetService budgetService, IBudgetItemService budgetItemService, ICategoryService categoryService, ICurrencyService currencyService) {
        this.securityCheck = securityCheck;
        this.budgetService = budgetService;
        this.budgetItemService = budgetItemService;
        this.categoryService = categoryService;
        this.currencyService = currencyService;
    }

    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> search(
            @RequestParam(required = false) String term,
            @RequestParam(required = false) Instant within,
            @ApiIgnore Pageable pageable
    ) {
        User u = securityCheck.getAuthentication();
        Page<Budget> budgets = budgetService.search(term, within, pageable, u);
        return ResponseEntity.ok(budgetService.build(budgets));
    }

    @PostMapping
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = MBudget.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> create(
            @RequestBody MBudget mb,
            @RequestParam(required = false) Long currencyId,
            @RequestParam(required = false) Long originalId
    ) {
        User u = securityCheck.getAuthentication();
        Currency c = currencyService.get(currencyId);
        Budget budget;
        if (originalId != null) {
            Budget original = budgetService.get(originalId);
            budget = budgetService.repeat(mb, original, c, u);
        } else {
            budget = budgetService.create(mb, c, u);
        }
        return new ResponseEntity<>(budgetService.build(budget), HttpStatus.CREATED);
    }

    @GetMapping("/{budgetId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MBudget.class),
            @ApiResponse(code = 200, message = "Ok", response = MBudgetReport.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> get(
            @PathVariable Long budgetId,
            @RequestParam(required = false, defaultValue = "false") boolean expanded
    ) {
        Budget budget = budgetService.get(budgetId);
        if (expanded) {
            return ResponseEntity.ok(budgetService.report(budget, securityCheck.getAuthentication()));
        } else {
            return ResponseEntity.ok(budgetService.build(budget));
        }
    }

    @PutMapping("/{budgetId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MBudget.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> update(
            @PathVariable Long budgetId,
            @RequestBody MBudget mb,
            @RequestParam(required = false) Long currencyId
    ) {
        Budget budget = budgetService.get(budgetId);
        Currency c = currencyService.get(currencyId);
        budget = budgetService.update(budget, mb, c);
        return ResponseEntity.ok(budgetService.build(budget));
    }

    @DeleteMapping("/{budgetId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> delete(@PathVariable Long budgetId) {
        Budget budget = budgetService.get(budgetId);
        budgetService.delete(budget);
        return ResponseEntity.ok(MResponse.of("Budget deleted"));
    }

    @GetMapping("/{budgetId}/items")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> items(
            @PathVariable Long budgetId,
            @ApiIgnore Pageable pageable
    ) {
        Budget budget = budgetService.get(budgetId);
        Page<BudgetItem> items = budgetItemService.search(budget, pageable);
        return ResponseEntity.ok(budgetItemService.build(items));
    }

    @PostMapping("/{budgetId}/items")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = MBudgetItem.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> createItem(
            @PathVariable Long budgetId,
            @RequestBody MBudgetItem budgetItem,
            @RequestParam Long categoryId
    ) {
        Budget budget = budgetService.get(budgetId);
        Category c = categoryService.get(categoryId);
        BudgetItem bi = budgetItemService.setIem(budgetItem, budget, c);
        return new ResponseEntity<>(budgetItemService.build(bi), bi.getUpdated() == null ? HttpStatus.CREATED : HttpStatus.OK);
    }

    @DeleteMapping("/{budgetId}/items/{budgetItemId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MBudgetItem.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> deleteItem(
            @PathVariable Long budgetId,
            @PathVariable Long budgetItemId
    ) {
        Budget budget = budgetService.get(budgetId);
        BudgetItem bi = budgetItemService.get(budgetItemId);
        budgetItemService.delete(bi, budget);
        return ResponseEntity.ok(MResponse.of("Budge item deleted"));
    }

}
