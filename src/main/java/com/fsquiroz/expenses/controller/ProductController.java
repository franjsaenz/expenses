package com.fsquiroz.expenses.controller;

import com.fsquiroz.expenses.entity.db.Category;
import com.fsquiroz.expenses.entity.db.Product;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MException;
import com.fsquiroz.expenses.entity.json.MProduct;
import com.fsquiroz.expenses.entity.json.MResponse;
import com.fsquiroz.expenses.security.SecurityCheck;
import com.fsquiroz.expenses.service.declaration.ICategoryService;
import com.fsquiroz.expenses.service.declaration.IDetailService;
import com.fsquiroz.expenses.service.declaration.IProductService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/products")
public class ProductController {

    private final SecurityCheck securityCheck;

    private final ICategoryService categoryService;

    private final IProductService productService;

    private final IDetailService detailService;

    public ProductController(
            SecurityCheck securityCheck,
            IProductService productService,
            ICategoryService categoryService,
            IDetailService detailService
    ) {
        this.securityCheck = securityCheck;
        this.productService = productService;
        this.categoryService = categoryService;
        this.detailService = detailService;
    }

    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> search(
            @RequestParam(required = false) Long categoryId,
            @RequestParam(required = false) String term,
            @ApiIgnore Pageable pageable
    ) {
        User u = securityCheck.getAuthentication();
        Category category = null;
        if (categoryId != null) {
            category = categoryService.get(categoryId);
        }
        Page<Product> products = productService.search(category, term, pageable, u);
        return ResponseEntity.ok(productService.build(products));
    }

    @PostMapping
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = MProduct.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> create(
            @RequestBody MProduct product,
            @RequestParam Long categoryId
    ) {
        User u = securityCheck.getAuthentication();
        Category category = categoryService.get(categoryId);
        Product p = productService.create(product, category, u);
        return new ResponseEntity(productService.build(p), HttpStatus.CREATED);
    }

    @GetMapping("/{productId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MProduct.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> get(@PathVariable Long productId) {
        Product product = productService.get(productId);
        return ResponseEntity.ok(productService.build(product));
    }

    @PutMapping("/{productId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MProduct.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> update(
            @PathVariable Long productId,
            @RequestBody MProduct product,
            @RequestParam Long categoryId
    ) {
        Category category = categoryService.get(categoryId);
        Product p = productService.get(productId);
        p = productService.update(p, category, product);
        return ResponseEntity.ok(productService.build(p));
    }

    @DeleteMapping("/{productId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> delete(@PathVariable Long productId) {
        Product c = productService.get(productId);
        productService.delete(c);
        return ResponseEntity.ok(MResponse.of("Product deleted"));
    }

    @PostMapping("/consolidation")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    @Transactional
    public ResponseEntity<MResponse> consolidate(
            @RequestParam long toKeepId,
            @RequestParam long toRemoveId
    ) {
        Product toKeep = productService.get(toKeepId);
        Product toRemove = productService.get(toRemoveId);
        detailService.consolidate(toKeep, toRemove);
        productService.delete(toRemove);
        return ResponseEntity.ok(MResponse.of("Consolidated"));
    }

}
