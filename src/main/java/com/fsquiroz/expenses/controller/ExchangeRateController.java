package com.fsquiroz.expenses.controller;

import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.ExchangeRate;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MException;
import com.fsquiroz.expenses.entity.json.MExchangeRate;
import com.fsquiroz.expenses.entity.json.MResponse;
import com.fsquiroz.expenses.security.SecurityCheck;
import com.fsquiroz.expenses.service.declaration.ICurrencyService;
import com.fsquiroz.expenses.service.declaration.IExchangeRateService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/exchangeRates")
public class ExchangeRateController {

    private SecurityCheck securityCheck;

    private ICurrencyService currencyService;

    private IExchangeRateService exchangeRateService;

    public ExchangeRateController(SecurityCheck securityCheck, ICurrencyService currencyService, IExchangeRateService exchangeRateService) {
        this.securityCheck = securityCheck;
        this.currencyService = currencyService;
        this.exchangeRateService = exchangeRateService;
    }

    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                    value = "Page number. Range from 0 to N", example = "0")
            ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                    value = "Page size. Number of elements to be retrieved. Rage from 1 to N", example = "20")
            ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                    value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = Page.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> search(@RequestParam(required = false) Long currencyId, @ApiIgnore Pageable pageable) {
        User u = securityCheck.getAuthentication();
        Currency c = null;
        if (currencyId != null) {
            c = currencyService.get(currencyId);
        }
        Page<ExchangeRate> exchangeRate = exchangeRateService.search(c, pageable, u);
        return ResponseEntity.ok(exchangeRateService.build(exchangeRate));
    }

    @PostMapping
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = MExchangeRate.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class)
    })
    public ResponseEntity<?> create(
            @RequestBody MExchangeRate exchangeRate,
            @RequestParam Long fromId,
            @RequestParam Long toId
    ) {
        User u = securityCheck.getAuthentication();
        Currency from = currencyService.get(fromId);
        Currency to = currencyService.get(toId);
        ExchangeRate b = exchangeRateService.create(exchangeRate, from, to, u);
        return new ResponseEntity(exchangeRateService.build(b), HttpStatus.CREATED);
    }

    @GetMapping("/{exchangeRateId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MExchangeRate.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> get(@PathVariable Long exchangeRateId) {
        ExchangeRate b = exchangeRateService.get(exchangeRateId);
        return ResponseEntity.ok(exchangeRateService.build(b));
    }

    @PutMapping("/{exchangeRateId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MExchangeRate.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> update(@PathVariable Long exchangeRateId, @RequestBody MExchangeRate exchangeRate) {
        ExchangeRate b = exchangeRateService.get(exchangeRateId);
        b = exchangeRateService.update(b, exchangeRate);
        return ResponseEntity.ok(exchangeRateService.build(b));
    }

    @DeleteMapping("/{exchangeRateId}")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Ok", response = MResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = MException.class),
            @ApiResponse(code = 401, message = "Unauthorized", response = MException.class),
            @ApiResponse(code = 403, message = "Forbidden", response = MException.class),
            @ApiResponse(code = 404, message = "Not Found", response = MException.class)
    })
    public ResponseEntity<?> delete(@PathVariable Long exchangeRateId) {
        ExchangeRate b = exchangeRateService.get(exchangeRateId);
        exchangeRateService.delete(b);
        return ResponseEntity.ok(MResponse.of("ExchangeRate deleted"));
    }

}
