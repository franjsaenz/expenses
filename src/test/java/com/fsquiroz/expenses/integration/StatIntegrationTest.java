package com.fsquiroz.expenses.integration;

import com.fsquiroz.expenses.config.*;
import com.fsquiroz.expenses.entity.db.Business;
import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
@Slf4j
public class StatIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    @Autowired
    private BusinessIntegrationConfig businessIntegrationConfig;

    @Autowired
    private CategoryIntegrationConfig categoryIntegrationConfig;

    @Autowired
    private CurrencyIntegrationConfig currencyIntegrationConfig;

    @Autowired
    private ProductIntegrationConfig productIntegrationConfig;

    private User root;

    private User user;

    private User owner;

    private Business business;

    private Currency currency;

    @Before
    public void setup() {
        userIntegrationConfig.setup();
        businessIntegrationConfig.setup();
        currencyIntegrationConfig.setup();
        categoryIntegrationConfig.setup();
        productIntegrationConfig.setup();
        root = userIntegrationConfig.getRoot();
        user = userIntegrationConfig.getUser();
        owner = userIntegrationConfig.getOwner();
        business = businessIntegrationConfig.getFirst();
        currency = currencyIntegrationConfig.getDollar();
    }

    @After
    public void cleanup() {
        productIntegrationConfig.cleanup();
        categoryIntegrationConfig.cleanup();
        currencyIntegrationConfig.cleanup();
        businessIntegrationConfig.cleanup();
        userIntegrationConfig.cleanup();
    }

    @Test
    public void getTotalsByBusinessAsRoot() throws Exception {
        log.info("Test get totals by business and currency as root");
        mockMvc.perform(businessTotals(root, business, currency))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getTotalsByBusinessAsUser() throws Exception {
        log.info("Test get totals by business and currency as user");
        mockMvc.perform(businessTotals(user, business, currency))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getTotalsByBusinessAsOwner() throws Exception {
        log.info("Test get totals by business and currency as owner");
        mockMvc.perform(businessTotals(owner, business, currency))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getTotalsByBusinessAsClient() throws Exception {
        log.info("Test get totals by business and currency as client");
        mockMvc.perform(businessTotals(root, business, currency))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getTotalsByBusinessAsAnonymous() throws Exception {
        log.info("Test get totals by business and currency as anonymous");
        mockMvc.perform(businessTotals(null, business, currency))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getTotalsGroupedByBusinessAsRoot() throws Exception {
        log.info("Test get totals grouped by business and currency as root");
        mockMvc.perform(businessTotals(root, null, currency))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getTotalsGroupedByBusinessAsUser() throws Exception {
        log.info("Test get totals grouped by business and currency as user");
        mockMvc.perform(businessTotals(user, null, currency))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getTotalsGroupedByBusinessAsOwner() throws Exception {
        log.info("Test get totals grouped by business and currency as owner");
        mockMvc.perform(businessTotals(owner, null, currency))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getTotalsGroupedByBusinessAsClient() throws Exception {
        log.info("Test get totals grouped by business and currency as client");
        mockMvc.perform(businessTotals(root, null, currency))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getTotalsGroupedByBusinessAsAnonymous() throws Exception {
        log.info("Test get totals grouped by business and currency as anonymous");
        mockMvc.perform(businessTotals(null, null, currency))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    private MockHttpServletRequestBuilder businessTotals(User who, Business business, Currency currency) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/stats/businesses/totals")
                .param("currencyId", currency.getId().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        if (business != null) {
            request.param("businessId", business.getId().toString());
        }
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

}
