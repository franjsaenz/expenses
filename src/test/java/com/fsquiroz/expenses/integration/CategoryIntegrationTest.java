package com.fsquiroz.expenses.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.expenses.config.CategoryIntegrationConfig;
import com.fsquiroz.expenses.config.ProductIntegrationConfig;
import com.fsquiroz.expenses.config.UserIntegrationConfig;
import com.fsquiroz.expenses.entity.db.Category;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MCategory;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
@Slf4j
public class CategoryIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    @Autowired
    private CategoryIntegrationConfig categoryIntegrationConfig;

    @Autowired
    private ProductIntegrationConfig productIntegrationConfig;

    private User root;

    private User user;

    private User owner;

    private User client;

    private Category first;

    private Category second;

    @Before
    public void setup() {
        userIntegrationConfig.setup();
        categoryIntegrationConfig.setup();
        productIntegrationConfig.setup();
        root = userIntegrationConfig.getRoot();
        user = userIntegrationConfig.getUser();
        owner = userIntegrationConfig.getOwner();
        client = userIntegrationConfig.getClient();
        first = categoryIntegrationConfig.getFirst();
        second = categoryIntegrationConfig.getSecond();
    }

    @After
    public void cleanup() {
        productIntegrationConfig.cleanup();
        categoryIntegrationConfig.cleanup();
        userIntegrationConfig.cleanup();
    }

    @Test
    public void searchAsRoot() throws Exception {
        log.info("Test search categories as root");
        mockMvc.perform(search(root))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isEmpty());
    }

    @Test
    public void searchAsUser() throws Exception {
        log.info("Test search categories as user");
        mockMvc.perform(search(user))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isEmpty());
    }

    @Test
    public void searchAsOwner() throws Exception {
        log.info("Test search categories as owner");
        mockMvc.perform(search(owner))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isNotEmpty());
    }

    @Test
    public void searchAsClient() throws Exception {
        log.info("Test search categories as client");
        mockMvc.perform(search(client))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isEmpty());
    }

    @Test
    public void searchAsAnonymous() throws Exception {
        log.info("Test search categories as anonymous");
        mockMvc.perform(search(null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void createAsRoot() throws Exception {
        log.info("Test create category as root");
        MCategory mc = new MCategory();
        mc.setName("New category");
        mockMvc.perform(create(root, mc))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsUser() throws Exception {
        log.info("Test create category as user");
        MCategory mc = new MCategory();
        mc.setName("New category");
        mockMvc.perform(create(user, mc))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsClient() throws Exception {
        log.info("Test create category as client");
        MCategory mc = new MCategory();
        mc.setName("New category");
        mockMvc.perform(create(client, mc))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsAnonymous() throws Exception {
        log.info("Test create category as anonymous");
        MCategory mc = new MCategory();
        mc.setName("New category");
        mockMvc.perform(create(null, mc))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getAsRoot() throws Exception {
        log.info("Test get category as root");
        mockMvc.perform(get(root, first))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsUser() throws Exception {
        log.info("Test get category as user");
        mockMvc.perform(get(user, first))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsOwner() throws Exception {
        log.info("Test get category as owner");
        mockMvc.perform(get(owner, first))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsClient() throws Exception {
        log.info("Test get category as client");
        mockMvc.perform(get(client, first))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsAnonymous() throws Exception {
        log.info("Test get category as anonymous");
        mockMvc.perform(get(null, first))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateAsRoot() throws Exception {
        log.info("Test update category as root");
        MCategory mc = new MCategory();
        mc.setName("New name");
        mockMvc.perform(put(root, first, mc))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateAsUser() throws Exception {
        log.info("Test update category as user");
        MCategory mc = new MCategory();
        mc.setName("New name");
        mockMvc.perform(put(user, first, mc))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsOwner() throws Exception {
        log.info("Test update category as owner");
        MCategory mc = new MCategory();
        mc.setName("New name");
        mockMvc.perform(put(owner, first, mc))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateAsClient() throws Exception {
        log.info("Test update category as client");
        MCategory mc = new MCategory();
        mc.setName("New name");
        mockMvc.perform(put(client, first, mc))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsAnonymous() throws Exception {
        log.info("Test update category as anonymous");
        MCategory mc = new MCategory();
        mc.setName("New name");
        mockMvc.perform(put(null, first, mc))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateDeletedAsRoot() throws Exception {
        log.info("Test update deleted category as root");
        categoryIntegrationConfig.delete(first);
        MCategory mc = new MCategory();
        mc.setName("New name");
        mockMvc.perform(put(root, first, mc))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteAsRoot() throws Exception {
        log.info("Test delete category as root");
        mockMvc.perform(delete(root, second))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteAsUser() throws Exception {
        log.info("Test delete category as user");
        mockMvc.perform(delete(user, second))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsClient() throws Exception {
        log.info("Test delete category as client");
        mockMvc.perform(delete(client, second))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsAnonymous() throws Exception {
        log.info("Test delete category as anonymous");
        mockMvc.perform(delete(null, second))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void deleteDeletedAsRoot() throws Exception {
        log.info("Test delete deleted category as root");
        categoryIntegrationConfig.delete(second);
        mockMvc.perform(delete(root, second))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    private MockHttpServletRequestBuilder search(User who) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder create(User who, MCategory toCreate) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(toCreate));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder get(User who, Category toGet) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/categories/{categoryId}", toGet.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder put(User who, Category toEdit, MCategory edition) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/categories/{categoryId}", toEdit.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(edition));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder delete(User who, Category toDelete) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/categories/{categoryId}", toDelete.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder searchProducts(User who, Category category) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/categories/{categoryId}/products", category.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

}
