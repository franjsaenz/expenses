package com.fsquiroz.expenses.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.expenses.config.UserIntegrationConfig;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MLogin;
import com.fsquiroz.expenses.service.TimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Instant;
import java.util.Calendar;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
@Slf4j
public class PublicControllerIntegration {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    @After
    public void cleanup() {
        this.userIntegrationConfig.cleanup();
    }

    @Test
    public void loginAsRoot() throws Exception {
        log.info("Test login as root");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getRoot();
        MLogin login = new MLogin(who.getEmail(), "123456");
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void loginAsUser() throws Exception {
        log.info("Test login as user");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getUser();
        MLogin login = new MLogin(who.getEmail(), "123456");
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void loginAsClient() throws Exception {
        log.info("Test login as client");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getClient();
        MLogin login = new MLogin(who.getEmail(), "123456");
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void loginAsNoUser() throws Exception {
        log.info("Test login as no user");
        MLogin login = new MLogin("not@found.com", "123456");
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void loginAsDeleted() throws Exception {
        log.info("Test login as user deleted");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getUser();
        userIntegrationConfig.delete(who);
        MLogin login = new MLogin(who.getEmail(), "123456");
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void loginWithoutEmail() throws Exception {
        log.info("Test login without emails");
        MLogin login = new MLogin(null, "123456");
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void loginWithoutPassword() throws Exception {
        log.info("Test login without password");
        MLogin login = new MLogin("not@found.com", null);
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void loginWithWrongPassword() throws Exception {
        log.info("Test login with wrong password");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getUser();
        MLogin login = new MLogin(who.getEmail(), "wrong");
        mockMvc.perform(login(login))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void loginWithExpirationWithoutNotBefore() throws Exception {
        log.info("Test login with expiration and without notBefore");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getRoot();
        MLogin login = new MLogin(who.getEmail(), "123456");
        Instant expiration = offset(0, 1);
        mockMvc.perform(login(login, expiration, null))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void loginWithoutExpirationWithNotBefore() throws Exception {
        log.info("Test login without expiration and with notBefore");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getRoot();
        MLogin login = new MLogin(who.getEmail(), "123456");
        Instant notBefore = offset(15, 0);
        mockMvc.perform(login(login, null, notBefore))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void loginWithExpirationAndNotBefore() throws Exception {
        log.info("Test login with expiration and notBefore");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getRoot();
        MLogin login = new MLogin(who.getEmail(), "123456");
        Instant expiration = offset(0, 2);
        Instant notBefore = offset(0, 1);
        mockMvc.perform(login(login, expiration, notBefore))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void loginWithExpirationAndNotBeforeSwapped() throws Exception {
        log.info("Test login with expiration and notBefore swapped");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getRoot();
        MLogin login = new MLogin(who.getEmail(), "123456");
        Instant expiration = offset(0, 1);
        Instant notBefore = offset(0, 2);
        mockMvc.perform(login(login, expiration, notBefore))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void loggedInAsRoot() throws Exception {
        log.info("Test get logged in user as root");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getRoot();
        mockMvc.perform(loggedIn(who))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void loggedInAsUser() throws Exception {
        log.info("Test get logged in user as user");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getUser();
        mockMvc.perform(loggedIn(who))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void loggedInAsClient() throws Exception {
        log.info("Test get logged in user as client");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getClient();
        mockMvc.perform(loggedIn(who))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void loggedInAsAnonymous() throws Exception {
        log.info("Test get logged in user as anonymous");
        userIntegrationConfig.setup();
        mockMvc.perform(loggedIn(null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void statusAsRoot() throws Exception {
        log.info("Test get status as root");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getRoot();
        mockMvc.perform(index(who))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void statusAsUser() throws Exception {
        log.info("Test get status as user");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getUser();
        mockMvc.perform(index(who))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void statusAsClient() throws Exception {
        log.info("Test get status as client");
        userIntegrationConfig.setup();
        User who = userIntegrationConfig.getClient();
        mockMvc.perform(index(who))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void statusAsAnonymous() throws Exception {
        log.info("Test get status as anonymous");
        mockMvc.perform(index(null))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    private MockHttpServletRequestBuilder login(MLogin login) throws Exception {
        return login(login, null, null);
    }

    private MockHttpServletRequestBuilder login(MLogin login, Instant expiration, Instant notBefore) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(login));
        if (expiration != null) {
            request.param("expiration", TimeUtils.standard(expiration));
        }
        if (notBefore != null) {
            request.param("notBefore", TimeUtils.standard(notBefore));
        }
        return request;
    }

    private MockHttpServletRequestBuilder loggedIn(User who) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/login");
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder index(User who) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/")
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private Instant offset(int minutesOffset, int hoursOffset) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, minutesOffset);
        c.add(Calendar.HOUR, hoursOffset);
        return c.getTime().toInstant();
    }

}
