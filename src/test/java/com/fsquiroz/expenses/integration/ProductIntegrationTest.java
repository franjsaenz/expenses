package com.fsquiroz.expenses.integration;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.expenses.config.BusinessIntegrationConfig;
import com.fsquiroz.expenses.config.CategoryIntegrationConfig;
import com.fsquiroz.expenses.config.CurrencyIntegrationConfig;
import com.fsquiroz.expenses.config.ExchangeRateIntegrationConfig;
import com.fsquiroz.expenses.config.ExpenseIntegrationConfig;
import com.fsquiroz.expenses.config.ProductIntegrationConfig;
import com.fsquiroz.expenses.config.UserIntegrationConfig;
import com.fsquiroz.expenses.entity.db.Category;
import com.fsquiroz.expenses.entity.db.Product;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MProduct;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
@Slf4j
public class ProductIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    @Autowired
    private CategoryIntegrationConfig categoryIntegrationConfig;

    @Autowired
    private BusinessIntegrationConfig businessIntegrationConfig;

    @Autowired
    private CurrencyIntegrationConfig currencyIntegrationConfig;

    @Autowired
    private ExchangeRateIntegrationConfig exchangeRateIntegrationConfig;

    @Autowired
    private ProductIntegrationConfig productIntegrationConfig;

    @Autowired
    private ExpenseIntegrationConfig expenseIntegrationConfig;

    private User root;

    private User user;

    private User owner;

    private User client;

    private Category category;

    private Product first;

    private Product second;

    @Before
    public void setup() {
        userIntegrationConfig.setup();
        categoryIntegrationConfig.setup();
        businessIntegrationConfig.setup();
        productIntegrationConfig.setup();
        currencyIntegrationConfig.setup();
        exchangeRateIntegrationConfig.setup();
        expenseIntegrationConfig.setup();
        root = userIntegrationConfig.getRoot();
        user = userIntegrationConfig.getUser();
        owner = userIntegrationConfig.getOwner();
        client = userIntegrationConfig.getClient();
        category = categoryIntegrationConfig.getFirst();
        first = productIntegrationConfig.getFirst();
        second = productIntegrationConfig.getSecond();
    }

    @After
    public void cleanup() {
        expenseIntegrationConfig.cleanup();
        productIntegrationConfig.cleanup();
        categoryIntegrationConfig.cleanup();
        businessIntegrationConfig.cleanup();
        exchangeRateIntegrationConfig.cleanup();
        currencyIntegrationConfig.cleanup();
        userIntegrationConfig.cleanup();
    }

    @Test
    public void searchAsRoot() throws Exception {
        log.info("Test search products as root");
        mockMvc.perform(search(root, null))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isEmpty());
    }

    @Test
    public void searchAsUser() throws Exception {
        log.info("Test search products as user");
        mockMvc.perform(search(user, null))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isEmpty());
    }

    @Test
    public void searchAsOwner() throws Exception {
        log.info("Test search products as owner");
        mockMvc.perform(search(owner, null))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isNotEmpty());
    }

    @Test
    public void searchAsClient() throws Exception {
        log.info("Test search products as client");
        mockMvc.perform(search(client, null))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isEmpty());
    }

    @Test
    public void searchAsAnonymous() throws Exception {
        log.info("Test search products as anonymous");
        mockMvc.perform(search(null, null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void searchByCategoryAsRoot() throws Exception {
        log.info("Test search products by category as root");
        mockMvc.perform(search(root, category))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void searchByCategoryAsUser() throws Exception {
        log.info("Test search products by category as user");
        mockMvc.perform(search(user, category))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchByCategoryAsOwner() throws Exception {
        log.info("Test search products by category as owner");
        mockMvc.perform(search(owner, category))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void searchByCategoryAsClient() throws Exception {
        log.info("Test search products by category as client");
        mockMvc.perform(search(client, category))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchByCategoryAsAnonymous() throws Exception {
        log.info("Test search products by category as anonymous");
        mockMvc.perform(search(null, category))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void createAsRoot() throws Exception {
        log.info("Test create product as root");
        MProduct mp = new MProduct();
        mp.setName("New product");
        mockMvc.perform(create(root, mp, category))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsUser() throws Exception {
        log.info("Test create product as user");
        MProduct mp = new MProduct();
        mp.setName("New product");
        mockMvc.perform(create(user, mp, category))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsOwner() throws Exception {
        log.info("Test create product as owner");
        MProduct mp = new MProduct();
        mp.setName("New product");
        mockMvc.perform(create(owner, mp, category))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsClient() throws Exception {
        log.info("Test create product as client");
        MProduct mp = new MProduct();
        mp.setName("New product");
        mockMvc.perform(create(client, mp, category))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsAnonymous() throws Exception {
        log.info("Test create product as anonymous");
        MProduct mp = new MProduct();
        mp.setName("New product");
        mockMvc.perform(create(null, mp, category))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getAsRoot() throws Exception {
        log.info("Test get product as root");
        mockMvc.perform(get(root, first))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsUser() throws Exception {
        log.info("Test get product as user");
        mockMvc.perform(get(user, first))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsOwner() throws Exception {
        log.info("Test get product as owner");
        mockMvc.perform(get(owner, first))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsClient() throws Exception {
        log.info("Test get product as client");
        mockMvc.perform(get(client, first))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsAnonymous() throws Exception {
        log.info("Test get product as anonymous");
        mockMvc.perform(get(null, first))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateAsRoot() throws Exception {
        log.info("Test update product as root");
        MProduct mp = new MProduct();
        mp.setName("New name");
        mockMvc.perform(put(root, first, mp, category))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateAsUser() throws Exception {
        log.info("Test update product as user");
        MProduct mp = new MProduct();
        mp.setName("New name");
        mockMvc.perform(put(user, first, mp, category))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsOwner() throws Exception {
        log.info("Test update product as owner");
        MProduct mp = new MProduct();
        mp.setName("New name");
        mockMvc.perform(put(owner, first, mp, category))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateAsClient() throws Exception {
        log.info("Test update product as client");
        MProduct mp = new MProduct();
        mp.setName("New name");
        mockMvc.perform(put(client, first, mp, category))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsAnonymous() throws Exception {
        log.info("Test update product as anonymous");
        MProduct mp = new MProduct();
        mp.setName("New name");
        mockMvc.perform(put(null, first, mp, category))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateDeletedAsRoot() throws Exception {
        log.info("Test update deleted product as root");
        productIntegrationConfig.delete(first);
        MProduct mp = new MProduct();
        mp.setName("New name");
        mockMvc.perform(put(root, first, mp, category))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteAsRoot() throws Exception {
        log.info("Test delete product as root");
        mockMvc.perform(delete(root, second))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteAsUser() throws Exception {
        log.info("Test delete product as user");
        mockMvc.perform(delete(user, second))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsClient() throws Exception {
        log.info("Test delete product as client");
        mockMvc.perform(delete(client, second))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsAnonymous() throws Exception {
        log.info("Test delete product as anonymous");
        mockMvc.perform(delete(null, second))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void deleteDeletedAsRoot() throws Exception {
        log.info("Test delete deleted product as root");
        productIntegrationConfig.delete(second);
        mockMvc.perform(delete(root, second))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void consolidateAsRoot() throws Exception {
        log.info("Test consolidate products as root");
        mockMvc.perform(consolidate(root, productIntegrationConfig.getFirst(), productIntegrationConfig.getSecond()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void consolidateAsUser() throws Exception {
        log.info("Test consolidate products as user");
        mockMvc.perform(consolidate(owner, productIntegrationConfig.getFirst(), productIntegrationConfig.getSecond()))
                .andExpect(MockMvcResultMatchers.status().isOk());

        assertThat(expenseIntegrationConfig.get(expenseIntegrationConfig.getFirstProductADetail()).getProduct())
                .isEqualTo(productIntegrationConfig.get(productIntegrationConfig.getFirst()));
        assertThat(expenseIntegrationConfig.get(expenseIntegrationConfig.getSecondProductADetail()).getProduct())
                .isEqualTo(productIntegrationConfig.get(productIntegrationConfig.getFirst()));
        assertThat(expenseIntegrationConfig.get(expenseIntegrationConfig.getFirstProductBDetail()).getProduct())
                .isEqualTo(productIntegrationConfig.get(productIntegrationConfig.getFirst()));
    }

    @Test
    public void consolidateAsClient() throws Exception {
        log.info("Test consolidate products as client");
        mockMvc.perform(consolidate(client, productIntegrationConfig.getFirst(), productIntegrationConfig.getSecond()))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void consolidateAsAnonymous() throws Exception {
        log.info("Test consolidate products as anonymous");
        mockMvc.perform(consolidate(null, productIntegrationConfig.getFirst(), productIntegrationConfig.getSecond()))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    private MockHttpServletRequestBuilder search(User who, Category category) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        if (category != null) {
            request.param("categoryId", category.getId().toString());
        }
        return request;
    }

    private MockHttpServletRequestBuilder create(User who, MProduct toCreate, Category category) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/products")
                .param("categoryId", category.getId().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(toCreate));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder get(User who, Product toGet) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/products/{productId}", toGet.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder put(User who, Product toEdit, MProduct edition, Category category) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/products/{productId}", toEdit.getId())
                .param("categoryId", category.getId().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(edition));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder delete(User who, Product toDelete) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/products/{productId}", toDelete.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder consolidate(User who, Product toKeep, Product toRemove) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/products/consolidation")
                .param("toKeepId", toKeep.getId().toString())
                .param("toRemoveId", toRemove.getId().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

}
