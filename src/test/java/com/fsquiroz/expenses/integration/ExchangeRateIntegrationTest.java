package com.fsquiroz.expenses.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.expenses.config.CurrencyIntegrationConfig;
import com.fsquiroz.expenses.config.ExchangeRateIntegrationConfig;
import com.fsquiroz.expenses.config.UserIntegrationConfig;
import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.ExchangeRate;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MExchangeRate;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.time.Instant;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
@Slf4j
public class ExchangeRateIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    @Autowired
    private CurrencyIntegrationConfig currencyIntegrationConfig;

    @Autowired
    private ExchangeRateIntegrationConfig exchangeRateIntegrationConfig;

    private User root;

    private User user;

    private User owner;

    private User client;

    private Currency dollar;

    private Currency euro;

    private ExchangeRate dollarToEuro;

    private ExchangeRate dollarToPound;

    @Before
    public void setup() {
        userIntegrationConfig.setup();
        currencyIntegrationConfig.setup();
        exchangeRateIntegrationConfig.setup();
        root = userIntegrationConfig.getRoot();
        user = userIntegrationConfig.getUser();
        owner = userIntegrationConfig.getOwner();
        client = userIntegrationConfig.getClient();
        dollar = currencyIntegrationConfig.getDollar();
        euro = currencyIntegrationConfig.getEuro();
        dollarToEuro = exchangeRateIntegrationConfig.getDollarToEuro();
        dollarToPound = exchangeRateIntegrationConfig.getDollarToPound();
    }

    @After
    public void cleanup() {
        exchangeRateIntegrationConfig.cleanup();
        currencyIntegrationConfig.cleanup();
        userIntegrationConfig.cleanup();
    }

    @Test
    public void searchAsRoot() throws Exception {
        log.info("Test search exchangeRates as root");
        mockMvc.perform(search(root))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isEmpty());
    }

    @Test
    public void searchAsUser() throws Exception {
        log.info("Test search exchangeRates as user");
        mockMvc.perform(search(user))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isEmpty());
    }

    @Test
    public void searchAsOwner() throws Exception {
        log.info("Test search exchangeRates as owner");
        mockMvc.perform(search(owner))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isNotEmpty());
    }

    @Test
    public void searchAsClient() throws Exception {
        log.info("Test search exchangeRates as client");
        mockMvc.perform(search(client))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isEmpty());
    }

    @Test
    public void searchAsAnonymous() throws Exception {
        log.info("Test search exchangeRates as anonymous");
        mockMvc.perform(search(null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void createAsRoot() throws Exception {
        log.info("Test create exchangeRate as root");
        MExchangeRate mer = new MExchangeRate();
        mer.setMultiplier(BigDecimal.valueOf(1.12));
        mer.setRegistered(Instant.now());
        mockMvc.perform(create(root, mer, euro, dollar))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsUser() throws Exception {
        log.info("Test create exchangeRate as user");
        MExchangeRate mer = new MExchangeRate();
        mer.setMultiplier(BigDecimal.valueOf(1.12));
        mer.setRegistered(Instant.now());
        mockMvc.perform(create(root, mer, euro, dollar))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsClient() throws Exception {
        log.info("Test create exchangeRate as client");
        MExchangeRate mer = new MExchangeRate();
        mer.setMultiplier(BigDecimal.valueOf(1.12));
        mer.setRegistered(Instant.now());
        mockMvc.perform(create(client, mer, euro, dollar))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsAnonymous() throws Exception {
        log.info("Test create exchangeRate as anonymous");
        MExchangeRate mer = new MExchangeRate();
        mer.setMultiplier(BigDecimal.valueOf(1.12));
        mer.setRegistered(Instant.now());
        mockMvc.perform(create(null, mer, euro, dollar))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getAsRoot() throws Exception {
        log.info("Test get exchangeRate as root");
        mockMvc.perform(get(root, dollarToEuro))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsUser() throws Exception {
        log.info("Test get exchangeRate as user");
        mockMvc.perform(get(user, dollarToEuro))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsOwner() throws Exception {
        log.info("Test get exchangeRate as owner");
        mockMvc.perform(get(owner, dollarToEuro))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsClient() throws Exception {
        log.info("Test get exchangeRate as client");
        mockMvc.perform(get(client, dollarToEuro))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsAnonymous() throws Exception {
        log.info("Test get exchangeRate as anonymous");
        mockMvc.perform(get(null, dollarToEuro))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateAsRoot() throws Exception {
        log.info("Test update exchangeRate as root");
        MExchangeRate mer = new MExchangeRate();
        mer.setMultiplier(BigDecimal.valueOf(0.88));
        mockMvc.perform(put(root, dollarToEuro, mer))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateAsUser() throws Exception {
        log.info("Test update exchangeRate as user");
        MExchangeRate mer = new MExchangeRate();
        mer.setMultiplier(BigDecimal.valueOf(0.88));
        mockMvc.perform(put(user, dollarToEuro, mer))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsOwner() throws Exception {
        log.info("Test update exchangeRate as owner");
        MExchangeRate mer = new MExchangeRate();
        mer.setMultiplier(BigDecimal.valueOf(0.88));
        mockMvc.perform(put(owner, dollarToEuro, mer))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateAsClient() throws Exception {
        log.info("Test update exchangeRate as client");
        MExchangeRate mer = new MExchangeRate();
        mer.setMultiplier(BigDecimal.valueOf(0.88));
        mockMvc.perform(put(client, dollarToEuro, mer))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsAnonymous() throws Exception {
        log.info("Test update exchangeRate as anonymous");
        MExchangeRate mer = new MExchangeRate();
        mer.setMultiplier(BigDecimal.valueOf(0.88));
        mockMvc.perform(put(null, dollarToEuro, mer))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateDeletedAsRoot() throws Exception {
        log.info("Test update deleted exchangeRate as root");
        exchangeRateIntegrationConfig.delete(dollarToEuro);
        MExchangeRate mer = new MExchangeRate();
        mer.setMultiplier(BigDecimal.valueOf(0.88));
        mockMvc.perform(put(root, dollarToEuro, mer))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteAsRoot() throws Exception {
        log.info("Test delete exchangeRate as root");
        mockMvc.perform(delete(root, dollarToPound))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteAsUser() throws Exception {
        log.info("Test delete exchangeRate as user");
        mockMvc.perform(delete(user, dollarToPound))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsClient() throws Exception {
        log.info("Test delete exchangeRate as client");
        mockMvc.perform(delete(client, dollarToPound))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsAnonymous() throws Exception {
        log.info("Test delete exchangeRate as anonymous");
        mockMvc.perform(delete(null, dollarToPound))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void deleteDeletedAsRoot() throws Exception {
        log.info("Test delete deleted exchangeRate as root");
        exchangeRateIntegrationConfig.delete(dollarToPound);
        mockMvc.perform(delete(root, dollarToPound))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    private MockHttpServletRequestBuilder search(User who) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/exchangeRates")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder create(User who, MExchangeRate toCreate, Currency from, Currency to) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/exchangeRates")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .param("fromId", from.getId().toString())
                .param("toId", to.getId().toString())
                .content(mapper.writeValueAsString(toCreate));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder get(User who, ExchangeRate toGet) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/exchangeRates/{exchangeRateId}", toGet.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder put(User who, ExchangeRate toEdit, MExchangeRate edition) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/exchangeRates/{exchangeRateId}", toEdit.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(edition));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder delete(User who, ExchangeRate toDelete) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/exchangeRates/{exchangeRateId}", toDelete.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

}
