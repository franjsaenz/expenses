package com.fsquiroz.expenses.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.expenses.config.CurrencyIntegrationConfig;
import com.fsquiroz.expenses.config.UserIntegrationConfig;
import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MCurrency;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
@Slf4j
public class CurrencyIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    @Autowired
    private CurrencyIntegrationConfig currencyIntegrationConfig;

    private User root;

    private User user;

    private User owner;

    private User client;

    private Currency dollar;

    private Currency euro;

    @Before
    public void setup() {
        userIntegrationConfig.setup();
        currencyIntegrationConfig.setup();
        root = userIntegrationConfig.getRoot();
        user = userIntegrationConfig.getUser();
        owner = userIntegrationConfig.getOwner();
        client = userIntegrationConfig.getClient();
        dollar = currencyIntegrationConfig.getDollar();
        euro = currencyIntegrationConfig.getEuro();
    }

    @After
    public void cleanup() {
        currencyIntegrationConfig.cleanup();
        userIntegrationConfig.cleanup();
    }

    private MockHttpServletRequestBuilder search(User who) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/currencies")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    @Test
    public void searchAsRoot() throws Exception {
        log.info("Test search currencies as root");
        mockMvc.perform(search(root))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isNotEmpty());
    }

    @Test
    public void searchAsUser() throws Exception {
        log.info("Test search currencies as user");
        mockMvc.perform(search(user))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isNotEmpty());
    }

    @Test
    public void searchAsOwner() throws Exception {
        log.info("Test search currencies as owner");
        mockMvc.perform(search(owner))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isNotEmpty());
    }

    @Test
    public void searchAsClient() throws Exception {
        log.info("Test search currencies as client");
        mockMvc.perform(search(client))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isNotEmpty());
    }

    @Test
    public void searchAsAnonymous() throws Exception {
        log.info("Test search currencies as anonymous");
        mockMvc.perform(search(null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void createAsRoot() throws Exception {
        log.info("Test create currency as root");
        MCurrency mc = new MCurrency();
        mc.setName("Argentinian Peso");
        mc.setCode("ARG");
        mc.setIdentifier("$AR");
        mockMvc.perform(create(root, mc))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsUser() throws Exception {
        log.info("Test create currency as user");
        MCurrency mc = new MCurrency();
        mc.setName("Argentinian Peso");
        mc.setCode("ARG");
        mc.setIdentifier("$AR");
        mockMvc.perform(create(user, mc))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsClient() throws Exception {
        log.info("Test create currency as client");
        MCurrency mc = new MCurrency();
        mc.setName("Argentinian Peso");
        mc.setCode("ARG");
        mc.setIdentifier("$AR");
        mockMvc.perform(create(client, mc))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsAnonymous() throws Exception {
        log.info("Test create currency as anonymous");
        MCurrency mc = new MCurrency();
        mc.setName("Argentinian Peso");
        mc.setCode("ARG");
        mc.setIdentifier("$AR");
        mockMvc.perform(create(null, mc))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getAsRoot() throws Exception {
        log.info("Test get currency as root");
        mockMvc.perform(get(root, dollar))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsUser() throws Exception {
        log.info("Test get currency as user");
        mockMvc.perform(get(user, dollar))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsOwner() throws Exception {
        log.info("Test get currency as owner");
        mockMvc.perform(get(owner, dollar))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsClient() throws Exception {
        log.info("Test get currency as client");
        mockMvc.perform(get(client, dollar))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsAnonymous() throws Exception {
        log.info("Test get currency as anonymous");
        mockMvc.perform(get(null, dollar))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateAsRoot() throws Exception {
        log.info("Test update currency as root");
        MCurrency mc = new MCurrency();
        mc.setName("Uruguayan Peso");
        mc.setCode("URU");
        mc.setIdentifier("$UY");
        mockMvc.perform(put(root, dollar, mc))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateAsUser() throws Exception {
        log.info("Test update currency as user");
        MCurrency mc = new MCurrency();
        mc.setName("Uruguayan Peso");
        mc.setCode("URU");
        mc.setIdentifier("$UY");
        mockMvc.perform(put(user, dollar, mc))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsOwner() throws Exception {
        log.info("Test update currency as owner");
        MCurrency mc = new MCurrency();
        mc.setName("Uruguayan Peso");
        mc.setCode("URU");
        mc.setIdentifier("$UY");
        mockMvc.perform(put(owner, dollar, mc))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsClient() throws Exception {
        log.info("Test update currency as client");
        MCurrency mc = new MCurrency();
        mc.setName("Uruguayan Peso");
        mc.setCode("URU");
        mc.setIdentifier("$UY");
        mockMvc.perform(put(client, dollar, mc))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsAnonymous() throws Exception {
        log.info("Test update currency as anonymous");
        MCurrency mc = new MCurrency();
        mc.setName("Uruguayan Peso");
        mc.setCode("URU");
        mc.setIdentifier("$UY");
        mockMvc.perform(put(null, dollar, mc))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateDeletedAsRoot() throws Exception {
        log.info("Test update deleted currency as root");
        currencyIntegrationConfig.delete(dollar);
        MCurrency mc = new MCurrency();
        mc.setName("Uruguayan Peso");
        mc.setCode("URU");
        mc.setIdentifier("$UY");
        mockMvc.perform(put(root, dollar, mc))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteAsRoot() throws Exception {
        log.info("Test delete currency as root");
        mockMvc.perform(delete(root, euro))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteAsUser() throws Exception {
        log.info("Test delete currency as user");
        mockMvc.perform(delete(user, euro))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsClient() throws Exception {
        log.info("Test delete currency as client");
        mockMvc.perform(delete(client, euro))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsAnonymous() throws Exception {
        log.info("Test delete currency as anonymous");
        mockMvc.perform(delete(null, euro))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void deleteDeletedAsRoot() throws Exception {
        log.info("Test delete deleted currency as root");
        currencyIntegrationConfig.delete(euro);
        mockMvc.perform(delete(root, euro))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    private MockHttpServletRequestBuilder create(User who, MCurrency toCreate) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/currencies")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(toCreate));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder get(User who, Currency toGet) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/currencies/{currencyId}", toGet.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder put(User who, Currency toEdit, MCurrency edition) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/currencies/{currencyId}", toEdit.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(edition));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder delete(User who, Currency toDelete) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/currencies/{currencyId}", toDelete.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

}
