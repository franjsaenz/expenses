package com.fsquiroz.expenses.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.expenses.config.*;
import com.fsquiroz.expenses.entity.db.*;
import com.fsquiroz.expenses.entity.json.MDetail;
import com.fsquiroz.expenses.entity.json.MDiscount;
import com.fsquiroz.expenses.entity.json.MExpense;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
@Slf4j
public class ExpenseIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    @Autowired
    private CategoryIntegrationConfig categoryIntegrationConfig;

    @Autowired
    private BusinessIntegrationConfig businessIntegrationConfig;

    @Autowired
    private CurrencyIntegrationConfig currencyIntegrationConfig;

    @Autowired
    private ExchangeRateIntegrationConfig exchangeRateIntegrationConfig;

    @Autowired
    private ProductIntegrationConfig productIntegrationConfig;

    @Autowired
    private ExpenseIntegrationConfig expenseIntegrationConfig;

    private User root;

    private User user;

    private User owner;

    private User client;

    private Business business;

    private Currency pound;

    private Product product;

    private Expense full;

    private Expense empty;

    private Expense closed;

    private Discount discount;

    private Discount closedDiscount;

    private Detail detail;

    private Detail closedDetail;

    @Before
    public void setup() {
        userIntegrationConfig.setup();
        categoryIntegrationConfig.setup();
        businessIntegrationConfig.setup();
        productIntegrationConfig.setup();
        currencyIntegrationConfig.setup();
        exchangeRateIntegrationConfig.setup();
        expenseIntegrationConfig.setup();
        root = userIntegrationConfig.getRoot();
        user = userIntegrationConfig.getUser();
        owner = userIntegrationConfig.getOwner();
        client = userIntegrationConfig.getClient();
        business = businessIntegrationConfig.getSecond();
        pound = currencyIntegrationConfig.getPound();
        product = productIntegrationConfig.getFirst();
        full = expenseIntegrationConfig.getExpense();
        empty = expenseIntegrationConfig.getEmptyExpense();
        closed = expenseIntegrationConfig.getClosedExpense();
        discount = expenseIntegrationConfig.getDiscount();
        detail = expenseIntegrationConfig.getFirstDetail();
        closedDetail = expenseIntegrationConfig.getClosedDetail();
        closedDiscount = expenseIntegrationConfig.getClosedDiscount();
    }

    @After
    public void cleanup() {
        expenseIntegrationConfig.cleanup();
        exchangeRateIntegrationConfig.cleanup();
        currencyIntegrationConfig.cleanup();
        productIntegrationConfig.cleanup();
        businessIntegrationConfig.cleanup();
        categoryIntegrationConfig.cleanup();
        userIntegrationConfig.cleanup();
    }

    @Test
    public void searchAsRoot() throws Exception {
        log.info("Test search expenses as root");
        mockMvc.perform(search(root))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isEmpty());
    }

    @Test
    public void searchAsUser() throws Exception {
        log.info("Test search expenses as user");
        mockMvc.perform(search(user))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isEmpty());
    }

    @Test
    public void searchAsOwner() throws Exception {
        log.info("Test search expenses as owner");
        mockMvc.perform(search(owner))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isNotEmpty());
    }

    @Test
    public void searchAsClient() throws Exception {
        log.info("Test search expenses as client");
        mockMvc.perform(search(client))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isEmpty());
    }

    @Test
    public void searchAsAnonymous() throws Exception {
        log.info("Test search expenses as anonymous");
        mockMvc.perform(search(null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void createAsRoot() throws Exception {
        log.info("Test create expense as root");
        MExpense mp = new MExpense();
        mockMvc.perform(create(root, mp, business, pound, null))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsUser() throws Exception {
        log.info("Test create expense as user");
        MExpense mp = new MExpense();
        mockMvc.perform(create(user, mp, business, pound, null))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsOwner() throws Exception {
        log.info("Test create expense as owner");
        MExpense mp = new MExpense();
        mockMvc.perform(create(owner, mp, business, pound, null))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsClient() throws Exception {
        log.info("Test create expense as client");
        MExpense mp = new MExpense();
        mockMvc.perform(create(client, mp, business, pound, null))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsAnonymous() throws Exception {
        log.info("Test create expense as anonymous");
        MExpense mp = new MExpense();
        mockMvc.perform(create(null, mp, business, pound, null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getAsRoot() throws Exception {
        log.info("Test get expense as root");
        mockMvc.perform(get(root, full))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsUser() throws Exception {
        log.info("Test get expense as user");
        mockMvc.perform(get(user, full))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsOwner() throws Exception {
        log.info("Test get expense as owner");
        mockMvc.perform(get(owner, full))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsClient() throws Exception {
        log.info("Test get expense as client");
        mockMvc.perform(get(client, full))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsAnonymous() throws Exception {
        log.info("Test get expense as anonymous");
        mockMvc.perform(get(null, full))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateAsRoot() throws Exception {
        log.info("Test update expense as root");
        MExpense mp = new MExpense();
        mockMvc.perform(put(root, full, mp, business, pound, null))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateAsUser() throws Exception {
        log.info("Test update expense as user");
        MExpense mp = new MExpense();
        mockMvc.perform(put(user, full, mp, business, pound, null))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsOwner() throws Exception {
        log.info("Test update expense as owner");
        MExpense mp = new MExpense();
        mockMvc.perform(put(owner, full, mp, business, pound, null))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateAsClient() throws Exception {
        log.info("Test update expense as client");
        MExpense mp = new MExpense();
        mockMvc.perform(put(client, full, mp, business, pound, null))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsAnonymous() throws Exception {
        log.info("Test update expense as anonymous");
        MExpense mp = new MExpense();
        mockMvc.perform(put(null, full, mp, business, pound, null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateDeletedAsRoot() throws Exception {
        log.info("Test update deleted expense as root");
        expenseIntegrationConfig.delete(empty);
        MExpense mp = new MExpense();
        mockMvc.perform(put(root, empty, mp, business, pound, null))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void updateStatusAsRoot() throws Exception {
        log.info("Test update expense status as root");
        mockMvc.perform(status(root, full, ExpenseStatus.CLOSED))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateStatusAsUser() throws Exception {
        log.info("Test update expense status as user");
        mockMvc.perform(status(user, full, ExpenseStatus.CLOSED))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateStatusAsOwner() throws Exception {
        log.info("Test update expense status as owner");
        mockMvc.perform(status(owner, full, ExpenseStatus.CLOSED))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateStatusAsClient() throws Exception {
        log.info("Test update expense status as client");
        mockMvc.perform(status(client, full, ExpenseStatus.CLOSED))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateStatusAsAnonymous() throws Exception {
        log.info("Test update expense status as anonymous");
        mockMvc.perform(status(null, full, ExpenseStatus.CLOSED))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateDeletedStatusAsRoot() throws Exception {
        log.info("Test update expense status as root");
        expenseIntegrationConfig.delete(full);
        mockMvc.perform(status(root, full, ExpenseStatus.CLOSED))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteAsRoot() throws Exception {
        log.info("Test delete expense as root");
        mockMvc.perform(delete(root, full))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteAsUser() throws Exception {
        log.info("Test delete expense as user");
        mockMvc.perform(delete(user, full))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsClient() throws Exception {
        log.info("Test delete expense as client");
        mockMvc.perform(delete(client, full))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsAnonymous() throws Exception {
        log.info("Test delete expense as anonymous");
        mockMvc.perform(delete(null, full))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void deleteDeletedAsRoot() throws Exception {
        log.info("Test delete deleted expense as root");
        expenseIntegrationConfig.delete(full);
        mockMvc.perform(delete(root, full))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void searchDiscountsAsRoot() throws Exception {
        log.info("Test search discounts as root");
        mockMvc.perform(searchDiscounts(root, full))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void searchDiscountsAsOwner() throws Exception {
        log.info("Test search discounts as owner");
        mockMvc.perform(searchDiscounts(owner, full))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void searchDiscountsAsOther() throws Exception {
        log.info("Test search discounts as other");
        mockMvc.perform(searchDiscounts(user, full))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchDiscountsAsClient() throws Exception {
        log.info("Test search discounts as client");
        mockMvc.perform(searchDiscounts(client, full))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchDiscountsAsAnonymous() throws Exception {
        log.info("Test search discounts as anonymous");
        mockMvc.perform(searchDiscounts(null, full))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void addDiscountAsRoot() throws Exception {
        log.info("Test add discount to expense as root");
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(10.0));
        mockMvc.perform(addDiscount(root, empty, md))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void addDiscountAsUser() throws Exception {
        log.info("Test add discount to expense as user");
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(10.0));
        mockMvc.perform(addDiscount(user, empty, md))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void addDiscountAsOwner() throws Exception {
        log.info("Test add discount to expense as owner");
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(10.0));
        mockMvc.perform(addDiscount(owner, empty, md))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }


    @Test
    public void addDiscountAsClient() throws Exception {
        log.info("Test add discount to expense as client");
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(10.0));
        mockMvc.perform(addDiscount(client, empty, md))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void addDiscountAsAnonymous() throws Exception {
        log.info("Test add discount to expense as anonymous");
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(10.0));
        mockMvc.perform(addDiscount(null, empty, md))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void addDiscountToClosedAsOwner() throws Exception {
        log.info("Test add discount to closed expense as owner");
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(10.0));
        mockMvc.perform(addDiscount(owner, closed, md))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void addDiscountToDeletedExpenseAsRoot() throws Exception {
        log.info("Test add discount to deleted expense as root");
        expenseIntegrationConfig.delete(empty);
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(10.0));
        mockMvc.perform(addDiscount(root, empty, md))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void editDiscountAsRoot() throws Exception {
        log.info("Test edit discount as root");
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDiscount(root, full, discount, md))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void editDiscountAsUser() throws Exception {
        log.info("Test edit discount as user");
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDiscount(user, full, discount, md))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void editDiscountAsOwner() throws Exception {
        log.info("Test edit discount as owner");
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDiscount(owner, full, discount, md))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void editDiscountAsClient() throws Exception {
        log.info("Test edit discount as client");
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDiscount(client, full, discount, md))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void editDiscountAsAnonymous() throws Exception {
        log.info("Test edit discount as anonymous");
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDiscount(null, full, discount, md))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void editDeletedDiscountAsRoot() throws Exception {
        log.info("Test edit deleted discount as root");
        expenseIntegrationConfig.delete(discount);
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDiscount(root, full, discount, md))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void editDiscountFromDeletedExpenseAsRoot() throws Exception {
        log.info("Test edit discount from deleted expense as root");
        expenseIntegrationConfig.delete(full);
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDiscount(root, full, discount, md))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void editDiscountFromDifferentExpenseAsRoot() throws Exception {
        log.info("Test edit discount from different expense as root");
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDiscount(root, empty, discount, md))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void editDiscountFromClosedAsOwner() throws Exception {
        log.info("Test edit discount from closed expense as owner");
        MDiscount md = new MDiscount();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDiscount(owner, closed, closedDiscount, md))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteDiscountAsRoot() throws Exception {
        log.info("Test delete discount as root");
        mockMvc.perform(deleteDiscount(root, full, discount))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteDiscountAsOwner() throws Exception {
        log.info("Test delete discount as owner");
        mockMvc.perform(deleteDiscount(owner, full, discount))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteDiscountAsUser() throws Exception {
        log.info("Test delete discount as user");
        mockMvc.perform(deleteDiscount(user, full, discount))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteDiscountAsClient() throws Exception {
        log.info("Test delete discount as client");
        mockMvc.perform(deleteDiscount(client, full, discount))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteDiscountAsAnonymous() throws Exception {
        log.info("Test delete discount as anonymous");
        mockMvc.perform(deleteDiscount(null, full, discount))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void deleteDeletedDiscountAsRoot() throws Exception {
        log.info("Test delete discount as root");
        expenseIntegrationConfig.delete(discount);
        mockMvc.perform(deleteDiscount(root, full, discount))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteDiscountFromDeletedExpenseAsRoot() throws Exception {
        log.info("Test discount from deleted expense as root");
        expenseIntegrationConfig.delete(full);
        mockMvc.perform(deleteDiscount(root, full, discount))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteDiscountFromDifferentExpenseAsRoot() throws Exception {
        log.info("Test discount from different expense as root");
        mockMvc.perform(deleteDiscount(root, empty, discount))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteDiscountFromClosedAsOwner() throws Exception {
        log.info("Test delete discount from closed expense as owner");
        mockMvc.perform(deleteDiscount(owner, closed, closedDiscount))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void searchDetailsAsRoot() throws Exception {
        log.info("Test search details as root");
        mockMvc.perform(searchDetails(root, full))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void searchDetailsAsOwner() throws Exception {
        log.info("Test search details as owner");
        mockMvc.perform(searchDetails(owner, full))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void searchDetailsAsOther() throws Exception {
        log.info("Test search details as other");
        mockMvc.perform(searchDetails(user, full))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchDetailsAsClient() throws Exception {
        log.info("Test search details as client");
        mockMvc.perform(searchDetails(client, full))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchDetailsAsAnonymous() throws Exception {
        log.info("Test search details as anonymous");
        mockMvc.perform(searchDetails(null, full))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void addDetailAsRoot() throws Exception {
        log.info("Test add detail to expense as root");
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(10.0));
        mockMvc.perform(addDetail(root, empty, product, md))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void addDetailAsUser() throws Exception {
        log.info("Test add detail to expense as user");
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(10.0));
        mockMvc.perform(addDetail(user, empty, product, md))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void addDetailAsOwner() throws Exception {
        log.info("Test add detail to expense as owner");
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(10.0));
        mockMvc.perform(addDetail(owner, empty, product, md))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void addDetailAsClient() throws Exception {
        log.info("Test add detail to expense as client");
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(10.0));
        mockMvc.perform(addDetail(client, empty, product, md))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void addDetailAsAnonymous() throws Exception {
        log.info("Test add detail to expense as anonymous");
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(10.0));
        mockMvc.perform(addDetail(null, empty, product, md))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void addDetailToDeletedExpenseAsRoot() throws Exception {
        log.info("Test add detail to deleted expense as root");
        expenseIntegrationConfig.delete(empty);
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(10.0));
        mockMvc.perform(addDetail(root, empty, product, md))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void addDetailWithDeletedProductAsRoot() throws Exception {
        log.info("Test add detail with deleted product as root");
        productIntegrationConfig.delete(product);
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(10.0));
        mockMvc.perform(addDetail(root, empty, product, md))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void addDetailToClosedAsOwner() throws Exception {
        log.info("Test add detail to closed expense as owner");
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(10.0));
        mockMvc.perform(addDetail(owner, closed, product, md))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void editDetailAsRoot() throws Exception {
        log.info("Test edit detail as root");
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDetail(root, full, product, detail, md))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void editDetailAsUser() throws Exception {
        log.info("Test edit detail as user");
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDetail(user, full, product, detail, md))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void editDetailAsOwner() throws Exception {
        log.info("Test edit detail as owner");
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDetail(owner, full, product, detail, md))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void editDetailAsClient() throws Exception {
        log.info("Test edit detail as client");
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDetail(client, full, product, detail, md))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void editDetailAsAnonymous() throws Exception {
        log.info("Test edit detail as anonymous");
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDetail(null, full, product, detail, md))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void editDeletedDetailAsRoot() throws Exception {
        log.info("Test edit deleted detail as root");
        expenseIntegrationConfig.delete(detail);
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDetail(root, full, product, detail, md))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void editDetailFromDeletedExpenseAsRoot() throws Exception {
        log.info("Test edit detail from deleted expense as root");
        expenseIntegrationConfig.delete(full);
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDetail(root, full, product, detail, md))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void editDetailWithDeletedProductAsRoot() throws Exception {
        log.info("Test edit detail with deleted product as root");
        productIntegrationConfig.delete(product);
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDetail(root, full, product, detail, md))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void editDetailFromDifferentExpenseAsRoot() throws Exception {
        log.info("Test edit detail from different expense as root");
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDetail(root, empty, product, detail, md))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void editDetailFromClosedAsOwner() throws Exception {
        log.info("Test edit detail from closed expense as owner");
        MDetail md = new MDetail();
        md.setAmount(new BigDecimal(99.99));
        mockMvc.perform(editDetail(owner, closed, product, closedDetail, md))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteDetailAsRoot() throws Exception {
        log.info("Test delete detail as root");
        mockMvc.perform(deleteDetail(root, full, detail))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteDetailAsOwner() throws Exception {
        log.info("Test delete detail as owner");
        mockMvc.perform(deleteDetail(owner, full, detail))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteDetailAsUser() throws Exception {
        log.info("Test delete detail as user");
        mockMvc.perform(deleteDetail(user, full, detail))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteDetailAsClient() throws Exception {
        log.info("Test delete detail as client");
        mockMvc.perform(deleteDetail(client, full, detail))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteDetailAsAnonymous() throws Exception {
        log.info("Test delete detail as anonymous");
        mockMvc.perform(deleteDetail(null, full, detail))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void deleteDeletedDetailAsRoot() throws Exception {
        log.info("Test delete detail as root");
        expenseIntegrationConfig.delete(detail);
        mockMvc.perform(deleteDetail(root, full, detail))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteDetailFromDeletedExpenseAsRoot() throws Exception {
        log.info("Test detail from deleted expense as root");
        expenseIntegrationConfig.delete(full);
        mockMvc.perform(deleteDetail(root, full, detail))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteDetailFromDifferentExpenseAsRoot() throws Exception {
        log.info("Test detail from different expense as root");
        mockMvc.perform(deleteDetail(root, empty, detail))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteDetailFromClosedAsOwner() throws Exception {
        log.info("Test delete detail from closed expense as owner");
        mockMvc.perform(deleteDetail(owner, closed, closedDetail))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


    private MockHttpServletRequestBuilder search(User who) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/expenses")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder create(User who, MExpense toCreate, Business business, Currency currency, ExchangeRate exchangeRate) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/expenses")
                .param("businessId", business.getId().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(toCreate));
        if (currency != null) {
            request.param("currencyId", currency.getId().toString());
        }
        if (exchangeRate != null) {
            request.param("exchangeRateId", exchangeRate.getId().toString());
        }
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder get(User who, Expense toGet) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/expenses/{expenseId}", toGet.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder put(User who, Expense toEdit, MExpense edition, Business business, Currency currency, ExchangeRate exchangeRate) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/expenses/{expenseId}", toEdit.getId())
                .param("businessId", business.getId().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(edition));
        if (currency != null) {
            request.param("currencyId", currency.getId().toString());
        }
        if (exchangeRate != null) {
            request.param("exchangeRateId", exchangeRate.getId().toString());
        }
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder status(User who, Expense toEdit, ExpenseStatus status) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/expenses/{expenseId}/status", toEdit.getId())
                .param("status", status.getName())
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder delete(User who, Expense toDelete) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/expenses/{expenseId}", toDelete.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder searchDiscounts(User who, Expense expense) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/expenses/{expenseId}/discounts", expense.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder addDiscount(User who, Expense expense, MDiscount md) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/expenses/{expenseId}/discounts", expense.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(md));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder editDiscount(User who, Expense expense, Discount toEdit, MDiscount md) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/expenses/{expenseId}/discounts/{discountId}", expense.getId(), toEdit.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(md));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder deleteDiscount(User who, Expense expense, Discount toDelete) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/expenses/{expenseId}/discounts/{discountId}", expense.getId(), toDelete.getId())
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder searchDetails(User who, Expense expense) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/expenses/{expenseId}/details", expense.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder addDetail(User who, Expense expense, Product p, MDetail md) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/expenses/{expenseId}/details", expense.getId())
                .param("productId", p.getId().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(md));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder editDetail(User who, Expense expense, Product p, Detail toEdit, MDetail md) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/expenses/{expenseId}/details/{detailId}", expense.getId(), toEdit.getId())
                .param("productId", p.getId().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(md));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder deleteDetail(User who, Expense expense, Detail toDelete) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/expenses/{expenseId}/details/{detailId}", expense.getId(), toDelete.getId())
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

}
