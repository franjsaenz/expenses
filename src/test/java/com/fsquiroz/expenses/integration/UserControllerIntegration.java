package com.fsquiroz.expenses.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.expenses.config.UserIntegrationConfig;
import com.fsquiroz.expenses.entity.db.Role;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MResetPassword;
import com.fsquiroz.expenses.entity.json.MUser;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@ActiveProfiles("test")
@Slf4j
public class UserControllerIntegration {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    private User root;

    private User user;

    private User owner;

    private User client;

    @Before
    public void setup() {
        userIntegrationConfig.setup();
        this.root = userIntegrationConfig.getRoot();
        this.user = userIntegrationConfig.getUser();
        this.owner = userIntegrationConfig.getOwner();
        this.client = userIntegrationConfig.getClient();
    }

    @After
    public void cleanup() {
        this.userIntegrationConfig.cleanup();
    }

    @Test
    public void searchAsRoot() throws Exception {
        log.info("Test search users as root");
        mockMvc.perform(search(root))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").isNotEmpty());
    }

    @Test
    public void searchAsUser() throws Exception {
        log.info("Test search users as user");
        mockMvc.perform(search(user))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsOwner() throws Exception {
        log.info("Test search users as owner");
        mockMvc.perform(search(owner))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsClient() throws Exception {
        log.info("Test search users as client");
        mockMvc.perform(search(client))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsAnonymous() throws Exception {
        log.info("Test search users as anonymous");
        mockMvc.perform(search(null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void createAsRoot() throws Exception {
        log.info("Test create user as root");
        MUser mu = new MUser();
        mu.setRole(Role.USER);
        mu.setEmail("some@test.com");
        mu.setPassword("123456");
        mockMvc.perform(create(root, mu))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsUser() throws Exception {
        log.info("Test create user as user");
        MUser mu = new MUser();
        mu.setRole(Role.USER);
        mu.setEmail("some@test.com");
        mu.setPassword("123456");
        mockMvc.perform(create(user, mu))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsClient() throws Exception {
        log.info("Test create user as client");
        MUser mu = new MUser();
        mu.setRole(Role.USER);
        mu.setEmail("some@test.com");
        mu.setPassword("123456");
        mockMvc.perform(create(client, mu))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsAnonymous() throws Exception {
        log.info("Test create user as anonymous");
        MUser mu = new MUser();
        mu.setRole(Role.USER);
        mu.setEmail("some@test.com");
        mu.setPassword("123456");
        mockMvc.perform(create(null, mu))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getSelfAsRoot() throws Exception {
        log.info("Test get self user as root");
        mockMvc.perform(getSelf(root))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getSelfAsUser() throws Exception {
        log.info("Test get self user as user");
        mockMvc.perform(getSelf(user))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getSelfAsClient() throws Exception {
        log.info("Test get self user as client");
        mockMvc.perform(getSelf(client))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getOtherAsRoot() throws Exception {
        log.info("Test get other user as root");
        mockMvc.perform(get(root, client))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getOtherAsUser() throws Exception {
        log.info("Test get other user as user");
        mockMvc.perform(get(user, root))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getOtherAsClient() throws Exception {
        log.info("Test get other user as client");
        mockMvc.perform(get(client, root))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getOtherAsAnonymous() throws Exception {
        log.info("Test get other user as anonymous");
        mockMvc.perform(get(null, root))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void editSelfAsRoot() throws Exception {
        log.info("Test edit self user as root");
        MUser edition = new MUser();
        mockMvc.perform(putSelf(root, edition))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void editSelfAsUser() throws Exception {
        log.info("Test edit self user as user");
        MUser edition = new MUser();
        mockMvc.perform(putSelf(user, edition))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void editSelfAsClient() throws Exception {
        log.info("Test edit self user as client");
        MUser edition = new MUser();
        mockMvc.perform(putSelf(client, edition))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void editOtherAsRoot() throws Exception {
        log.info("Test edit other user as root");
        MUser edition = new MUser();
        mockMvc.perform(put(root, user, edition))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void editOtherAsUser() throws Exception {
        log.info("Test edit other user as user");
        MUser edition = new MUser();
        mockMvc.perform(put(user, root, edition))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void editOtherAsClient() throws Exception {
        log.info("Test edit other user as client");
        MUser edition = new MUser();
        mockMvc.perform(put(client, user, edition))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void editOtherAsAnonymous() throws Exception {
        log.info("Test edit other user as anonymous");
        MUser edition = new MUser();
        mockMvc.perform(put(null, user, edition))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void deleteSelfAsRoot() throws Exception {
        log.info("Test delete self user as root");
        mockMvc.perform(deleteSelf(root))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteSelfAsUser() throws Exception {
        log.info("Test delete self user as user");
        mockMvc.perform(deleteSelf(user))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteSelfAsClient() throws Exception {
        log.info("Test delete self user as client");
        mockMvc.perform(deleteSelf(client))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteOtherAsRoot() throws Exception {
        log.info("Test delete other user as root");
        mockMvc.perform(delete(root, user))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteOtherAsUser() throws Exception {
        log.info("Test delete other user as user");
        mockMvc.perform(delete(user, client))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteOtherAsClient() throws Exception {
        log.info("Test delete other user as client");
        mockMvc.perform(delete(client, user))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteOtherAsAnonymous() throws Exception {
        log.info("Test delete other user as anonymous");
        mockMvc.perform(delete(null, user))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateSelfPasswordAsRoot() throws Exception {
        log.info("Test update self password as root");
        MResetPassword pwd = new MResetPassword();
        pwd.setOldPassword(root.getPassword());
        pwd.setNewPassword("newPass");
        mockMvc.perform(updateSelfPassword(root, pwd))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateSelfPasswordAsUser() throws Exception {
        log.info("Test update self password as user");
        MResetPassword pwd = new MResetPassword();
        pwd.setOldPassword(user.getPassword());
        pwd.setNewPassword("newPass");
        mockMvc.perform(updateSelfPassword(user, pwd))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateSelfPasswordAsClient() throws Exception {
        log.info("Test update self password as root");
        MResetPassword pwd = new MResetPassword();
        pwd.setOldPassword("oldPass");
        pwd.setNewPassword("newPass");
        mockMvc.perform(updateSelfPassword(client, pwd))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void updateUserPasswordAsRoot() throws Exception {
        log.info("Test update user password as root");
        MResetPassword pwd = new MResetPassword();
        pwd.setOldPassword(user.getPassword());
        pwd.setNewPassword("newPass");
        mockMvc.perform(updatePassword(root, user, pwd))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateClientPasswordAsRoot() throws Exception {
        log.info("Test update client password as root");
        MResetPassword pwd = new MResetPassword();
        pwd.setOldPassword("oldPass");
        pwd.setNewPassword("newPass");
        mockMvc.perform(updatePassword(root, client, pwd))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void updateRootPasswordAsUser() throws Exception {
        log.info("Test update root password as user");
        MResetPassword pwd = new MResetPassword();
        pwd.setOldPassword(root.getPassword());
        pwd.setNewPassword("newPass");
        mockMvc.perform(updatePassword(user, root, pwd))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateRootPasswordAsClient() throws Exception {
        log.info("Test update root password as client");
        MResetPassword pwd = new MResetPassword();
        pwd.setOldPassword(root.getPassword());
        pwd.setNewPassword("newPass");
        mockMvc.perform(updatePassword(client, root, pwd))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateRootPasswordAsAnonymous() throws Exception {
        log.info("Test update root password as anonymous");
        MResetPassword pwd = new MResetPassword();
        pwd.setOldPassword(root.getPassword());
        pwd.setNewPassword("newPass");
        mockMvc.perform(updatePassword(null, root, pwd))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getSelfTokenAsRoot() throws Exception {
        log.info("Test get self token as root");
        mockMvc.perform(getSelfToken(root))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void getSelfTokenAsUser() throws Exception {
        log.info("Test get self token as user");
        mockMvc.perform(getSelfToken(user))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getSelfTokenAsClient() throws Exception {
        log.info("Test get self token as client");
        mockMvc.perform(getSelfToken(client))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getOtherTokenAsRoot() throws Exception {
        log.info("Test get other token as root");
        mockMvc.perform(getToken(root, user))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getOtherTokenAsUser() throws Exception {
        log.info("Test get other token as user");
        mockMvc.perform(getToken(user, root))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getOtherTokenAsClient() throws Exception {
        log.info("Test get other token as client");
        mockMvc.perform(getToken(client, user))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getOtherTokenAsAnonymous() throws Exception {
        log.info("Test get other token as anonymous");
        mockMvc.perform(getToken(null, user))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getClientTokenAsRoot() throws Exception {
        log.info("Test get client token as root");
        mockMvc.perform(getToken(root, client))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getClientTokenAsUser() throws Exception {
        log.info("Test get client token as user");
        mockMvc.perform(getToken(user, client))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getClientTokenAsClient() throws Exception {
        log.info("Test get client token as client");
        mockMvc.perform(getToken(client, client))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getClientTokenAsAnonymous() throws Exception {
        log.info("Test get client token as anonymous");
        mockMvc.perform(getToken(null, client))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    private MockHttpServletRequestBuilder search(User who) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder create(User who, MUser toCreate) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(toCreate));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder getSelf(User who) throws Exception {
        return get(who, who);
    }

    private MockHttpServletRequestBuilder get(User who, User toGet) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/users/{userId}", toGet.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder putSelf(User who, MUser edition) throws Exception {
        return put(who, who, edition);
    }

    private MockHttpServletRequestBuilder put(User who, User toEdit, MUser edition) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/users/{userId}", toEdit.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(edition));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder deleteSelf(User who) throws Exception {
        return delete(who, who);
    }

    private MockHttpServletRequestBuilder delete(User who, User toDelete) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/users/{userId}", toDelete.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder updateSelfPassword(User who, MResetPassword password) throws Exception {
        return updatePassword(who, who, password);
    }

    private MockHttpServletRequestBuilder updatePassword(User who, User toEdit, MResetPassword password) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/users/{userId}/password", toEdit.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(password));
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder getSelfToken(User who) throws Exception {
        return getToken(who, who);
    }

    private MockHttpServletRequestBuilder getToken(User who, User toGet) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/users/{userId}/token", toGet.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);
        userIntegrationConfig.authenticate(request, who);
        return request;
    }

}
