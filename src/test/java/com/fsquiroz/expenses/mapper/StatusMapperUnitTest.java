package com.fsquiroz.expenses.mapper;

import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MStatus;
import com.fsquiroz.expenses.exception.UnauthorizedException;
import com.fsquiroz.expenses.security.SecurityCheck;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.data.TemporalUnitWithinOffset;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class StatusMapperUnitTest {

    @MockBean
    private SecurityCheck securityCheck;

    private StatusMapper mapper;

    @Before
    public void setup() {
        this.mapper = new StatusMapper(securityCheck);
    }

    @Test
    public void givenAuthenticatedUserWhenMapStatusThenFullStatus() {
        MStatus status = new MStatus();
        status.setMessage("Up");
        status.setName("expenses");
        status.setVersion("test");
        status.setBuild(Instant.now());
        status.setStartUp(Instant.now());

        Mockito.doReturn(new User()).when(securityCheck).getAuthentication();

        MStatus response = mapper.map(status);

        assertThat(response)
                .isNotNull()
                .hasFieldOrPropertyWithValue("message", "Up")
                .hasFieldOrPropertyWithValue("name", "expenses")
                .hasFieldOrPropertyWithValue("version", "test");
        assertThat(response.getBuild())
                .isNotNull()
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(200, ChronoUnit.MILLIS));
        assertThat(response.getStartUp())
                .isNotNull()
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(200, ChronoUnit.MILLIS));
    }

    @Test
    public void givenUnauthenticatedUserWhenMapStatusThenFullStatus() {
        MStatus status = new MStatus();
        status.setMessage("Up");
        status.setName("expenses");
        status.setVersion("test");
        status.setBuild(Instant.now());
        status.setStartUp(Instant.now());

        Mockito.doThrow(UnauthorizedException.byInvalidCredentials()).when(securityCheck).getAuthentication();

        MStatus response = mapper.map(status);

        assertThat(response)
                .isNotNull()
                .hasFieldOrPropertyWithValue("message", "Up")
                .hasFieldOrPropertyWithValue("name", null)
                .hasFieldOrPropertyWithValue("version", null);
        assertThat(response.getBuild())
                .isNull();
        assertThat(response.getStartUp())
                .isNull();
    }
}
