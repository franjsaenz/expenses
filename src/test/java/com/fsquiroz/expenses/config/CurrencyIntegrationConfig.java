package com.fsquiroz.expenses.config;

import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;

@Component
@ActiveProfiles("test")
public class CurrencyIntegrationConfig {

    @Autowired
    private CurrencyRepository currencyRepository;

    private Currency dollar;

    private Currency euro;

    private Currency pound;

    public void setup() {
        dollar = new Currency();
        dollar.setCreated(Instant.now());
        dollar.setName("United State Dollar");
        dollar.setCode("USD");
        dollar.setIdentifier("U$D");
        dollar = currencyRepository.save(dollar);
        euro = new Currency();
        euro.setCreated(Instant.now());
        euro.setName("Euro");
        euro.setCode("EUR");
        euro.setIdentifier("€");
        euro = currencyRepository.save(euro);
        pound = new Currency();
        pound.setCreated(Instant.now());
        pound.setName("United Kingdom Pound");
        pound.setCode("GBP");
        pound.setIdentifier("£");
        pound = currencyRepository.save(pound);
    }

    public void cleanup() {
        currencyRepository.deleteAll();
    }

    public void delete(Currency c) {
        c.setDeleted(Instant.now());
        currencyRepository.save(c);
    }

    public Currency getDollar() {
        return dollar;
    }

    public Currency getEuro() {
        return euro;
    }

    public Currency getPound() {
        return pound;
    }

}
