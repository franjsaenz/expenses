package com.fsquiroz.expenses.config;

import com.fsquiroz.expenses.entity.json.MStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.Instant;

@Configuration
@Profile("test")
@Slf4j
public class TestBeans {

    private MStatus status;

    private PasswordEncoder encoder;

    @Bean
    @Primary
    public MStatus status() {
        if (status == null) {
            String title = "Expenses";
            Instant now = Instant.now();
            String version = "TESTING";
            status = new MStatus(title, version, now, now, "Up");
        }
        return status;
    }

    @Bean
    @Primary
    public PasswordEncoder passwordEncoder() {
        if (encoder == null) {
            encoder = new PasswordEncoder() {

                @Override
                public String encode(CharSequence rawPassword) {
                    return rawPassword == null ? null : rawPassword.toString();
                }

                @Override
                public boolean matches(CharSequence rawPassword, String encodedPassword) {
                    return rawPassword == null ? encodedPassword == null : rawPassword.toString().equals(encodedPassword);
                }
            };
        }
        return encoder;
    }

}
