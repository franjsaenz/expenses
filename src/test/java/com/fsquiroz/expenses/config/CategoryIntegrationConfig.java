package com.fsquiroz.expenses.config;

import com.fsquiroz.expenses.entity.db.Category;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;

@Component
@ActiveProfiles("test")
public class CategoryIntegrationConfig {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    private Category first;

    private Category second;

    public void setup() {
        User owner = userIntegrationConfig.getOwner();
        first = new Category();
        first.setCreated(Instant.now());
        first.setName("First");
        first.setOwner(owner);
        first = categoryRepository.save(first);
        second = new Category();
        second.setCreated(Instant.now());
        second.setName("Second");
        second.setOwner(owner);
        second = categoryRepository.save(second);
    }

    public void cleanup() {
        categoryRepository.deleteAll();
    }

    public void delete(Category c) {
        c.setDeleted(Instant.now());
        categoryRepository.save(c);
    }

    public Category getFirst() {
        return first;
    }

    public Category getSecond() {
        return second;
    }

}
