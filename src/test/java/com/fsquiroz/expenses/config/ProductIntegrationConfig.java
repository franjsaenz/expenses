package com.fsquiroz.expenses.config;

import com.fsquiroz.expenses.entity.db.Product;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;

@Component
@ActiveProfiles("test")
public class ProductIntegrationConfig {

    @Autowired
    private CategoryIntegrationConfig categoryIntegrationConfig;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    @Autowired
    private ProductRepository productRepository;

    private Product first;

    private Product second;

    public void setup() {
        User owner = userIntegrationConfig.getOwner();
        first = new Product();
        first.setCreated(Instant.now());
        first.setCategory(categoryIntegrationConfig.getFirst());
        first.setName("First product");
        first.setOwner(owner);
        first = productRepository.save(first);
        second = new Product();
        second.setCreated(Instant.now());
        second.setCategory(categoryIntegrationConfig.getSecond());
        second.setName("Second category");
        second.setOwner(owner);
        second = productRepository.save(second);
    }

    public void cleanup() {
        productRepository.deleteAll();
    }

    public Product getFirst() {
        return first;
    }

    public Product getSecond() {
        return second;
    }

    public Product get(Product product) {
        return productRepository.findById(product.getId()).orElse(null);
    }

    public void delete(Product p) {
        p.setDeleted(Instant.now());
        productRepository.save(p);
    }

}
