package com.fsquiroz.expenses.config;

import com.fsquiroz.expenses.entity.db.Business;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.repository.BusinessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;

@Component
@ActiveProfiles("test")
public class BusinessIntegrationConfig {

    @Autowired
    private BusinessRepository businessRepository;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    private Business first;

    private Business second;

    public void setup() {
        User owner = userIntegrationConfig.getOwner();
        first = new Business();
        first.setCreated(Instant.now());
        first.setName("First Business");
        first.setOwner(owner);
        first = businessRepository.save(first);
        second = new Business();
        second.setCreated(Instant.now());
        second.setName("Second Business");
        second.setOwner(owner);
        second = businessRepository.save(second);
    }

    public void cleanup() {
        businessRepository.deleteAll();
    }

    public void delete(Business c) {
        c.setDeleted(Instant.now());
        businessRepository.save(c);
    }

    public Business getFirst() {
        return first;
    }

    public Business getSecond() {
        return second;
    }

}
