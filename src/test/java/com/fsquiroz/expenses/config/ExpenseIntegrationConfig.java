package com.fsquiroz.expenses.config;

import com.fsquiroz.expenses.entity.db.*;
import com.fsquiroz.expenses.repository.DetailRepository;
import com.fsquiroz.expenses.repository.DiscountRepository;
import com.fsquiroz.expenses.repository.ExpenseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.time.Instant;

import lombok.Getter;

@Component
@ActiveProfiles("test")
public class ExpenseIntegrationConfig {

    @Autowired
    private BusinessIntegrationConfig businessIntegrationConfig;

    @Autowired
    private CurrencyIntegrationConfig currencyIntegrationConfig;

    @Autowired
    private ProductIntegrationConfig productIntegrationConfig;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    @Autowired
    private ExpenseRepository expenseRepository;

    @Autowired
    private DiscountRepository discountRepository;

    @Autowired
    private DetailRepository detailRepository;

    @Getter
    private Expense expense;

    @Getter
    private Expense emptyExpense;

    @Getter
    private Expense closedExpense;

    @Getter
    private Expense completeExpense;

    @Getter
    private Discount discount;

    @Getter
    private Discount closedDiscount;

    @Getter
    private Detail firstDetail;

    @Getter
    private Detail closedDetail;

    @Getter
    private Detail firstProductADetail;

    @Getter
    private Detail secondProductADetail;

    @Getter
    private Detail firstProductBDetail;

    public void setup() {
        User owner = userIntegrationConfig.getOwner();
        Business b = businessIntegrationConfig.getFirst();
        Currency dollar = currencyIntegrationConfig.getDollar();
        Product p1 = productIntegrationConfig.getFirst();
        Product p2 = productIntegrationConfig.getSecond();
        expense = new Expense();
        expense.setCreated(Instant.now());
        expense.setStatus(ExpenseStatus.OPENED);
        expense.setBusiness(b);
        expense.setCurrency(dollar);
        expense.setOwner(owner);
        expense = expenseRepository.save(expense);
        emptyExpense = new Expense();
        emptyExpense.setStatus(ExpenseStatus.OPENED);
        emptyExpense.setCreated(Instant.now());
        emptyExpense.setBusiness(b);
        emptyExpense.setOwner(owner);
        emptyExpense = expenseRepository.save(emptyExpense);
        closedExpense = new Expense();
        closedExpense.setCreated(Instant.now());
        closedExpense.setStatus(ExpenseStatus.CLOSED);
        closedExpense.setBusiness(b);
        closedExpense.setCurrency(dollar);
        closedExpense.setOwner(owner);
        closedExpense = expenseRepository.save(closedExpense);
        completeExpense = new Expense();
        completeExpense.setCreated(Instant.now());
        completeExpense.setStatus(ExpenseStatus.CLOSED);
        completeExpense.setBusiness(b);
        completeExpense.setCurrency(dollar);
        completeExpense.setOwner(owner);
        completeExpense = expenseRepository.save(completeExpense);
        discount = new Discount();
        discount.setCreated(Instant.now());
        discount.setAmount(BigDecimal.valueOf(10));
        discount.setExpense(expense);
        discount.setOwner(owner);
        discount = discountRepository.save(discount);
        firstDetail = new Detail();
        firstDetail.setCreated(Instant.now());
        firstDetail.setProduct(p1);
        firstDetail.setExpense(expense);
        firstDetail.setAmount(new BigDecimal(100));
        firstDetail.setOwner(owner);
        firstDetail = detailRepository.save(firstDetail);
        closedDetail = new Detail();
        closedDetail.setCreated(Instant.now());
        closedDetail.setProduct(p2);
        closedDetail.setExpense(closedExpense);
        closedDetail.setAmount(new BigDecimal(50));
        closedDetail.setOwner(owner);
        closedDetail = detailRepository.save(closedDetail);
        closedDiscount = new Discount();
        closedDiscount.setCreated(Instant.now());
        closedDiscount.setAmount(new BigDecimal(10));
        closedDiscount.setExpense(closedExpense);
        closedDiscount.setOwner(owner);
        closedDiscount = discountRepository.save(closedDiscount);

        firstProductADetail = new Detail();
        firstProductADetail.setCreated(Instant.now());
        firstProductADetail.setProduct(p1);
        firstProductADetail.setExpense(completeExpense);
        firstProductADetail.setAmount(new BigDecimal(100));
        firstProductADetail.setOwner(owner);
        firstProductADetail = detailRepository.save(firstProductADetail);
        secondProductADetail = new Detail();
        secondProductADetail.setCreated(Instant.now());
        secondProductADetail.setProduct(p1);
        secondProductADetail.setExpense(completeExpense);
        secondProductADetail.setAmount(new BigDecimal(100));
        secondProductADetail.setOwner(owner);
        secondProductADetail = detailRepository.save(secondProductADetail);
        firstProductBDetail = new Detail();
        firstProductBDetail.setCreated(Instant.now());
        firstProductBDetail.setProduct(p2);
        firstProductBDetail.setExpense(completeExpense);
        firstProductBDetail.setAmount(new BigDecimal(100));
        firstProductBDetail.setOwner(owner);
        firstProductBDetail = detailRepository.save(firstProductBDetail);
    }

    public void cleanup() {
        detailRepository.deleteAll();
        discountRepository.deleteAll();
        expenseRepository.deleteAll();
    }

    public void delete(Expense e) {
        discount.setDeleted(Instant.now());
        discount = discountRepository.save(discount);
        firstDetail.setDeleted(Instant.now());
        firstDetail = detailRepository.save(firstDetail);
        e.setDeleted(Instant.now());
        expense = expenseRepository.save(e);
    }

    public void delete(Discount d) {
        d.setDeleted(Instant.now());
        discountRepository.save(d);
    }

    public void delete(Detail d) {
        d.setDeleted(Instant.now());
        detailRepository.save(d);
    }

    public Detail get(Detail detail) {
        return detailRepository.findById(detail.getId()).orElse(null);
    }
}
