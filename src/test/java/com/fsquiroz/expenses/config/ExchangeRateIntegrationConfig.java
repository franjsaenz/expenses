package com.fsquiroz.expenses.config;

import com.fsquiroz.expenses.entity.db.Currency;
import com.fsquiroz.expenses.entity.db.ExchangeRate;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.repository.ExchangeRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.time.Instant;

@Component
@ActiveProfiles("test")
public class ExchangeRateIntegrationConfig {

    @Autowired
    private CurrencyIntegrationConfig currencyIntegrationConfig;

    @Autowired
    private ExchangeRateRepository exchangeRateRepository;

    @Autowired
    private UserIntegrationConfig userIntegrationConfig;

    private ExchangeRate dollarToEuro;

    private ExchangeRate dollarToPound;

    public void setup() {
        Currency dollar = currencyIntegrationConfig.getDollar();
        Currency euro = currencyIntegrationConfig.getEuro();
        Currency pound = currencyIntegrationConfig.getPound();
        User owner = userIntegrationConfig.getOwner();
        dollarToEuro = new ExchangeRate();
        dollarToEuro.setCreated(Instant.now());
        dollarToEuro.setRegistered(Instant.now());
        dollarToEuro.setMultiplier(new BigDecimal(0.89));
        dollarToEuro.setFrom(dollar);
        dollarToEuro.setTo(euro);
        dollarToEuro.setOwner(owner);
        dollarToEuro = exchangeRateRepository.save(dollarToEuro);
        dollarToPound = new ExchangeRate();
        dollarToPound.setCreated(Instant.now());
        dollarToPound.setRegistered(Instant.now());
        dollarToPound.setMultiplier(new BigDecimal(0.77));
        dollarToPound.setFrom(dollar);
        dollarToPound.setTo(pound);
        dollarToPound.setOwner(owner);
        dollarToPound = exchangeRateRepository.save(dollarToPound);
    }

    public void cleanup() {
        exchangeRateRepository.deleteAll();
    }

    public void delete(ExchangeRate er) {
        er.setDeleted(Instant.now());
        exchangeRateRepository.save(er);
    }

    public ExchangeRate getDollarToEuro() {
        return dollarToEuro;
    }

    public ExchangeRate getDollarToPound() {
        return dollarToPound;
    }

}
