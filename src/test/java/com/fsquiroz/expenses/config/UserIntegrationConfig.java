package com.fsquiroz.expenses.config;

import com.fsquiroz.expenses.entity.db.Role;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.repository.UserRepository;
import com.fsquiroz.expenses.security.SecurityCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.time.Instant;

@Component
@ActiveProfiles("test")
public class UserIntegrationConfig {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SecurityCheck securityCheck;

    private User root;

    private User user;

    private User owner;

    private User client;

    public void setup() {
        root = new User();
        root.setPassword(passwordEncoder.encode("123456"));
        root.setEmail("super@test.com");
        root.setRole(Role.SUPER);
        root.setCreated(Instant.now());
        root = userRepository.save(root);
        user = new User();
        user.setPassword(passwordEncoder.encode("123456"));
        user.setEmail("user@test.com");
        user.setRole(Role.USER);
        user.setCreated(Instant.now());
        user = userRepository.save(user);
        owner = new User();
        owner.setPassword(passwordEncoder.encode("123456"));
        owner.setEmail("owner@test.com");
        owner.setRole(Role.USER);
        owner.setCreated(Instant.now());
        owner = userRepository.save(owner);
        client = new User();
        client.setEmail("client@test.com");
        client.setRole(Role.CLIENT);
        client.setCreated(Instant.now());
        client = userRepository.save(client);
    }

    public void cleanup() {
        userRepository.deleteAll();
    }

    public void authenticate(MockHttpServletRequestBuilder request, User user) {
        if (user != null) {
            String token = securityCheck.generateToken(null, null, null, user);
            request.header("Authorization", "Bearer " + token);
        }
    }

    public User getRoot() {
        return root;
    }

    public User getUser() {
        return user;
    }

    public User getOwner() {
        return owner;
    }

    public User getClient() {
        return client;
    }

    public void delete(User u) {
        u.setDeleted(Instant.now());
        userRepository.save(u);
    }

}
