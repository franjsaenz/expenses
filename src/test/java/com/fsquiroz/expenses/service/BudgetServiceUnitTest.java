package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.*;
import com.fsquiroz.expenses.entity.json.MBudget;
import com.fsquiroz.expenses.entity.json.MBudgetReport;
import com.fsquiroz.expenses.entity.json.MCurrency;
import com.fsquiroz.expenses.entity.json.MGroupDetail;
import com.fsquiroz.expenses.exception.AppException;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.ErrorCode;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.repository.BudgetRepository;
import com.fsquiroz.expenses.repository.StatRepository;
import com.fsquiroz.expenses.service.declaration.IBudgetItemService;
import com.fsquiroz.expenses.service.declaration.ICurrencyService;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.data.Offset;
import org.assertj.core.data.TemporalUnitWithinOffset;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.stubbing.answers.ReturnsArgumentAt;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class BudgetServiceUnitTest {

    @MockBean
    private IBudgetItemService budgetItemService;

    @MockBean
    private ICurrencyService currencyService;

    @MockBean
    private BudgetRepository budgetRepository;

    @MockBean
    private StatRepository statRepository;

    private BudgetService service;

    @Before
    public void setup() {
        service = new BudgetService(budgetItemService, currencyService, budgetRepository, statRepository);
    }

    @Test
    public void getById() {
        log.info("Test get budget by id");

        Budget b = new Budget();
        b.setId(1L);
        b.setCreated(Instant.now());

        Mockito.when(budgetRepository.findById(any()))
                .thenReturn(Optional.of(b));

        Budget resp = service.get(1L);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(b.getId());
        assertThat(resp.getCreated())
                .isCloseTo(b.getCreated(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
    }

    @Test
    public void getNotFound() {
        log.info("Test get not found budget");

        Mockito.when(budgetRepository.findById(any()))
                .thenReturn(Optional.empty());

        Exception resp = null;
        try {
            service.get(1L);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(NotFoundException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.NF_BY_ID);
    }

    @Test
    public void getIdNull() {
        log.info("Test get with null id");

        Exception resp = null;
        try {
            service.get(null);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_INVALID_ID_VALUE);
    }

    @Test
    public void report() {
        log.info("Test get budget report");

        Instant from = Instant.from(Instant.now().truncatedTo(ChronoUnit.DAYS));
        Instant to = Instant.from(Instant.now().plus(10, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
        Currency c = new Currency();
        c.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setFrom(from);
        b.setTo(to);
        Category cat1 = new Category();
        cat1.setId(1L);
        cat1.setName("Cat1");
        Category cat2 = new Category();
        cat2.setId(2L);
        cat2.setName("Cat2");
        Category cat4 = new Category();
        cat4.setId(4L);
        cat4.setName("Cat4");
        BudgetItem bi1 = new BudgetItem();
        bi1.setCategory(cat1);
        bi1.setAmount(new BigDecimal("10.00"));
        bi1.setBudget(b);
        BudgetItem bi2 = new BudgetItem();
        bi2.setCategory(cat2);
        bi2.setAmount(new BigDecimal("20.00"));
        bi2.setBudget(b);
        BudgetItem bi3 = new BudgetItem();
        bi3.setCategory(cat4);
        bi3.setAmount(new BigDecimal("5.00"));
        bi3.setBudget(b);
        MGroupDetail d1 = new MGroupDetail(1L, "Cat1", null, null, new BigDecimal("9.00"));
        MGroupDetail d2 = new MGroupDetail(2L, "Cat2", null, null, new BigDecimal("25.00"));
        MGroupDetail d3 = new MGroupDetail(3L, "Cat3", null, null, new BigDecimal("1.00"));

        Mockito.when(budgetItemService.list(any()))
                .thenReturn(Arrays.asList(bi1, bi2, bi3));
        Mockito.when(statRepository.totalsByCategoryAndCurrency(any(), any(), any(), any()))
                .thenReturn(Arrays.asList(d1, d2, d3));

        MBudgetReport resp = service.report(b, null);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getTotalExpected())
                .isCloseTo(new BigDecimal("35.00"), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.getTotalExpended())
                .isCloseTo(new BigDecimal("35.00"), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.getTotalRemainedToExpend())
                .isCloseTo(new BigDecimal("0.00"), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.getTotalUnaccounted())
                .isCloseTo(new BigDecimal("1.00"), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.getDetails())
                .hasSize(3);
    }

    @Test
    public void reportWithoutExpenses() {
        log.info("Test get budget report without expenses");

        Instant from = Instant.from(Instant.now().truncatedTo(ChronoUnit.DAYS));
        Instant to = Instant.from(Instant.now().plus(10, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
        Currency c = new Currency();
        c.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setFrom(from);
        b.setTo(to);

        Mockito.when(budgetItemService.list(any()))
                .thenReturn(new ArrayList<>());
        Mockito.when(statRepository.totalsByCategoryAndCurrency(any(), any(), any(), any()))
                .thenReturn(new ArrayList<>());

        MBudgetReport resp = service.report(b, null);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getTotalExpected())
                .isCloseTo(new BigDecimal("0.00"), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.getTotalExpended())
                .isCloseTo(new BigDecimal("0.00"), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.getTotalRemainedToExpend())
                .isCloseTo(new BigDecimal("0.00"), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.getTotalUnaccounted())
                .isCloseTo(new BigDecimal("0.00"), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.getDetails())
                .hasSize(0);
    }

    @Test
    public void create() {
        log.info("Test create budget");

        Instant from = Instant.from(Instant.now().truncatedTo(ChronoUnit.DAYS));
        Instant to = Instant.from(Instant.now().plus(10, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
        User u = new User();
        u.setId(1L);
        Currency c = new Currency();
        c.setId(1L);
        MBudget mb = new MBudget();
        mb.setFrom(from);
        mb.setTo(to);
        mb.setName("test");

        Mockito.when(budgetRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Budget resp = service.create(mb, c, u);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getOwner())
                .isEqualTo(u);
        assertThat(resp.getCurrency())
                .isEqualTo(c);
        assertThat(resp.getName())
                .isEqualTo("test");
        assertThat(resp.getFrom())
                .isCloseTo(from, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getTo())
                .isCloseTo(to, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
    }

    @Test
    public void createWithoutFrom() {
        log.info("Test create budget without from");

        Instant from = null;
        Instant to = Instant.from(Instant.now().plus(10, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
        User u = new User();
        u.setId(1L);
        Currency c = new Currency();
        c.setId(1L);
        MBudget mb = new MBudget();
        mb.setFrom(from);
        mb.setTo(to);
        mb.setName("test");

        Mockito.when(budgetRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;

        try {
            service.create(mb, c, u);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_MISSING_PARAM);
    }

    @Test
    public void createWithoutTo() {
        log.info("Test create budget without to");

        Instant from = Instant.from(Instant.now().truncatedTo(ChronoUnit.DAYS));
        Instant to = null;
        User u = new User();
        u.setId(1L);
        Currency c = new Currency();
        c.setId(1L);
        MBudget mb = new MBudget();
        mb.setFrom(from);
        mb.setTo(to);
        mb.setName("test");

        Mockito.when(budgetRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;

        try {
            service.create(mb, c, u);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_MISSING_PARAM);
    }

    @Test
    public void createWithFromAfterTo() {
        log.info("Test create budget with from after to");

        Instant from = Instant.from(Instant.now().truncatedTo(ChronoUnit.DAYS));
        Instant to = Instant.from(Instant.now().plus(10, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
        User u = new User();
        u.setId(1L);
        Currency c = new Currency();
        c.setId(1L);
        MBudget mb = new MBudget();
        mb.setFrom(to);
        mb.setTo(from);
        mb.setName("test");

        Mockito.when(budgetRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;

        try {
            service.create(mb, c, u);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_FROM_AFTER_TO);
    }

    @Test
    public void createWithoutCurrency() {
        log.info("Test create budget without currency");

        Instant from = Instant.from(Instant.now().truncatedTo(ChronoUnit.DAYS));
        Instant to = null;
        User u = new User();
        u.setId(1L);
        MBudget mb = new MBudget();
        mb.setFrom(from);
        mb.setTo(to);
        mb.setName("test");

        Mockito.when(budgetRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;

        try {
            service.create(mb, null, u);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_MISSING_PARAM);
    }

    @Test
    public void update() {
        log.info("Test update budget");

        Instant created = Instant.from(Instant.now().minus(1, ChronoUnit.DAYS));
        Instant from = Instant.from(Instant.now().truncatedTo(ChronoUnit.DAYS));
        Instant to = Instant.from(Instant.now().plus(10, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
        User u = new User();
        u.setId(1L);
        Currency c = new Currency();
        c.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setCreated(created);
        b.setOwner(u);
        b.setName("old");
        b.setFrom(Instant.now());
        b.setTo(Instant.now());
        MBudget mb = new MBudget();
        mb.setFrom(from);
        mb.setTo(to);
        mb.setName("new");

        Mockito.when(budgetRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Budget resp = service.update(b, mb, c);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(created, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getOwner())
                .isEqualTo(u);
        assertThat(resp.getName())
                .isEqualTo("new");
        assertThat(resp.getFrom())
                .isCloseTo(from, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getTo())
                .isCloseTo(to, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
    }

    @Test
    public void updateDeleted() {
        log.info("Test update deleted budget");

        Instant created = Instant.from(Instant.now().minus(1, ChronoUnit.DAYS));
        Instant from = Instant.from(Instant.now().truncatedTo(ChronoUnit.DAYS));
        Instant to = Instant.from(Instant.now().plus(10, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
        User u = new User();
        u.setId(1L);
        Currency c = new Currency();
        c.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setCreated(created);
        b.setDeleted(Instant.now());
        b.setOwner(u);
        b.setName("old");
        b.setFrom(Instant.now());
        b.setTo(Instant.now());
        MBudget mb = new MBudget();
        mb.setFrom(from);
        mb.setTo(to);
        mb.setName("new");

        Mockito.when(budgetRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;

        try {
            service.update(b, mb, c);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_EDIT_DELETED);
    }

    @Test
    public void updateWithDeletedCurrency() {
        log.info("Test update with deleted currency");

        Instant created = Instant.from(Instant.now().minus(1, ChronoUnit.DAYS));
        Instant from = Instant.from(Instant.now().truncatedTo(ChronoUnit.DAYS));
        Instant to = Instant.from(Instant.now().plus(10, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
        User u = new User();
        u.setId(1L);
        Currency c = new Currency();
        c.setId(1L);
        c.setDeleted(Instant.now());
        Budget b = new Budget();
        b.setId(1L);
        b.setCreated(created);
        b.setOwner(u);
        b.setName("old");
        b.setFrom(Instant.now());
        b.setTo(Instant.now());
        MBudget mb = new MBudget();
        mb.setFrom(from);
        mb.setTo(to);
        mb.setName("new");

        Mockito.when(budgetRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;

        try {
            service.update(b, mb, c);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_USING_DELETED);
    }

    @Test
    public void repeat() {
        log.info("Test repeat budget");

        Instant from = Instant.from(Instant.now().truncatedTo(ChronoUnit.DAYS));
        Instant to = Instant.from(Instant.now().plus(10, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
        User u = new User();
        u.setId(1L);
        Currency c = new Currency();
        c.setId(1L);
        Budget original = new Budget();
        original.setId(1L);
        MBudget mb = new MBudget();
        mb.setFrom(from);
        mb.setTo(to);
        mb.setName("test");

        Mockito.when(budgetRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Budget resp = service.repeat(mb, original, c, u);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getOwner())
                .isEqualTo(u);
        assertThat(resp.getCurrency())
                .isEqualTo(c);
        assertThat(resp.getName())
                .isEqualTo("test");
        assertThat(resp.getFrom())
                .isCloseTo(from, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getTo())
                .isCloseTo(to, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
    }

    @Test
    public void delete() {
        log.info("Test delete budget");

        Instant from = Instant.from(Instant.now().truncatedTo(ChronoUnit.DAYS));
        Instant to = Instant.from(Instant.now().plus(10, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
        User u = new User();
        u.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setCreated(Instant.now());
        b.setOwner(u);
        b.setName("name");
        b.setFrom(from);
        b.setTo(to);

        Mockito.when(budgetRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Budget resp = service.delete(b);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getOwner())
                .isEqualTo(u);
        assertThat(resp.getName())
                .isEqualTo("name");
        assertThat(resp.getFrom())
                .isCloseTo(from, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getTo())
                .isCloseTo(to, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
    }

    @Test
    public void deleteDeleted() {
        log.info("Test delete deleted budget");

        Instant from = Instant.from(Instant.now().truncatedTo(ChronoUnit.DAYS));
        Instant to = Instant.from(Instant.now().plus(10, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS));
        User u = new User();
        u.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setCreated(Instant.now());
        b.setDeleted(Instant.now());
        b.setOwner(u);
        b.setName("name");
        b.setFrom(from);
        b.setTo(to);

        Mockito.when(budgetRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;

        try {
            service.delete(b);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_DELETE_DELETED);
    }

    @Test
    public void searchAll() {
        log.info("Test search all budgets");

        Pageable pageRequest = PageRequest.of(0, 10);
        User u = new User();
        u.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setOwner(u);
        Page<Budget> page = new PageImpl<>(Collections.singletonList(b), pageRequest, 1);

        Mockito.when(budgetRepository.search(any(), any()))
                .thenReturn(page);

        Page<Budget> resp = service.search(null, null, pageRequest, u);

        assertThat(resp)
                .hasSize(1);
    }

    @Test
    public void searchByName() {
        log.info("Test search budgets by name");

        Pageable pageRequest = PageRequest.of(0, 10);
        User u = new User();
        u.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setOwner(u);
        Page<Budget> page = new PageImpl<>(Collections.singletonList(b), pageRequest, 1);

        Mockito.when(budgetRepository.search(any(String.class), any(User.class), any(Pageable.class)))
                .thenReturn(page);

        Page<Budget> resp = service.search("some", null, pageRequest, u);

        assertThat(resp)
                .hasSize(1);
    }

    @Test
    public void searchByInstant() {
        log.info("Test search budgets by date");

        Pageable pageRequest = PageRequest.of(0, 10);
        User u = new User();
        u.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setOwner(u);
        Page<Budget> page = new PageImpl<>(Collections.singletonList(b), pageRequest, 1);

        Mockito.when(budgetRepository.search(any(Instant.class), any(User.class), any(Pageable.class)))
                .thenReturn(page);

        Page<Budget> resp = service.search(null, Instant.now(), pageRequest, u);

        assertThat(resp)
                .hasSize(1);
    }

    @Test
    public void searchByNameAndInstant() {
        log.info("Test search budgets by name and date");

        Pageable pageRequest = PageRequest.of(0, 10);
        User u = new User();
        u.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setOwner(u);
        Page<Budget> page = new PageImpl<>(Collections.singletonList(b), pageRequest, 1);

        Mockito.when(budgetRepository.search(any(String.class), any(Instant.class), any(User.class), any(Pageable.class)))
                .thenReturn(page);

        Page<Budget> resp = service.search("some", Instant.now(), pageRequest, u);

        assertThat(resp)
                .hasSize(1);
    }

    @Test
    public void build() {
        log.info("Test build model entity");

        Instant created = Instant.now().minus(7, ChronoUnit.DAYS);
        Instant updated = created.plus(2, ChronoUnit.DAYS);
        Instant deleted = created.plus(5, ChronoUnit.DAYS);
        Instant from = Instant.now().truncatedTo(ChronoUnit.DAYS);
        Instant to = Instant.now().plus(10, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS);
        Currency c = new Currency();
        c.setId(1L);
        MCurrency mc = new MCurrency();
        mc.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setCreated(created);
        b.setUpdated(updated);
        b.setDeleted(deleted);
        b.setCurrency(c);
        b.setFrom(from);
        b.setTo(to);
        b.setName("test");

        Mockito.when(currencyService.build(any(Currency.class)))
                .thenReturn(mc);

        MBudget resp = service.build(b);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(1L);
        assertThat(resp.getCreated())
                .isCloseTo(created, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(updated, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isCloseTo(deleted, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getCurrency())
                .isEqualTo(mc);
        assertThat(resp.getName())
                .isEqualTo("test");
        assertThat(resp.getFrom())
                .isCloseTo(from, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getTo())
                .isCloseTo(to, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
    }

}
