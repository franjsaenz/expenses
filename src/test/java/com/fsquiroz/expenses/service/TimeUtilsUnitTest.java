package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.Role;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.util.DateRange;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.data.TemporalUnitWithinOffset;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class TimeUtilsUnitTest {

    @After
    public void cleanup() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void timeToString() {
        log.info("Test time to string");

        Instant when = Instant.parse("2020-01-20T14:00:59.999Z");

        String response = TimeUtils.standard(when);

        assertThat(response)
                .isEqualTo("2020-01-20T14:00:59.999Z");
    }

    @Test
    public void timeToStringWithZone() {
        log.info("Test time to string with zone");

        Instant when = Instant.parse("2020-01-20T14:00:59.999Z");

        String response = TimeUtils.standard(when, ZoneId.of("-0300"));

        assertThat(response)
                .isEqualTo("2020-01-20T11:00:59.999-0300");
    }

    @Test
    public void stringToTime() {
        log.info("Test string to time");

        Map<String, Instant> testCases = new LinkedHashMap<>();
        testCases.put("2020-01-20T11:00:59.999-0300", Instant.parse("2020-01-20T14:00:59.999Z"));
        testCases.put("2020-01-20T11:00:59.999-03:00", Instant.parse("2020-01-20T14:00:59.999Z"));
        testCases.put("2020-01-20T11:00:59.999z", Instant.parse("2020-01-20T11:00:59.999Z"));
        testCases.put("2020-01-20T11:00:59.999Z", Instant.parse("2020-01-20T11:00:59.999Z"));

        testCases.forEach((String toDeserialize, Instant expected) -> {
            Instant response = TimeUtils.standard(toDeserialize);
            assertThat(response)
                    .isCloseTo(
                            expected,
                            new TemporalUnitWithinOffset(1, ChronoUnit.MILLIS)
                    );
        });
    }

    @Test
    public void dateRange() {
        log.info("Test date range");

        Instant from = ZonedDateTime.parse("2020-01-01T00:00:00.000-03:00").toInstant();
        Instant to = ZonedDateTime.parse("2020-12-31T23:59:59.999-03:00").toInstant();

        DateRange response = TimeUtils.dateRange(from, to);

        assertThat(response)
                .isNotNull();
        assertThat(response.getFrom())
                .isCloseTo(
                        Instant.parse("2020-01-01T03:00:00.000Z"),
                        new TemporalUnitWithinOffset(1, ChronoUnit.MILLIS)
                );
        assertThat(response.getTo())
                .isCloseTo(
                        Instant.parse("2021-01-01T02:59:59.999Z"),
                        new TemporalUnitWithinOffset(1, ChronoUnit.MILLIS)
                );
    }

    @Test
    public void dateRangeWithFrom() {
        log.info("Test date range with from");

        Instant from = ZonedDateTime.parse("2020-01-01T00:00:00.000-03:00").toInstant();

        DateRange response = TimeUtils.dateRange(from, null);

        assertThat(response)
                .isNotNull();
        assertThat(response.getFrom())
                .isCloseTo(
                        Instant.parse("2020-01-01T03:00:00.000Z"),
                        new TemporalUnitWithinOffset(1, ChronoUnit.MILLIS)
                );
        assertThat(response.getTo())
                .isCloseTo(
                        Instant.parse("2020-02-01T02:59:59.999Z"),
                        new TemporalUnitWithinOffset(1, ChronoUnit.MILLIS)
                );
    }

    @Test
    public void dateRangeWithTo() {
        log.info("Test date range with to");

        Instant to = ZonedDateTime.parse("2020-12-31T23:59:59.999-03:00").toInstant();

        DateRange response = TimeUtils.dateRange(null, to);

        assertThat(response)
                .isNotNull();
        assertThat(response.getFrom())
                .isCloseTo(
                        Instant.parse("2020-12-01T03:00:00.000Z"),
                        new TemporalUnitWithinOffset(1, ChronoUnit.MILLIS)
                );
        assertThat(response.getTo())
                .isCloseTo(
                        Instant.parse("2021-01-01T02:59:59.999Z"),
                        new TemporalUnitWithinOffset(1, ChronoUnit.MILLIS)
                );
    }

    @Test
    public void dateRangeEmpty() {
        log.info("Test date range empty");

        LocalDate now = LocalDate.now();

        DateRange response = TimeUtils.dateRange(null, null);

        log.info("Test response: {}", response);
        assertThat(response)
                .isNotNull()
                .hasFieldOrProperty("from")
                .hasFieldOrProperty("to");
        assertThat(response.getFrom().atZone(ZoneOffset.UTC).getMonthValue())
                .isEqualTo(now.getMonthValue());
        assertThat(response.getTo().atZone(ZoneOffset.UTC).getMonthValue())
                .isEqualTo(now.getMonthValue());
    }

    @Test
    public void getZoneIdFromNone() {
        log.info("Test get zoneId from none");

        ZoneId response = TimeUtils.getZoneId();

        assertThat(response)
                .isEqualTo(ZoneOffset.UTC);
    }

    @Test
    public void getZoneIdFromNonSetup() {
        log.info("Test get zoneIde from non setup");

        User u = new User();
        u.setId(1L);
        u.setRole(Role.USER);
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(u, null, u.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);

        ZoneId response = TimeUtils.getZoneId();

        assertThat(response)
                .isEqualTo(ZoneOffset.UTC);
    }

    @Test
    public void getZoneIdFromSetup() {
        log.info("Test get zoneIde from setup");

        User u = new User();
        u.setId(1L);
        u.setRole(Role.USER);
        u.setZoneId("UTC-03:00");
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(u, null, u.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);

        ZoneId response = TimeUtils.getZoneId();

        assertThat(response)
                .isEqualTo(ZoneId.of("UTC-03:00"));
    }

}
