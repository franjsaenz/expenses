package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.Budget;
import com.fsquiroz.expenses.entity.db.BudgetItem;
import com.fsquiroz.expenses.entity.db.Category;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MBudgetItem;
import com.fsquiroz.expenses.entity.json.MCategory;
import com.fsquiroz.expenses.exception.AppException;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.ErrorCode;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.repository.BudgetItemRepository;
import com.fsquiroz.expenses.service.declaration.ICategoryService;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.data.Offset;
import org.assertj.core.data.TemporalUnitWithinOffset;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.stubbing.answers.ReturnsArgumentAt;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class BudgetItemServiceUnitTest {

    @MockBean
    private ICategoryService categoryService;

    @MockBean
    private BudgetItemRepository budgetItemRepository;

    private BudgetItemService service;

    @Before
    public void setup() {
        service = new BudgetItemService(categoryService, budgetItemRepository);
    }

    @Test
    public void getById() {
        log.info("Test get budget item by id");

        BudgetItem bi = new BudgetItem();
        bi.setId(1L);
        bi.setCreated(Instant.now());

        Mockito.when(budgetItemRepository.findById(any()))
                .thenReturn(Optional.of(bi));

        BudgetItem resp = service.get(1L);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(bi.getId());
        assertThat(resp.getCreated())
                .isCloseTo(bi.getCreated(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
    }

    @Test
    public void getNotFound() {
        log.info("Test get not found budget item");

        Mockito.when(budgetItemRepository.findById(any()))
                .thenReturn(Optional.empty());

        Exception resp = null;
        try {
            service.get(1L);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(NotFoundException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.NF_BY_ID);
    }

    @Test
    public void getIdNull() {
        log.info("Test get with null id");

        Exception resp = null;
        try {
            service.get(null);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_INVALID_ID_VALUE);
    }

    @Test
    public void create() {
        log.info("Test create budget item");

        User u = new User();
        u.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setOwner(u);
        Category c = new Category();
        c.setId(1L);
        c.setOwner(u);

        MBudgetItem mbi = new MBudgetItem();
        mbi.setAmount(new BigDecimal("10.98999"));

        Mockito.when(budgetItemRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        BudgetItem resp = service.setIem(mbi, b, c);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getOwner())
                .isEqualTo(u);
        assertThat(resp.getCategory())
                .isEqualTo(c);
        assertThat(resp.getAmount())
                .isCloseTo(new BigDecimal("10.99"), Offset.offset(BigDecimal.valueOf(0.005)));
    }

    @Test
    public void createWithNegativeAmount() {
        log.info("Test create budget item with negative amount");

        User u = new User();
        u.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setOwner(u);
        Category c = new Category();
        c.setId(1L);
        c.setOwner(u);

        MBudgetItem mbi = new MBudgetItem();
        mbi.setAmount(new BigDecimal("-10.98999"));

        Mockito.when(budgetItemRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        BudgetItem resp = service.setIem(mbi, b, c);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getOwner())
                .isEqualTo(u);
        assertThat(resp.getCategory())
                .isEqualTo(c);
        assertThat(resp.getAmount())
                .isCloseTo(new BigDecimal("10.99"), Offset.offset(BigDecimal.valueOf(0.005)));
    }

    @Test
    public void createWithDeletedBudget() {
        log.info("Test create budget item with deleted parent budget");

        User u = new User();
        u.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setDeleted(Instant.now());
        b.setOwner(u);
        Category c = new Category();
        c.setId(1L);
        c.setOwner(u);

        MBudgetItem mbi = new MBudgetItem();
        mbi.setAmount(new BigDecimal("-10.98999"));

        Mockito.when(budgetItemRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;
        try {
            service.setIem(mbi, b, c);
        } catch (Exception ex) {
            resp = ex;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_USING_DELETED);
    }

    @Test
    public void createWithoutCategory() {
        log.info("Test create budget item without categroy");

        User u = new User();
        u.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setOwner(u);
        Category c = null;

        MBudgetItem mbi = new MBudgetItem();
        mbi.setAmount(new BigDecimal("-10.98999"));

        Mockito.when(budgetItemRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;
        try {
            service.setIem(mbi, b, c);
        } catch (Exception ex) {
            resp = ex;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_MISSING_PARAM);
    }

    @Test
    public void createWithDeletedCategory() {
        log.info("Test create budget item with deleted category");

        User u = new User();
        u.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setOwner(u);
        Category c = new Category();
        c.setId(1L);
        c.setDeleted(Instant.now());
        c.setOwner(u);

        MBudgetItem mbi = new MBudgetItem();
        mbi.setAmount(new BigDecimal("-10.98999"));

        Mockito.when(budgetItemRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;
        try {
            service.setIem(mbi, b, c);
        } catch (Exception ex) {
            resp = ex;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_USING_DELETED);
    }

    @Test
    public void createWithoutAmount() {
        log.info("Test create budget item without amount");

        User u = new User();
        u.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setOwner(u);
        Category c = new Category();
        c.setId(1L);
        c.setOwner(u);

        MBudgetItem mbi = new MBudgetItem();

        Mockito.when(budgetItemRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;
        try {
            service.setIem(mbi, b, c);
        } catch (Exception ex) {
            resp = ex;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_MISSING_PARAM);
    }

    @Test
    public void update() {
        log.info("Test update budget");

        Instant created = Instant.from(Instant.now().minus(1, ChronoUnit.DAYS));
        User u = new User();
        u.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        b.setOwner(u);
        Category c = new Category();
        c.setId(1L);
        c.setOwner(u);
        BudgetItem bi = new BudgetItem();
        bi.setId(1L);
        bi.setCreated(created);
        bi.setOwner(u);
        bi.setBudget(b);
        bi.setCategory(c);

        MBudgetItem mbi = new MBudgetItem();
        mbi.setAmount(new BigDecimal("-10.98999"));

        Mockito.when(budgetItemRepository.findByDeletedIsNullAndBudgetAndCategory(any(), any()))
                .thenReturn(Optional.of(bi));
        Mockito.when(budgetItemRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        BudgetItem resp = service.setIem(mbi, b, c);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(created, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getOwner())
                .isEqualTo(u);
        assertThat(resp.getCategory())
                .isEqualTo(c);
        assertThat(resp.getAmount())
                .isCloseTo(new BigDecimal("10.99"), Offset.offset(BigDecimal.valueOf(0.005)));
    }

    @Test
    public void repeat() {
        log.info("Test repeat budget's items");

        User u = new User();
        u.setId(1L);
        Budget original = new Budget();
        original.setId(1L);
        original.setOwner(u);
        Budget repeated = new Budget();
        repeated.setId(2L);
        repeated.setOwner(u);
        Category c1 = new Category();
        c1.setId(1L);
        c1.setOwner(u);
        Category c2 = new Category();
        c2.setId(1L);
        c2.setDeleted(Instant.now());
        c2.setOwner(u);
        BudgetItem bi1 = new BudgetItem();
        bi1.setId(1L);
        bi1.setOwner(u);
        bi1.setBudget(original);
        bi1.setCategory(c1);
        bi1.setAmount(new BigDecimal("10.001"));
        BudgetItem bi2 = new BudgetItem();
        bi2.setId(2L);
        bi2.setOwner(u);
        bi2.setBudget(original);
        bi2.setCategory(c2);
        bi2.setAmount(new BigDecimal("10.001"));

        List<BudgetItem> items = Arrays.asList(bi1, bi2);

        Mockito.when(budgetItemRepository.findByDeletedIsNullAndBudget(any()))
                .thenReturn(items);

        List<BudgetItem> resp = service.repeat(original, repeated);

        assertThat(resp)
                .hasSize(1);
        assertThat(resp.get(0).getCreated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.get(0).getUpdated())
                .isNull();
        assertThat(resp.get(0).getDeleted())
                .isNull();
        assertThat(resp.get(0).getOwner())
                .isEqualTo(u);
        assertThat(resp.get(0).getBudget())
                .isEqualTo(repeated);
        assertThat(resp.get(0).getCategory())
                .isEqualTo(c1);
        assertThat(resp.get(0).getAmount())
                .isCloseTo(new BigDecimal("10.00"), Offset.offset(BigDecimal.valueOf(0.005)));
    }

    @Test
    public void delete() {
        log.info("Test delete budget item");

        Instant created = Instant.from(Instant.now().minus(1, ChronoUnit.DAYS));
        Budget b = new Budget();
        b.setId(1L);
        BudgetItem bi = new BudgetItem();
        bi.setId(1L);
        bi.setCreated(created);
        bi.setBudget(b);

        Mockito.when(budgetItemRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        BudgetItem resp = service.delete(bi, b);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(created, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
    }

    @Test
    public void deleteDeleted() {
        log.info("Test delete deleted budget item");

        Instant created = Instant.from(Instant.now().minus(1, ChronoUnit.DAYS));
        Budget b = new Budget();
        b.setId(1L);
        BudgetItem bi = new BudgetItem();
        bi.setId(1L);
        bi.setCreated(created);
        bi.setDeleted(Instant.now());
        bi.setBudget(b);

        Mockito.when(budgetItemRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;

        try {
            service.delete(bi, b);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_DELETE_DELETED);
    }

    @Test
    public void deleteMismatchBudget() {
        log.info("Test delete budget item with mismatch budget");

        Instant created = Instant.from(Instant.now().minus(1, ChronoUnit.DAYS));
        Budget b = new Budget();
        b.setId(1L);
        Budget b2 = new Budget();
        b2.setId(2L);
        BudgetItem bi = new BudgetItem();
        bi.setId(1L);
        bi.setCreated(created);
        bi.setBudget(b);

        Mockito.when(budgetItemRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;

        try {
            service.delete(bi, b2);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_MISMATCH_BUDGET);
    }

    @Test
    public void list() {
        log.info("Test list all budget's items");

        Pageable pageRequest = PageRequest.of(0, 10);
        User u = new User();
        u.setId(1L);
        Budget b = new Budget();
        b.setId(1L);
        BudgetItem bi = new BudgetItem();
        bi.setId(1L);
        bi.setOwner(u);
        Page<BudgetItem> page = new PageImpl<>(Collections.singletonList(bi), pageRequest, 1);

        Mockito.when(budgetItemRepository.list(any(), any()))
                .thenReturn(page);

        Page<BudgetItem> resp = service.search(b, pageRequest);

        assertThat(resp)
                .hasSize(1);
    }

    @Test
    public void build() {
        log.info("Test build model entity");

        Instant created = Instant.now().minus(7, ChronoUnit.DAYS);
        Instant updated = created.plus(2, ChronoUnit.DAYS);
        Instant deleted = created.plus(5, ChronoUnit.DAYS);
        Category c = new Category();
        c.setId(1L);
        MCategory mc = new MCategory();
        mc.setId(1L);
        BudgetItem bi = new BudgetItem();
        bi.setId(1L);
        bi.setCreated(created);
        bi.setUpdated(updated);
        bi.setDeleted(deleted);
        bi.setCategory(c);
        bi.setAmount(new BigDecimal("10.00"));

        Mockito.when(categoryService.build(any(Category.class)))
                .thenReturn(mc);

        MBudgetItem resp = service.build(bi);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(1L);
        assertThat(resp.getCreated())
                .isCloseTo(created, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(updated, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isCloseTo(deleted, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getCategory())
                .isEqualTo(mc);
        assertThat(resp.getAmount())
                .isCloseTo(new BigDecimal("10.00"), Offset.offset(BigDecimal.valueOf(0.005)));
    }

}
