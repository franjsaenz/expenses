package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.*;
import com.fsquiroz.expenses.entity.json.MDetail;
import com.fsquiroz.expenses.entity.json.MGroupDetail;
import com.fsquiroz.expenses.entity.json.MProduct;
import com.fsquiroz.expenses.exception.AppException;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.ErrorCode;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.repository.DetailRepository;
import com.fsquiroz.expenses.repository.StatRepository;
import com.fsquiroz.expenses.service.declaration.ICategoryService;
import com.fsquiroz.expenses.service.declaration.IProductService;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.data.Offset;
import org.assertj.core.data.TemporalUnitWithinOffset;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.stubbing.answers.ReturnsArgumentAt;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class DetailServiceUnitTest {

    @MockBean
    private ICategoryService categoryService;

    @MockBean
    private IProductService productService;

    @MockBean
    private DetailRepository detailRepository;

    @MockBean
    private StatRepository statRepository;

    private DetailService service;

    @Before
    public void setup() {
        this.service = new DetailService(20, categoryService, productService, detailRepository, statRepository);
    }

    @Test
    public void getById() {
        log.info("Test get expense by id");

        Instant created = Instant.now();
        Detail d = new Detail();
        d.setId(1L);
        d.setCreated(created);

        Mockito.when(detailRepository.findById(any()))
                .thenReturn(Optional.of(d));

        Detail resp = service.get(1L);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(1L);
        assertThat(resp.getCreated())
                .isCloseTo(created, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
    }

    @Test
    public void getNotFound() {
        log.info("Test get not found expense");

        Mockito.when(detailRepository.findById(any()))
                .thenReturn(Optional.empty());

        Exception resp = null;
        try {
            service.get(1L);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(NotFoundException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.NF_BY_ID);
    }

    @Test
    public void getIdNull() {
        log.info("Test get with null id");

        Exception resp = null;
        try {
            service.get(null);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_INVALID_ID_VALUE);
    }

    @Test
    public void getTotal() {
        log.info("Test get details totals by expense");

        Expense e = new Expense();
        BigDecimal total = BigDecimal.valueOf(9.99);

        Mockito.when(detailRepository.getTotal(any()))
                .thenReturn(total);

        BigDecimal resp = service.getTotal(e);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isCloseTo(BigDecimal.valueOf(9.99), Offset.offset(BigDecimal.valueOf(0.01)));
    }

    @Test
    public void getTotalWithoutDetails() {
        log.info("Test get details totals by expense without details");

        Expense e = new Expense();
        BigDecimal total = null;

        Mockito.when(detailRepository.getTotal(any()))
                .thenReturn(total);

        BigDecimal resp = service.getTotal(e);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isCloseTo(BigDecimal.valueOf(0.00), Offset.offset(BigDecimal.valueOf(0.01)));
    }

    @Test
    public void applyDiscount() {
        log.info("Test apply discount to expense");

        Expense e = new Expense();
        BigDecimal discount = BigDecimal.valueOf(10.00);
        Detail a = new Detail();
        a.setAmount(BigDecimal.valueOf(25.00));
        Detail b = new Detail();
        b.setAmount(BigDecimal.valueOf(50.00));
        Detail c = new Detail();
        c.setAmount(BigDecimal.valueOf(25.00));
        Detail d = new Detail();
        d.setAmount(BigDecimal.valueOf(100.00));
        d.setDiscount(BigDecimal.valueOf(0.00));
        d.setSubtotal(BigDecimal.valueOf(100.00));
        d.setWithoutDiscount(true);
        List<Detail> details = Arrays.asList(a, b, c, d);

        Mockito.when(detailRepository.findByDeletedIsNullAndExpense(any()))
                .thenReturn(details);
        Mockito.when(detailRepository.saveAll(any()))
                .thenReturn(details);

        List<Detail> resp = service.applyDiscounts(e, discount);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(4);
        assertThat(resp.get(0).getAmount())
                .isCloseTo(BigDecimal.valueOf(25.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(0).getDiscount())
                .isCloseTo(BigDecimal.valueOf(2.50), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(0).getSubtotal())
                .isCloseTo(BigDecimal.valueOf(22.50), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(1).getAmount())
                .isCloseTo(BigDecimal.valueOf(50.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(1).getDiscount())
                .isCloseTo(BigDecimal.valueOf(5.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(1).getSubtotal())
                .isCloseTo(BigDecimal.valueOf(45.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(2).getAmount())
                .isCloseTo(BigDecimal.valueOf(25.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(2).getDiscount())
                .isCloseTo(BigDecimal.valueOf(2.50), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(2).getSubtotal())
                .isCloseTo(BigDecimal.valueOf(22.50), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(3).getAmount())
                .isCloseTo(BigDecimal.valueOf(100.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(3).getDiscount())
                .isCloseTo(BigDecimal.valueOf(0.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(3).getSubtotal())
                .isCloseTo(BigDecimal.valueOf(100.00), Offset.offset(BigDecimal.valueOf(0.005)));
    }

    @Test
    public void applyUnevenDiscount() {
        log.info("Test apply uneven discount");

        Expense e = new Expense();
        BigDecimal discount = BigDecimal.valueOf(54.85);
        BigDecimal total = BigDecimal.valueOf(1815.30);
        List<Detail> details = new ArrayList<>();
        Detail d32 = new Detail();
        d32.setAmount(BigDecimal.valueOf(95.00));
        details.add(d32);
        Detail d33 = new Detail();
        d33.setAmount(BigDecimal.valueOf(89.00));
        details.add(d33);
        Detail d34 = new Detail();
        d34.setAmount(BigDecimal.valueOf(241.61));
        details.add(d34);
        Detail d35 = new Detail();
        d35.setAmount(BigDecimal.valueOf(92.51));
        details.add(d35);
        Detail d36 = new Detail();
        d36.setAmount(BigDecimal.valueOf(4.00));
        details.add(d36);
        Detail d37 = new Detail();
        d37.setAmount(BigDecimal.valueOf(4.00));
        details.add(d37);
        Detail d38 = new Detail();
        d38.setAmount(BigDecimal.valueOf(4.00));
        details.add(d38);
        Detail d39 = new Detail();
        d39.setAmount(BigDecimal.valueOf(4.00));
        details.add(d39);
        Detail d40 = new Detail();
        d40.setAmount(BigDecimal.valueOf(69.9));
        details.add(d40);
        Detail d41 = new Detail();
        d41.setAmount(BigDecimal.valueOf(69.9));
        details.add(d41);
        Detail d42 = new Detail();
        d42.setAmount(BigDecimal.valueOf(65.00));
        details.add(d42);
        Detail d43 = new Detail();
        d43.setAmount(BigDecimal.valueOf(70.00));
        details.add(d43);
        Detail d44 = new Detail();
        d44.setAmount(BigDecimal.valueOf(27.00));
        details.add(d44);
        Detail d45 = new Detail();
        d45.setAmount(BigDecimal.valueOf(27.00));
        details.add(d45);
        Detail d46 = new Detail();
        d46.setAmount(BigDecimal.valueOf(159.00));
        details.add(d46);
        Detail d47 = new Detail();
        d47.setAmount(BigDecimal.valueOf(89.00));
        details.add(d47);
        Detail d48 = new Detail();
        d48.setAmount(BigDecimal.valueOf(56.93));
        details.add(d48);
        Detail d49 = new Detail();
        d49.setAmount(BigDecimal.valueOf(123.00));
        details.add(d49);
        Detail d50 = new Detail();
        d50.setAmount(BigDecimal.valueOf(79.00));
        details.add(d50);
        Detail d51 = new Detail();
        d51.setAmount(BigDecimal.valueOf(46.45));
        details.add(d51);
        Detail d52 = new Detail();
        d52.setAmount(BigDecimal.valueOf(399.00));
        details.add(d52);

        Mockito.when(detailRepository.getTotal(any()))
                .thenReturn(total);
        Mockito.when(detailRepository.findByDeletedIsNullAndExpense(any()))
                .thenReturn(details);
        Mockito.when(detailRepository.saveAll(any()))
                .thenReturn(details);

        List<Detail> resp = service.applyDiscounts(e, discount);

        assertThat(resp).isNotNull();
        assertThat(resp).hasSize(21);
        assertThat(resp.get(0).getAmount()).isCloseTo(BigDecimal.valueOf(95.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(0).getDiscount()).isCloseTo(BigDecimal.valueOf(2.86), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(0).getSubtotal()).isCloseTo(BigDecimal.valueOf(92.14), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(1).getAmount()).isCloseTo(BigDecimal.valueOf(89.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(1).getDiscount()).isCloseTo(BigDecimal.valueOf(2.69), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(1).getSubtotal()).isCloseTo(BigDecimal.valueOf(86.31), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(2).getAmount()).isCloseTo(BigDecimal.valueOf(241.61), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(2).getDiscount()).isCloseTo(BigDecimal.valueOf(7.30), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(2).getSubtotal()).isCloseTo(BigDecimal.valueOf(234.31), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(3).getAmount()).isCloseTo(BigDecimal.valueOf(92.51), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(3).getDiscount()).isCloseTo(BigDecimal.valueOf(2.80), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(3).getSubtotal()).isCloseTo(BigDecimal.valueOf(89.71), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(4).getAmount()).isCloseTo(BigDecimal.valueOf(4.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(4).getDiscount()).isCloseTo(BigDecimal.valueOf(0.12), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(4).getSubtotal()).isCloseTo(BigDecimal.valueOf(3.88), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(5).getAmount()).isCloseTo(BigDecimal.valueOf(4.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(5).getDiscount()).isCloseTo(BigDecimal.valueOf(0.12), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(5).getSubtotal()).isCloseTo(BigDecimal.valueOf(3.88), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(6).getAmount()).isCloseTo(BigDecimal.valueOf(4.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(6).getDiscount()).isCloseTo(BigDecimal.valueOf(0.12), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(6).getSubtotal()).isCloseTo(BigDecimal.valueOf(3.88), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(7).getAmount()).isCloseTo(BigDecimal.valueOf(4.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(7).getDiscount()).isCloseTo(BigDecimal.valueOf(0.12), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(7).getSubtotal()).isCloseTo(BigDecimal.valueOf(3.88), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(8).getAmount()).isCloseTo(BigDecimal.valueOf(69.9), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(8).getDiscount()).isCloseTo(BigDecimal.valueOf(2.11), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(8).getSubtotal()).isCloseTo(BigDecimal.valueOf(67.79), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(9).getAmount()).isCloseTo(BigDecimal.valueOf(69.9), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(9).getDiscount()).isCloseTo(BigDecimal.valueOf(2.11), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(9).getSubtotal()).isCloseTo(BigDecimal.valueOf(67.79), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(10).getAmount()).isCloseTo(BigDecimal.valueOf(65.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(10).getDiscount()).isCloseTo(BigDecimal.valueOf(1.96), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(10).getSubtotal()).isCloseTo(BigDecimal.valueOf(63.04), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(11).getAmount()).isCloseTo(BigDecimal.valueOf(70.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(11).getDiscount()).isCloseTo(BigDecimal.valueOf(2.12), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(11).getSubtotal()).isCloseTo(BigDecimal.valueOf(67.88), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(12).getAmount()).isCloseTo(BigDecimal.valueOf(27.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(12).getDiscount()).isCloseTo(BigDecimal.valueOf(0.82), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(12).getSubtotal()).isCloseTo(BigDecimal.valueOf(26.18), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(13).getAmount()).isCloseTo(BigDecimal.valueOf(27.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(13).getDiscount()).isCloseTo(BigDecimal.valueOf(0.82), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(13).getSubtotal()).isCloseTo(BigDecimal.valueOf(26.18), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(14).getAmount()).isCloseTo(BigDecimal.valueOf(159.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(14).getDiscount()).isCloseTo(BigDecimal.valueOf(4.80), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(14).getSubtotal()).isCloseTo(BigDecimal.valueOf(154.20), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(15).getAmount()).isCloseTo(BigDecimal.valueOf(89.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(15).getDiscount()).isCloseTo(BigDecimal.valueOf(2.69), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(15).getSubtotal()).isCloseTo(BigDecimal.valueOf(86.31), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(16).getAmount()).isCloseTo(BigDecimal.valueOf(56.93), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(16).getDiscount()).isCloseTo(BigDecimal.valueOf(1.72), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(16).getSubtotal()).isCloseTo(BigDecimal.valueOf(55.21), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(17).getAmount()).isCloseTo(BigDecimal.valueOf(123.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(17).getDiscount()).isCloseTo(BigDecimal.valueOf(3.72), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(17).getSubtotal()).isCloseTo(BigDecimal.valueOf(119.28), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(18).getAmount()).isCloseTo(BigDecimal.valueOf(79.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(18).getDiscount()).isCloseTo(BigDecimal.valueOf(2.39), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(18).getSubtotal()).isCloseTo(BigDecimal.valueOf(76.61), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(19).getAmount()).isCloseTo(BigDecimal.valueOf(46.45), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(19).getDiscount()).isCloseTo(BigDecimal.valueOf(1.40), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(19).getSubtotal()).isCloseTo(BigDecimal.valueOf(45.05), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(20).getAmount()).isCloseTo(BigDecimal.valueOf(399.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(20).getDiscount()).isCloseTo(BigDecimal.valueOf(12.06), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(20).getSubtotal()).isCloseTo(BigDecimal.valueOf(386.94), Offset.offset(BigDecimal.valueOf(0.005)));
    }

    @Test
    public void applyDiscountWithoutDetails() {
        log.info("Test apply discount to expense without details");

        Expense e = new Expense();
        BigDecimal discount = BigDecimal.valueOf(10.00);
        List<Detail> details = new ArrayList<>();

        Mockito.when(detailRepository.getTotal(any()))
                .thenReturn(BigDecimal.valueOf(0.00));
        Mockito.when(detailRepository.findByDeletedIsNullAndExpense(any()))
                .thenReturn(details);

        List<Detail> resp = service.applyDiscounts(e, discount);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(0);
    }

    @Test
    public void create() {
        log.info("Test create detail");

        User u = new User();
        u.setId(1L);
        u.setCreated(Instant.now());
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(Instant.now());
        e.setStatus(ExpenseStatus.OPENED);
        Product p = new Product();
        p.setId(1L);
        p.setCreated(Instant.now());
        MDetail md = new MDetail();
        md.setAmount(BigDecimal.valueOf(100.00));
        md.setDescription("Some");

        Mockito.when(detailRepository.saveAll(any()))
                .thenAnswer((invocation -> invocation.getArgument(0, List.class)));

        List<Detail> resp = service.create(md, e, null, p, 1, u);

        assertThat(resp)
                .isNotNull()
                .hasSize(1);
        assertThat(resp.get(0).getCreated())
                .isNotNull();
        assertThat(resp.get(0).getAmount())
                .isCloseTo(BigDecimal.valueOf(100.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(0).getOwner())
                .isNotNull();
        assertThat(resp.get(0).getOwner().getId())
                .isEqualTo(1L);
        assertThat(resp.get(0).getExpense())
                .isNotNull();
        assertThat(resp.get(0).getExpense().getId())
                .isEqualTo(1L);
        assertThat(resp.get(0).getProduct())
                .isNotNull();
        assertThat(resp.get(0).getProduct().getId())
                .isEqualTo(1L);
        assertThat(resp.get(0).getDescription())
                .isEqualTo("Some");
    }

    @Test
    public void createWithNegativeAmount() {
        log.info("Test create detail with negative amount");

        User u = new User();
        u.setId(1L);
        u.setCreated(Instant.now());
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(Instant.now());
        e.setStatus(ExpenseStatus.OPENED);
        Product p = new Product();
        p.setId(1L);
        p.setCreated(Instant.now());
        MDetail md = new MDetail();
        md.setAmount(BigDecimal.valueOf(-100.00));

        Mockito.when(detailRepository.saveAll(any()))
                .thenAnswer((invocation -> invocation.getArgument(0, List.class)));

        List<Detail> resp = service.create(md, e, null, p, 2, u);

        assertThat(resp)
                .isNotNull()
                .hasSize(2);
        assertThat(resp.get(0).getCreated())
                .isNotNull();
        assertThat(resp.get(0).getAmount())
                .isCloseTo(BigDecimal.valueOf(100.0), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(0).getOwner())
                .isNotNull();
        assertThat(resp.get(0).getOwner().getId())
                .isEqualTo(1L);
        assertThat(resp.get(0).getExpense())
                .isNotNull();
        assertThat(resp.get(0).getExpense().getId())
                .isEqualTo(1L);
        assertThat(resp.get(0).getProduct())
                .isNotNull();
        assertThat(resp.get(0).getProduct().getId())
                .isEqualTo(1L);

        assertThat(resp.get(1).getCreated())
                .isNotNull();
        assertThat(resp.get(1).getAmount())
                .isCloseTo(BigDecimal.valueOf(100.0), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(1).getOwner())
                .isNotNull();
        assertThat(resp.get(1).getOwner().getId())
                .isEqualTo(1L);
        assertThat(resp.get(1).getExpense())
                .isNotNull();
        assertThat(resp.get(1).getExpense().getId())
                .isEqualTo(1L);
        assertThat(resp.get(1).getProduct())
                .isNotNull();
        assertThat(resp.get(1).getProduct().getId())
                .isEqualTo(1L);
    }

    @Test
    public void createWithoutAmount() {
        log.info("Test create detail without amount");

        User u = new User();
        u.setId(1L);
        u.setCreated(Instant.now());
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(Instant.now());
        e.setStatus(ExpenseStatus.OPENED);
        Product p = new Product();
        p.setId(1L);
        p.setCreated(Instant.now());
        MDetail md = new MDetail();

        Exception resp = null;
        try {
            service.create(md, e, null, p, 1, u);
        } catch (Exception ex) {
            resp = ex;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_MISSING_PARAM);
    }

    @Test
    public void createWithDeletedExpense() {
        log.info("Test create detail with deleted expense");

        User u = new User();
        u.setId(1L);
        u.setCreated(Instant.now());
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(Instant.now());
        e.setDeleted(Instant.now());
        e.setStatus(ExpenseStatus.OPENED);
        Product p = new Product();
        p.setId(1L);
        p.setCreated(Instant.now());
        MDetail md = new MDetail();
        md.setAmount(BigDecimal.valueOf(100.00));

        Exception resp = null;
        try {
            service.create(md, e, null, p, 1, u);
        } catch (Exception ex) {
            resp = ex;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_USING_DELETED);
    }

    @Test
    public void createWithDeletedProduct() {
        log.info("Test create detail with deleted product");

        User u = new User();
        u.setId(1L);
        u.setCreated(Instant.now());
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(Instant.now());
        e.setStatus(ExpenseStatus.OPENED);
        Product p = new Product();
        p.setId(1L);
        p.setCreated(Instant.now());
        p.setDeleted(Instant.now());
        MDetail md = new MDetail();
        md.setAmount(BigDecimal.valueOf(100.00));

        Exception resp = null;
        try {
            service.create(md, e, null, p, 1, u);
        } catch (Exception ex) {
            resp = ex;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_USING_DELETED);
    }

    @Test
    public void createWithClosedExpense() {
        log.info("Test create detail with closed expense");

        User u = new User();
        u.setId(1L);
        u.setCreated(Instant.now());
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(Instant.now());
        e.setStatus(ExpenseStatus.CLOSED);
        Product p = new Product();
        p.setId(1L);
        p.setCreated(Instant.now());
        MDetail md = new MDetail();
        md.setAmount(BigDecimal.valueOf(100.00));

        Exception resp = null;
        try {
            service.create(md, e, null, p, 1, u);
        } catch (Exception ex) {
            resp = ex;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_NOT_OPENED);
    }

    @Test
    public void repeat() {
        log.info("Test repeat expense's details");

        Expense original = new Expense();
        original.setId(1L);
        Expense repeated = new Expense();
        repeated.setId(2L);
        Product p1 = new Product();
        p1.setId(1L);
        p1.setDeleted(Instant.now());
        Product p2 = new Product();
        p2.setId(2L);
        Detail d1 = new Detail();
        d1.setId(1L);
        d1.setExpense(original);
        d1.setProduct(p1);
        d1.setAmount(BigDecimal.valueOf(100.00).setScale(2, RoundingMode.HALF_DOWN));
        d1.setDiscount(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_DOWN));
        d1.setSubtotal(d1.getAmount());
        Detail d2 = new Detail();
        d2.setId(2L);
        d2.setExpense(original);
        d2.setProduct(p2);
        d2.setAmount(BigDecimal.valueOf(200.00).setScale(2, RoundingMode.HALF_DOWN));
        d2.setDiscount(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_DOWN));
        d2.setSubtotal(d2.getAmount());
        List<Detail> details = Arrays.asList(d1, d2);

        Mockito.when(detailRepository.findByDeletedIsNullAndExpense(any()))
                .thenReturn(details);

        List<Detail> resp = service.repeat(original, repeated, new User());

        assertThat(resp)
                .hasSize(1);
        assertThat(resp.get(0).getExpense())
                .isEqualTo(repeated);
        assertThat(resp.get(0).getProduct())
                .isEqualTo(p2);
        assertThat(resp.get(0).getAmount())
                .isCloseTo(BigDecimal.valueOf(200.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(0).getSubtotal())
                .isCloseTo(BigDecimal.valueOf(200.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.get(0).getDiscount())
                .isCloseTo(BigDecimal.ZERO, Offset.offset(BigDecimal.valueOf(0.005)));
    }

    @Test
    public void update() {
        log.info("Test update detail");

        User u = new User();
        u.setId(1L);
        u.setCreated(Instant.now());
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(Instant.now());
        e.setStatus(ExpenseStatus.OPENED);
        Product p = new Product();
        p.setId(1L);
        p.setCreated(Instant.now());
        Detail d = new Detail();
        d.setId(1L);
        d.setOwner(u);
        d.setCreated(Instant.now());
        d.setExpense(e);
        d.setAmount(BigDecimal.valueOf(50.00));
        d.setDescription("Some");
        MDetail md = new MDetail();
        md.setAmount(BigDecimal.valueOf(100.00));

        Mockito.when(detailRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Detail resp = service.update(d, e, null, p, md);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isNotNull();
        assertThat(resp.getUpdated())
                .isNotNull();
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getAmount())
                .isCloseTo(BigDecimal.valueOf(100.00), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.getOwner())
                .isNotNull();
        assertThat(resp.getOwner().getId())
                .isEqualTo(1L);
        assertThat(resp.getExpense())
                .isNotNull();
        assertThat(resp.getExpense().getId())
                .isEqualTo(1L);
        assertThat(resp.getProduct())
                .isNotNull();
        assertThat(resp.getProduct().getId())
                .isEqualTo(1L);
        assertThat(resp.getDescription())
                .isNull();
    }

    @Test
    public void updateDeleted() {
        log.info("Test update deleted detail");

        User u = new User();
        u.setId(1L);
        u.setCreated(Instant.now());
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(Instant.now());
        e.setStatus(ExpenseStatus.OPENED);
        Product p = new Product();
        p.setId(1L);
        p.setCreated(Instant.now());
        Detail d = new Detail();
        d.setId(1L);
        d.setOwner(u);
        d.setCreated(Instant.now());
        d.setDeleted(Instant.now());
        d.setExpense(e);
        d.setAmount(BigDecimal.valueOf(50.00));
        MDetail md = new MDetail();
        md.setAmount(BigDecimal.valueOf(100.00));

        Mockito.when(detailRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;
        try {
            service.update(d, e, null, p, md);
        } catch (Exception ex) {
            resp = ex;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_EDIT_DELETED);
    }

    @Test
    public void updateMismatchExpense() {
        log.info("Test update detail with mismatch expense");

        User u = new User();
        u.setId(1L);
        u.setCreated(Instant.now());
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(Instant.now());
        e.setStatus(ExpenseStatus.OPENED);
        Expense e2 = new Expense();
        e2.setId(2L);
        e2.setCreated(Instant.now());
        e2.setStatus(ExpenseStatus.OPENED);
        Product p = new Product();
        p.setId(1L);
        p.setCreated(Instant.now());
        Detail d = new Detail();
        d.setId(1L);
        d.setOwner(u);
        d.setCreated(Instant.now());
        d.setExpense(e);
        d.setAmount(BigDecimal.valueOf(50.00));
        MDetail md = new MDetail();
        md.setAmount(BigDecimal.valueOf(100.00));

        Mockito.when(detailRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;
        try {
            service.update(d, e2, null, p, md);
        } catch (Exception ex) {
            resp = ex;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_MISMATCH_EXPENSE);
    }

    @Test
    public void delete() {
        log.info("Test delete detail");

        Instant created = (Instant.now().minus(2, ChronoUnit.DAYS));
        Expense e = new Expense();
        Detail d = new Detail();
        d.setId(1L);
        d.setCreated(created);
        d.setExpense(e);

        Mockito.when(detailRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Detail resp = service.delete(d, e);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isNotNull();
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isNotNull();
        assertThat(resp.getExpense())
                .isNotNull();
    }

    @Test
    public void totalsByCategory() {
        log.info("Test get details' totals group by category");

        Expense e = new Expense();
        e.setId(1L);
        List<MGroupDetail> totals = new ArrayList<>();

        Mockito.when(statRepository.totalsByCategoryAndCurrency(any()))
                .thenReturn(totals);

        List<MGroupDetail> resp = service.totalsByCategory(e);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(0);
    }

    @Test
    public void searchByExpense() {
        log.info("Test search details by expense");

        Expense e = new Expense();
        e.setId(1L);

        List<Detail> details = new ArrayList<>();
        Pageable pageRequest = PageRequest.of(0, 10);

        Page<Detail> page = new PageImpl<>(details, pageRequest, 0);

        Mockito.when(detailRepository.search(any(Expense.class), any()))
                .thenReturn(page);

        Page<Detail> resp = service.search(e, pageRequest);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(0);
    }

    @Test
    public void searchByExpenseWithoutExpense() {
        log.info("Test search details by expense without expense");

        Expense e = null;

        List<Detail> details = new ArrayList<>();
        Pageable pageRequest = PageRequest.of(0, 10);

        Page<Detail> page = new PageImpl<>(details, pageRequest, 0);

        Mockito.when(detailRepository.search(any()))
                .thenReturn(page);

        Page<Detail> resp = service.search(e, pageRequest);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(0);
    }

    @Test
    public void searchByProduct() {
        log.info("Test search details by product");

        Product p = new Product();
        p.setId(1L);

        List<Detail> details = new ArrayList<>();
        Pageable pageRequest = PageRequest.of(0, 10);

        Page<Detail> page = new PageImpl<>(details, pageRequest, 0);

        Mockito.when(detailRepository.search(any(Product.class), any()))
                .thenReturn(page);

        Page<Detail> resp = service.search(p, pageRequest);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(0);
    }

    @Test
    public void searchByProductWithoutProduct() {
        log.info("Test search details by product without product");

        Product p = null;

        List<Detail> details = new ArrayList<>();
        Pageable pageRequest = PageRequest.of(0, 10);

        Page<Detail> page = new PageImpl<>(details, pageRequest, 0);

        Mockito.when(detailRepository.search(any()))
                .thenReturn(page);

        Page<Detail> resp = service.search(p, pageRequest);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(0);
    }

    @Test
    public void consolidateWithOnePage() {
        log.info("Test consolidate with one result page");

        Product p1 = new Product();
        p1.setId(1L);
        Product p2 = new Product();
        p2.setId(1L);

        Detail d1 = new Detail();
        d1.setId(1L);
        d1.setProduct(p1);

        Page<Detail> firstPage = new PageImpl<>(
                Collections.singletonList(d1),
                PageRequest.of(0, 2),
                1
        );

        Mockito.when(detailRepository.search(any(Product.class), any())).thenReturn(firstPage);

        service.consolidate(p2, p1);

        Mockito.verify(detailRepository, Mockito.times(1)).search(any(Product.class), any());
        Mockito.verify(detailRepository, Mockito.times(1)).saveAll(any());

        assertThat(d1.getProduct()).isEqualTo(p2);
    }

    @Test
    public void consolidateWithTwoResultPages() {
        log.info("Test consolidate with two result pages");

        Product p1 = new Product();
        p1.setId(1L);
        Product p2 = new Product();
        p2.setId(1L);

        Detail d1 = new Detail();
        d1.setId(1L);
        d1.setProduct(p1);
        Detail d2 = new Detail();
        d2.setId(2L);
        d2.setProduct(p1);
        Detail d3 = new Detail();
        d3.setId(3L);
        d3.setProduct(p1);

        Page<Detail> firstPage = new PageImpl<>(
                Arrays.asList(d1, d2),
                PageRequest.of(0, 2),
                3
        );
        Page<Detail> secondPage = new PageImpl<>(
                Collections.singletonList(d3),
                PageRequest.of(1, 2),
                3
        );

        Mockito.when(detailRepository.search(any(Product.class), any())).thenReturn(firstPage, secondPage);

        service.consolidate(p2, p1);

        Mockito.verify(detailRepository, Mockito.times(2)).search(any(Product.class), any());
        Mockito.verify(detailRepository, Mockito.times(2)).saveAll(any());

        assertThat(d1.getProduct()).isEqualTo(p2);
        assertThat(d2.getProduct()).isEqualTo(p2);
        assertThat(d3.getProduct()).isEqualTo(p2);
    }

    @Test
    public void buildModel() {
        log.info("Test build detail model");

        Instant created = (Instant.now().minus(7, ChronoUnit.DAYS));
        Instant updated = (Instant.now().minus(4, ChronoUnit.DAYS));
        Instant deleted = (Instant.now().minus(1, ChronoUnit.DAYS));
        Product p = new Product();
        p.setId(1L);
        Detail d = new Detail();
        d.setId(1L);
        d.setProduct(p);
        d.setCreated(created);
        d.setUpdated(updated);
        d.setDeleted(deleted);
        d.setAmount(BigDecimal.valueOf(100.00));
        d.setDiscount(BigDecimal.valueOf(10.00));
        d.setSubtotal(BigDecimal.valueOf(90.00));

        MProduct mp = new MProduct();
        mp.setId(1L);

        Mockito.when(productService.build(any(Product.class)))
                .thenReturn(mp);

        MDetail resp = service.build(d);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(1L);
        assertThat(resp.getCreated())
                .isCloseTo(created, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(updated, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isCloseTo(deleted, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getProduct())
                .isNotNull();
        assertThat(resp.getProduct().getId())
                .isEqualTo(1L);
        assertThat(resp.getAmount())
                .isEqualTo(BigDecimal.valueOf(100.00));
        assertThat(resp.getDiscount())
                .isEqualTo(BigDecimal.valueOf(10.00));
        assertThat(resp.getSubtotal())
                .isEqualTo(BigDecimal.valueOf(90.00));
    }

}
