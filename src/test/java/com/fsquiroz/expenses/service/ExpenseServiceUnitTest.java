package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.*;
import com.fsquiroz.expenses.entity.json.*;
import com.fsquiroz.expenses.exception.AppException;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.ErrorCode;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.repository.ExpenseRepository;
import com.fsquiroz.expenses.service.declaration.*;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.data.Offset;
import org.assertj.core.data.TemporalUnitWithinOffset;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.stubbing.answers.ReturnsArgumentAt;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class ExpenseServiceUnitTest {

    @MockBean
    private IBusinessService businessService;

    @MockBean
    private ICurrencyService currencyService;

    @MockBean
    private IExchangeRateService exchangeRateService;

    @MockBean
    private IDetailService detailService;

    @MockBean
    private IDiscountService discountService;

    @MockBean
    private ExpenseRepository expenseRepository;

    private ExpenseService service;

    @Before
    public void setup() {
        this.service = new ExpenseService(businessService, currencyService, exchangeRateService, detailService, discountService, expenseRepository);
    }

    @Test
    public void getById() {
        log.info("Test get expense by id");

        Expense e = new Expense();
        e.setId(1L);
        e.setStatus(ExpenseStatus.OPENED);
        e.setCreated(Instant.now());

        Mockito.when(expenseRepository.findById(any()))
                .thenReturn(Optional.of(e));

        Expense resp = service.get(1L);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(e.getId());
        assertThat(resp.getStatus())
                .isEqualTo(e.getStatus());
        assertThat(resp.getCreated())
                .isCloseTo(e.getCreated(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
    }

    @Test
    public void getNotFound() {
        log.info("Test get not found expense");

        Mockito.when(expenseRepository.findById(any()))
                .thenReturn(Optional.empty());

        Exception resp = null;
        try {
            service.get(1L);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(NotFoundException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.NF_BY_ID);
    }

    @Test
    public void getIdNull() {
        log.info("Test get with null id");

        Exception resp = null;
        try {
            service.get(null);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_INVALID_ID_VALUE);
    }

    @Test
    public void create() {
        log.info("Test create expense");

        Business b = new Business();
        Currency c = new Currency();
        User owner = new User();

        MExpense me = new MExpense();
        me.setRegistered((Instant.now().minus(2, ChronoUnit.DAYS)));

        Mockito.when(expenseRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Expense resp = service.create(me, b, c, null, owner);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getRegistered())
                .isCloseTo(me.getRegistered(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getStatus())
                .isEqualTo(ExpenseStatus.OPENED);
        assertThat(resp.getOwner())
                .isEqualTo(owner);
        assertThat(resp.getBusiness())
                .isEqualTo(b);
        assertThat(resp.getCurrency())
                .isEqualTo(c);
    }

    @Test
    public void createWithExchangeRate() {
        log.info("Test create expense with exchange rate");

        Business b = new Business();
        Currency c = new Currency();
        ExchangeRate er = new ExchangeRate();
        er.setFrom(c);
        User owner = new User();

        MExpense me = new MExpense();

        Mockito.when(expenseRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Expense resp = service.create(me, b, c, er, owner);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getRegistered())
                .isCloseTo(resp.getCreated(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getStatus())
                .isEqualTo(ExpenseStatus.OPENED);
        assertThat(resp.getOwner())
                .isEqualTo(owner);
        assertThat(resp.getBusiness())
                .isEqualTo(b);
        assertThat(resp.getCurrency())
                .isEqualTo(c);
        assertThat(resp.getExchangeRate())
                .isEqualTo(er);
    }

    @Test
    public void createWithInvalidExchangeRate() {
        log.info("Test create expense with invalid exchange rate");

        Business b = new Business();
        Currency c = new Currency();
        ExchangeRate er = new ExchangeRate();
        User owner = new User();

        MExpense me = new MExpense();

        Mockito.when(expenseRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Expense resp = service.create(me, b, c, er, owner);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getRegistered())
                .isCloseTo(resp.getCreated(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getStatus())
                .isEqualTo(ExpenseStatus.OPENED);
        assertThat(resp.getOwner())
                .isEqualTo(owner);
        assertThat(resp.getBusiness())
                .isEqualTo(b);
        assertThat(resp.getCurrency())
                .isEqualTo(c);
        assertThat(resp.getExchangeRate())
                .isNull();
    }

    @Test
    public void createWithDeletedBusiness() {
        log.info("Test create expense with deleted business");

        Business b = new Business();
        b.setDeleted(Instant.now());
        Currency c = new Currency();
        User owner = new User();

        MExpense me = new MExpense();

        Exception resp = null;
        try {
            service.create(me, b, c, null, owner);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_USING_DELETED);
    }

    @Test
    public void createWithDeletedCurrency() {
        log.info("Test create expense with deleted currency");

        Business b = new Business();
        Currency c = new Currency();
        c.setDeleted(Instant.now());
        User owner = new User();

        MExpense me = new MExpense();

        Exception resp = null;
        try {
            service.create(me, b, c, null, owner);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_USING_DELETED);
    }

    @Test
    public void createWithDeletedExchangeRate() {
        log.info("Test create expense with deleted exchange rate");

        Business b = new Business();
        Currency c = new Currency();
        ExchangeRate er = new ExchangeRate();
        er.setFrom(c);
        er.setDeleted(Instant.now());
        User owner = new User();

        MExpense me = new MExpense();

        Exception resp = null;
        try {
            service.create(me, b, c, er, owner);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_USING_DELETED);
    }

    @Test
    public void createWithNullBusiness() {
        log.info("Test create expense with null business");

        Business b = null;
        Currency c = new Currency();
        User owner = new User();

        MExpense me = new MExpense();

        Exception resp = null;
        try {
            service.create(me, b, c, null, owner);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_MISSING_PARAM);
    }

    @Test
    public void createWithNullCurrency() {
        log.info("Test create expense with null business");

        Business b = new Business();
        Currency c = null;
        User owner = new User();

        MExpense me = new MExpense();

        Exception resp = null;
        try {
            service.create(me, b, c, null, owner);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_MISSING_PARAM);
    }

    @Test
    public void repeat() {
        log.info("Test repeat expense");

        Business b = new Business();
        Currency c = new Currency();
        User owner = new User();
        Expense original = new Expense();
        original.setStatus(ExpenseStatus.CLOSED);

        MExpense me = new MExpense();
        me.setRegistered(Instant.now().minus(2, ChronoUnit.DAYS));

        Mockito.when(expenseRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Expense resp = service.repeat(me, original, b, c, null, owner);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getRegistered())
                .isCloseTo(me.getRegistered(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getStatus())
                .isEqualTo(ExpenseStatus.OPENED);
        assertThat(resp.getOwner())
                .isEqualTo(owner);
        assertThat(resp.getBusiness())
                .isEqualTo(b);
        assertThat(resp.getCurrency())
                .isEqualTo(c);
    }

    @Test
    public void repeatFromOpen() {
        log.info("Test repeat from open expense");

        Business b = new Business();
        Currency c = new Currency();
        User owner = new User();
        Expense original = new Expense();
        original.setStatus(ExpenseStatus.OPENED);

        MExpense me = new MExpense();
        me.setRegistered(Instant.now().minus(2, ChronoUnit.DAYS));

        Exception resp = null;

        try {
            service.repeat(me, original, b, c, null, owner);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_NOT_CLOSED);
    }

    @Test
    public void update() {
        log.info("Test update expense");

        Instant created = (Instant.now().minus(1, ChronoUnit.DAYS));
        Instant registered = created;
        Instant newRegistered = (registered.plus(1, ChronoUnit.HOURS));
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(created);
        e.setRegistered(registered);
        e.setStatus(ExpenseStatus.OPENED);
        Business b = new Business();
        Currency c = new Currency();

        MExpense me = new MExpense();
        me.setRegistered(newRegistered);

        Mockito.when(expenseRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Expense resp = service.update(e, b, c, null, me);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(1L);
        assertThat(resp.getCreated())
                .isCloseTo(created, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getRegistered())
                .isCloseTo(newRegistered, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getStatus())
                .isEqualTo(ExpenseStatus.OPENED);
        assertThat(resp.getBusiness())
                .isEqualTo(b);
        assertThat(resp.getCurrency())
                .isEqualTo(c);
    }

    @Test
    public void updateWithoutRegistered() {
        log.info("Test update expense without registered");

        Instant created = (Instant.now().minus(1, ChronoUnit.DAYS));
        Instant registered = created;
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(created);
        e.setRegistered(registered);
        e.setStatus(ExpenseStatus.OPENED);
        Business b = new Business();
        Currency c = new Currency();

        MExpense me = new MExpense();

        Mockito.when(expenseRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Expense resp = service.update(e, b, c, null, me);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(1L);
        assertThat(resp.getCreated())
                .isCloseTo(created, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getRegistered())
                .isCloseTo(registered, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getStatus())
                .isEqualTo(ExpenseStatus.OPENED);
        assertThat(resp.getBusiness())
                .isEqualTo(b);
        assertThat(resp.getCurrency())
                .isEqualTo(c);
    }

    @Test
    public void updateDeleted() {
        log.info("Test update deleted expense");

        Instant created = (Instant.now().minus(1, ChronoUnit.DAYS));
        Instant registered = created;
        Instant deleted = (created.plus(4, ChronoUnit.HOURS));
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(created);
        e.setRegistered(registered);
        e.setDeleted(deleted);
        e.setStatus(ExpenseStatus.OPENED);
        Business b = new Business();
        Currency c = new Currency();

        MExpense me = new MExpense();

        Exception resp = null;
        try {
            service.update(e, b, c, null, me);
        } catch (Exception ex) {
            resp = ex;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_EDIT_DELETED);
    }

    @Test
    public void updateNotOpened() {
        log.info("Test update not opened expense");

        Instant created = (Instant.now().minus(1, ChronoUnit.DAYS));
        Instant registered = created;
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(created);
        e.setRegistered(registered);
        e.setStatus(ExpenseStatus.CLOSED);
        Business b = new Business();
        Currency c = new Currency();

        MExpense me = new MExpense();

        Exception resp = null;
        try {
            service.update(e, b, c, null, me);
        } catch (Exception ex) {
            resp = ex;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_NOT_OPENED);
    }

    @Test
    public void updateStatus() {
        log.info("Test update status");

        Instant created = (Instant.now().minus(1, ChronoUnit.DAYS));
        Instant registered = created;
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(created);
        e.setRegistered(registered);
        e.setStatus(ExpenseStatus.OPENED);

        Mockito.when(expenseRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Expense resp = service.updateStatus(e, ExpenseStatus.CLOSED);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(1L);
        assertThat(resp.getCreated())
                .isCloseTo(created, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getRegistered())
                .isCloseTo(registered, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getStatus())
                .isEqualTo(ExpenseStatus.CLOSED);
    }

    @Test
    public void updateStatusToDeleted() {
        log.info("Test update status to deleted expense");

        Instant created = (Instant.now().minus(1, ChronoUnit.DAYS));
        Instant registered = created;
        Instant deleted = Instant.now();
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(created);
        e.setDeleted(deleted);
        e.setRegistered(registered);
        e.setStatus(ExpenseStatus.OPENED);

        Mockito.when(expenseRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;
        try {
            service.updateStatus(e, ExpenseStatus.CLOSED);
        } catch (Exception ex) {
            resp = ex;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_EDIT_DELETED);
    }

    @Test
    public void updateAmounts() {
        log.info("Test update amounts");

        Expense e = new Expense();

        Mockito.when(discountService.getTotal(any()))
                .thenReturn(BigDecimal.valueOf(10.0));

        service.updateAmounts(e);
    }

    @Test
    public void delete() {
        log.info("Test delete expense");

        Instant created = (Instant.now().minus(1, ChronoUnit.DAYS));
        Instant registered = created;
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(created);
        e.setRegistered(registered);
        e.setStatus(ExpenseStatus.OPENED);

        Mockito.when(expenseRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Expense resp = service.delete(e);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(1L);
        assertThat(resp.getCreated())
                .isCloseTo(created, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getRegistered())
                .isCloseTo(registered, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getStatus())
                .isEqualTo(ExpenseStatus.IGNORED);
    }

    @Test
    public void deleteDeleted() {
        log.info("Test delete deleted expense");

        Instant created = (Instant.now().minus(1, ChronoUnit.DAYS));
        Instant registered = created;
        Instant deleted = Instant.now();
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(created);
        e.setDeleted(deleted);
        e.setRegistered(registered);
        e.setStatus(ExpenseStatus.OPENED);

        Mockito.when(expenseRepository.save(any()))
                .thenAnswer(new ReturnsArgumentAt(0));

        Exception resp = null;
        try {
            service.delete(e);
        } catch (Exception ex) {
            resp = ex;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
        assertThat(((AppException) resp).getCode())
                .isEqualTo(ErrorCode.BR_DELETE_DELETED);
    }

    @Test
    public void search() {
        log.info("Test search expenses by user");

        Pageable pageRequest = PageRequest.of(0, 10);
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(Instant.now());
        e.setStatus(ExpenseStatus.OPENED);
        Business b = null;
        Currency c = null;
        User u = new User();

        Page<Expense> page = new PageImpl<>(Collections.singletonList(e), pageRequest, 1);

        Mockito.when(expenseRepository.searchByUser(any(), any()))
                .thenReturn(page);

        Page<Expense> resp = service.search(b, c, pageRequest, u);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(1);
        assertThat(resp)
                .contains(e);
    }

    @Test
    public void searchByCurrency() {
        log.info("Test search expenses by currency");

        Pageable pageRequest = PageRequest.of(0, 10);
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(Instant.now());
        e.setStatus(ExpenseStatus.OPENED);
        Business b = null;
        Currency c = new Currency();
        User u = new User();

        Page<Expense> page = new PageImpl<>(Collections.singletonList(e), pageRequest, 1);

        Mockito.when(expenseRepository.searchByCurrency(any(), any()))
                .thenReturn(page);

        Page<Expense> resp = service.search(b, c, pageRequest, u);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(1);
        assertThat(resp)
                .contains(e);
    }

    @Test
    public void searchByBusiness() {
        log.info("Test search expenses by business");

        Pageable pageRequest = PageRequest.of(0, 10);
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(Instant.now());
        e.setStatus(ExpenseStatus.OPENED);
        Business b = new Business();
        Currency c = null;
        User u = new User();

        Page<Expense> page = new PageImpl<>(Collections.singletonList(e), pageRequest, 1);

        Mockito.when(expenseRepository.searchByBusiness(any(), any()))
                .thenReturn(page);

        Page<Expense> resp = service.search(b, c, pageRequest, u);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(1);
        assertThat(resp)
                .contains(e);
    }

    @Test
    public void searchByBusinessAndCurrency() {
        log.info("Test search expenses by business and currency");

        Pageable pageRequest = PageRequest.of(0, 10);
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(Instant.now());
        e.setStatus(ExpenseStatus.OPENED);
        Business b = new Business();
        Currency c = new Currency();
        User u = new User();

        Page<Expense> page = new PageImpl<>(Collections.singletonList(e), pageRequest, 1);

        Mockito.when(expenseRepository.search(any(), any(), any()))
                .thenReturn(page);

        Page<Expense> resp = service.search(b, c, pageRequest, u);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(1);
        assertThat(resp)
                .contains(e);
    }

    @Test
    public void build() {
        log.info("Test build expense model");

        Instant created = (Instant.now().minus(7, ChronoUnit.DAYS));
        Instant updated = (created.plus(2, ChronoUnit.DAYS));
        Instant deleted = (created.plus(5, ChronoUnit.DAYS));
        Instant registered = (created.minus(2, ChronoUnit.DAYS));
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(created);
        e.setUpdated(updated);
        e.setDeleted(deleted);
        e.setRegistered(registered);
        e.setStatus(ExpenseStatus.OPENED);
        e.setBusiness(new Business());
        e.setCurrency(new Currency());
        e.setExchangeRate(new ExchangeRate());

        BigDecimal discounts = BigDecimal.valueOf(15);
        BigDecimal details = BigDecimal.valueOf(190);

        Mockito.when(businessService.build(any(Business.class)))
                .thenReturn(new MBusiness());
        Mockito.when(currencyService.build(any(Currency.class)))
                .thenReturn(new MCurrency());
        Mockito.when(exchangeRateService.build(any(ExchangeRate.class)))
                .thenReturn(new MExchangeRate());
        Mockito.when(discountService.getTotal(any()))
                .thenReturn(discounts);
        Mockito.when(detailService.getTotal(any()))
                .thenReturn(details);

        MExpense resp = service.build(e, false);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(1L);
        assertThat(resp.getCreated())
                .isCloseTo(created, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(updated, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isCloseTo(deleted, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getRegistered())
                .isCloseTo(registered, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getStatus())
                .isEqualTo(ExpenseStatus.OPENED);
        assertThat(resp.getDiscounts())
                .isCloseTo(BigDecimal.valueOf(15), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.getSubtotal())
                .isCloseTo(BigDecimal.valueOf(190), Offset.offset(BigDecimal.valueOf(0.005)));

        assertThat(resp.getBusiness())
                .isNotNull();
        assertThat(resp.getCurrency())
                .isNotNull();
        assertThat(resp.getExchangeRate())
                .isNotNull();
        assertThat(resp.getTotalByCategory())
                .isNull();
        assertThat(resp.getTotal())
                .isStrictlyBetween(BigDecimal.valueOf(174.99), BigDecimal.valueOf(175.01));
    }

    @Test
    public void buildExtended() {
        log.info("Test build expense model extended");

        Instant created = (Instant.now().minus(7, ChronoUnit.DAYS));
        Instant updated = (created.plus(2, ChronoUnit.DAYS));
        Instant deleted = (created.plus(5, ChronoUnit.DAYS));
        Instant registered = (created.minus(2, ChronoUnit.DAYS));
        MGroupDetail mgd = new MGroupDetail();
        mgd.setId(1L);
        Expense e = new Expense();
        e.setId(1L);
        e.setCreated(created);
        e.setUpdated(updated);
        e.setDeleted(deleted);
        e.setRegistered(registered);
        e.setStatus(ExpenseStatus.OPENED);
        e.setBusiness(new Business());
        e.setCurrency(new Currency());
        e.setExchangeRate(new ExchangeRate());

        BigDecimal discounts = BigDecimal.valueOf(15);
        BigDecimal details = BigDecimal.valueOf(190);

        Mockito.when(businessService.build(any(Business.class)))
                .thenReturn(new MBusiness());
        Mockito.when(currencyService.build(any(Currency.class)))
                .thenReturn(new MCurrency());
        Mockito.when(exchangeRateService.build(any(ExchangeRate.class)))
                .thenReturn(new MExchangeRate());
        Mockito.when(discountService.getTotal(any()))
                .thenReturn(discounts);
        Mockito.when(detailService.getTotal(any()))
                .thenReturn(details);
        Mockito.when(detailService.totalsByCategory(any()))
                .thenReturn(Collections.singletonList(mgd));

        MExpense resp = service.build(e, true);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(1L);
        assertThat(resp.getCreated())
                .isCloseTo(created, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(updated, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isCloseTo(deleted, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getRegistered())
                .isCloseTo(registered, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getStatus())
                .isEqualTo(ExpenseStatus.OPENED);
        assertThat(resp.getDiscounts())
                .isCloseTo(BigDecimal.valueOf(15), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.getSubtotal())
                .isCloseTo(BigDecimal.valueOf(190), Offset.offset(BigDecimal.valueOf(0.005)));
        assertThat(resp.getTotalByCategory())
                .isNotNull();
        assertThat(resp.getTotalByCategory())
                .hasSize(1);
        assertThat(resp.getTotalByCategory().get(0))
                .isNotNull();
        assertThat(resp.getTotalByCategory().get(0).getId())
                .isEqualTo(1L);

        assertThat(resp.getBusiness())
                .isNotNull();
        assertThat(resp.getCurrency())
                .isNotNull();
        assertThat(resp.getExchangeRate())
                .isNotNull();
        assertThat(resp.getTotal())
                .isStrictlyBetween(BigDecimal.valueOf(174.99), BigDecimal.valueOf(175.01));
    }

}
