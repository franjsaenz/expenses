package com.fsquiroz.expenses.service;

import com.fsquiroz.expenses.entity.db.Role;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.entity.json.MLogin;
import com.fsquiroz.expenses.entity.json.MResetPassword;
import com.fsquiroz.expenses.entity.json.MUser;
import com.fsquiroz.expenses.exception.BadRequestException;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.exception.UnauthorizedException;
import com.fsquiroz.expenses.repository.UserRepository;
import com.fsquiroz.expenses.security.SecurityCheck;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.data.TemporalUnitWithinOffset;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.stubbing.answers.ReturnsArgumentAt;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class UserServiceUnitTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private SecurityCheck securityCheck;

    @Test
    public void loginOkWithoutExpirationNorNotBeforeAsUser() {
        log.info("Test login ok without expiration nor notBefore as user");

        String token = "token";

        Instant supposedExpiration = offset(30, 0);

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.USER);
        user.setEmail("some@email.com");
        user.setPassword("123456");

        MLogin login = new MLogin("some@email.com", "123456");

        Mockito.when(userRepository.findByEmail(login.getEmail()))
                .thenReturn(Optional.of(user));
        Mockito.when(passwordEncoder.matches(login.getPassword(), user.getPassword()))
                .thenReturn(true);
        Mockito.when(securityCheck.generateToken(any(), any(), any(), any(User.class)))
                .thenReturn(token);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.login(login, null, null);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(user.getId());
        assertThat(resp.getToken())
                .isEqualTo(token);
        assertThat(resp.getExpiration())
                .isNull();
        assertThat(resp.getNotBefore())
                .isNull();
    }

    @Test
    public void loginOkWithoutExpirationNorNotBeforeAsSuper() {
        log.info("Test login ok without expiration nor notBefore as super");

        String token = "token";

        Instant supposedExpiration = offset(30, 0);

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.SUPER);
        user.setEmail("some@email.com");
        user.setPassword("123456");

        MLogin login = new MLogin("some@email.com", "123456");

        Mockito.when(userRepository.findByEmail(login.getEmail()))
                .thenReturn(Optional.of(user));
        Mockito.when(passwordEncoder.matches(login.getPassword(), user.getPassword()))
                .thenReturn(true);
        Mockito.when(securityCheck.generateToken(any(), any(), any(), any(User.class)))
                .thenReturn(token);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.login(login, null, null);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(user.getId());
        assertThat(resp.getToken())
                .isEqualTo(token);
        assertThat(resp.getExpiration())
                .isNotNull();
        assertThat(resp.getExpiration())
                .isCloseTo(supposedExpiration, new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getNotBefore())
                .isNull();
    }

    @Test
    public void loginOkWithExpirationWithoutNotBefore() {
        log.info("Test login ok with expiration, without notBefore");

        String token = "token";

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.USER);
        user.setEmail("some@email.com");
        user.setPassword("123456");

        MLogin login = new MLogin("some@email.com", "123456");
        Instant expiration = Instant.now();

        Mockito.when(userRepository.findByEmail(login.getEmail()))
                .thenReturn(Optional.of(user));
        Mockito.when(passwordEncoder.matches(login.getPassword(), user.getPassword()))
                .thenReturn(true);
        Mockito.when(securityCheck.generateToken(any(), any(), any(), any(User.class)))
                .thenReturn(token);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.login(login, expiration, null);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(user.getId());
        assertThat(resp.getToken())
                .isEqualTo(token);
        assertThat(resp.getExpiration())
                .isNotNull();
        assertThat(resp.getExpiration())
                .isCloseTo(expiration, new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getNotBefore())
                .isNull();
    }

    @Test
    public void loginOkWithoutExpirationWithNotBeforeAsUser() {
        log.info("Test login ok without expiration, with notBefore as user");

        String token = "token";

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.USER);
        user.setEmail("some@email.com");
        user.setPassword("123456");

        MLogin login = new MLogin("some@email.com", "123456");
        Instant notBefore = offset(0, 1);

        Mockito.when(userRepository.findByEmail(login.getEmail()))
                .thenReturn(Optional.of(user));
        Mockito.when(passwordEncoder.matches(login.getPassword(), user.getPassword()))
                .thenReturn(true);
        Mockito.when(securityCheck.generateToken(any(), any(), any(), any(User.class)))
                .thenReturn(token);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.login(login, null, notBefore);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(user.getId());
        assertThat(resp.getToken())
                .isEqualTo(token);
        assertThat(resp.getExpiration())
                .isNull();
        assertThat(resp.getExpiration())
                .isNull();
        assertThat(resp.getNotBefore())
                .isNotNull();
        assertThat(resp.getNotBefore())
                .isCloseTo(notBefore, new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
    }

    @Test
    public void loginOkWithoutExpirationWithNotBeforeAsSuper() {
        log.info("Test login ok without expiration, with notBefore as super");

        String token = "token";

        Instant supposedExpiration = offset(30, 1);

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.SUPER);
        user.setEmail("some@email.com");
        user.setPassword("123456");

        MLogin login = new MLogin("some@email.com", "123456");
        Instant notBefore = offset(0, 1);

        Mockito.when(userRepository.findByEmail(login.getEmail()))
                .thenReturn(Optional.of(user));
        Mockito.when(passwordEncoder.matches(login.getPassword(), user.getPassword()))
                .thenReturn(true);
        Mockito.when(securityCheck.generateToken(any(), any(), any(), any(User.class)))
                .thenReturn(token);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.login(login, null, notBefore);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(user.getId());
        assertThat(resp.getToken())
                .isEqualTo(token);
        assertThat(resp.getExpiration())
                .isNotNull();
        assertThat(resp.getExpiration())
                .isCloseTo(supposedExpiration, new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getNotBefore())
                .isNotNull();
        assertThat(resp.getNotBefore())
                .isCloseTo(notBefore, new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
    }

    @Test
    public void loginOkWithExpirationAndNotBefore() {
        log.info("Test login ok with expiration and notBefore");

        String token = "token";

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.USER);
        user.setEmail("some@email.com");
        user.setPassword("123456");

        MLogin login = new MLogin("some@email.com", "123456");
        Instant expiration = offset(0, 2);
        Instant notBefore = offset(0, 1);

        Mockito.when(userRepository.findByEmail(login.getEmail()))
                .thenReturn(Optional.of(user));
        Mockito.when(passwordEncoder.matches(login.getPassword(), user.getPassword()))
                .thenReturn(true);
        Mockito.when(securityCheck.generateToken(any(), any(), any(), any(User.class)))
                .thenReturn(token);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.login(login, expiration, notBefore);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(user.getId());
        assertThat(resp.getToken())
                .isEqualTo(token);
        assertThat(resp.getExpiration())
                .isNotNull();
        assertThat(resp.getExpiration())
                .isCloseTo(expiration, new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getNotBefore())
                .isNotNull();
        assertThat(resp.getNotBefore())
                .isCloseTo(notBefore, new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
    }

    @Test
    public void loginWithExpirationBeforeNotBefore() {
        log.info("Test login with expiration before notBefore");

        String token = "token";

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.USER);
        user.setEmail("some@email.com");
        user.setPassword("123456");

        MLogin login = new MLogin("some@email.com", "123456");
        Instant expiration = offset(0, 1);
        Instant notBefore = offset(0, 2);

        Mockito.when(userRepository.findByEmail(login.getEmail()))
                .thenReturn(Optional.of(user));
        Mockito.when(passwordEncoder.matches(login.getPassword(), user.getPassword()))
                .thenReturn(true);
        Mockito.when(securityCheck.generateToken(any(), any(), any(), any(User.class)))
                .thenReturn(token);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.login(login, expiration, notBefore);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void loginMissingEmail() {
        log.info("Test login without 'email' parameter");

        MLogin login = new MLogin(null, "123456");

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.login(login, null, null);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void loginMissingPassword() {
        log.info("Test login without 'password' parameter");

        MLogin login = new MLogin("some@email.com", null);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.login(login, null, null);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void loginEmailNotFound() {
        log.info("Test login with email not found");

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.USER);
        user.setEmail("some@email.com");
        user.setPassword("654321");

        MLogin login = new MLogin("some@email.com", "123456");

        Mockito.when(userRepository.findByEmail(any()))
                .thenReturn(Optional.of(user));
        Mockito.when(passwordEncoder.matches(any(), any()))
                .thenReturn(false);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.login(login, null, null);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void loginAsClient() {
        log.info("Test login as client");

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.CLIENT);
        user.setEmail("some@email.com");

        MLogin login = new MLogin("some@email.com", "123456");

        Mockito.when(userRepository.findByEmail(any()))
                .thenReturn(Optional.of(user));

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.login(login, null, null);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void loginWrongPassword() {
        log.info("Test login with wrong password");

        MLogin login = new MLogin("some@email.com", "123456");

        Mockito.when(userRepository.findByEmail(any()))
                .thenReturn(Optional.empty());

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.login(login, null, null);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void loginDeletedUser() {
        log.info("Test login with deleted user");

        User user = new User();
        user.setId(1L);
        user.setCreated(offset(0, -1));
        user.setDeleted(Instant.now());
        user.setRole(Role.USER);
        user.setEmail("some@email.com");
        user.setPassword("123456");

        MLogin login = new MLogin("some@email.com", "123456");

        Mockito.when(userRepository.findByEmail(login.getEmail()))
                .thenReturn(Optional.of(user));
        Mockito.when(passwordEncoder.matches(login.getPassword(), user.getPassword()))
                .thenReturn(true);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.login(login, null, null);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void getByEmailFound() {
        log.info("Test get user by valid email");

        User user = new User();
        user.setId(1L);
        user.setCreated(offset(0, -1));
        user.setDeleted(Instant.now());
        user.setRole(Role.USER);
        user.setEmail("some@email.com");
        user.setPassword("123456");

        Mockito.when(userRepository.findByEmail(user.getEmail()))
                .thenReturn(Optional.of(user));

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.getByEmail(user.getEmail());

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(user.getId());
    }

    @Test
    public void getByEmailNotFound() {
        log.info("Test get user by invalid email");

        Mockito.when(userRepository.findByEmail(any()))
                .thenReturn(Optional.empty());

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.getByEmail("some@email.com");
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    public void changePassword() {
        log.info("Test change user password");

        String oldPassword = "old";
        String newPassword = "new";
        String encodedPassword = "encoded";

        User user = new User();
        user.setId(1L);
        user.setCreated(offset(-10, 0));
        user.setRole(Role.USER);
        user.setEmail("some@email.com");
        user.setPassword(oldPassword);

        Mockito.when(userRepository.save(any()))
                .then(new ReturnsArgumentAt(0));
        Mockito.when(passwordEncoder.matches(any(), any()))
                .thenReturn(true);
        Mockito.when(passwordEncoder.encode(any()))
                .thenReturn(encodedPassword);

        MResetPassword resetPassword = new MResetPassword();
        resetPassword.setOldPassword(oldPassword);
        resetPassword.setNewPassword(newPassword);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.changePassword(user, resetPassword);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(user.getId());
        assertThat(resp.getPassword())
                .isNotNull();
        assertThat(resp.getPassword())
                .isEqualTo(encodedPassword);
        assertThat(resp.getUpdated())
                .isNotNull();
        assertThat(resp.getUpdated())
                .isAfter(user.getCreated());
    }

    @Test
    public void changePasswordInvalidOldPassword() {
        log.info("Test change user password");

        String oldPassword = "old";
        String newPassword = "new";

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.USER);
        user.setEmail("some@email.com");
        user.setPassword(oldPassword);

        Mockito.when(passwordEncoder.matches(any(), any()))
                .thenReturn(false);

        MResetPassword resetPassword = new MResetPassword();
        resetPassword.setOldPassword(oldPassword);
        resetPassword.setNewPassword(newPassword);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.changePassword(user, resetPassword);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void changePasswordClient() {
        log.info("Test change password of client user");

        String oldPassword = "old";
        String newPassword = "new";

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.CLIENT);
        user.setEmail("some@email.com");

        MResetPassword resetPassword = new MResetPassword();
        resetPassword.setOldPassword(oldPassword);
        resetPassword.setNewPassword(newPassword);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.changePassword(user, resetPassword);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void changePasswordWithoutOldPassword() {
        log.info("Test change password without 'oldPassword' parameter");

        String oldPassword = null;
        String newPassword = "new";

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.USER);
        user.setEmail("some@email.com");
        user.setPassword("encoded");

        MResetPassword resetPassword = new MResetPassword();
        resetPassword.setOldPassword(oldPassword);
        resetPassword.setNewPassword(newPassword);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.changePassword(user, resetPassword);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void changePasswordWithoutNewPassword() {
        log.info("Test change password without 'oldPassword' parameter");

        String oldPassword = "old";
        String newPassword = null;

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.USER);
        user.setEmail("some@email.com");
        user.setPassword("encoded");

        MResetPassword resetPassword = new MResetPassword();
        resetPassword.setOldPassword(oldPassword);
        resetPassword.setNewPassword(newPassword);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.changePassword(user, resetPassword);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void generateTokenForClient() {
        log.info("Test generate token for client");

        String token = "token";
        Instant expiration = offset(0, 2);
        Instant notBefore = offset(0, 1);

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.CLIENT);
        user.setEmail("some@email.com");
        user.setPassword("encoded");

        Mockito.when(securityCheck.generateToken(any(), any(), any(), any()))
                .thenReturn(token);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.generateToken(user, expiration, notBefore);
        assertThat(resp)
                .isNotNull();
        assertThat(resp.getToken())
                .isNotNull();
        assertThat(resp.getToken())
                .isEqualTo(token);
        assertThat(resp.getExpiration())
                .isNotNull();
        assertThat(resp.getExpiration())
                .isCloseTo(expiration, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getNotBefore())
                .isNotNull();
        assertThat(resp.getNotBefore())
                .isCloseTo(notBefore, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
    }

    @Test
    public void generateTokenForUser() {
        log.info("Test generate token for user");

        String token = "token";
        Instant expiration = offset(0, 2);
        Instant notBefore = offset(0, 1);

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.USER);
        user.setEmail("some@email.com");
        user.setPassword("encoded");

        Mockito.when(securityCheck.generateToken(any(), any(), any(), any()))
                .thenReturn(token);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.generateToken(user, expiration, notBefore);
        assertThat(resp)
                .isNotNull();
        assertThat(resp.getToken())
                .isNotNull();
        assertThat(resp.getToken())
                .isEqualTo(token);
        assertThat(resp.getExpiration())
                .isNotNull();
        assertThat(resp.getExpiration())
                .isCloseTo(expiration, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getNotBefore())
                .isNotNull();
        assertThat(resp.getNotBefore())
                .isCloseTo(notBefore, new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
    }

    @Test
    public void generateTokenForSuper() {
        log.info("Test generate token for super");

        String token = "token";
        Instant expiration = offset(0, 2);
        Instant notBefore = offset(0, 1);

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.SUPER);
        user.setEmail("some@email.com");
        user.setPassword("encoded");

        Mockito.when(securityCheck.generateToken(any(), any(), any(), any()))
                .thenReturn(token);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.generateToken(user, expiration, notBefore);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void generateTokenExpirationBeforeNotBefore() {
        log.info("Test generate token with expiration before notBefore");

        Instant expiration = offset(0, 1);
        Instant notBefore = offset(0, 2);

        User user = new User();
        user.setId(1L);
        user.setCreated(Instant.now());
        user.setRole(Role.USER);
        user.setEmail("some@email.com");
        user.setPassword("encoded");

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.generateToken(user, expiration, notBefore);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void buildUser() {
        log.info("Test build user json entity");

        User user = new User();
        user.setId(1L);
        user.setCreated(offset(0, -6));
        user.setUpdated(offset(0, -2));
        user.setDeleted(offset(0, -1));
        user.setRole(Role.USER);
        user.setEmail("some@email.com");
        user.setPassword("password");
        user.setToken("token");
        user.setExpiration(offset(0, 2));
        user.setNotBefore(offset(10, 0));

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        MUser resp = service.build(user);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(user.getId());
        assertThat(resp.getCreated())
                .isCloseTo(user.getCreated(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(user.getUpdated(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isCloseTo(user.getDeleted(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getRole())
                .isEqualTo(user.getRole());
        assertThat(resp.getEmail())
                .isEqualTo(user.getEmail());
        assertThat(resp.getPassword())
                .isNull();
        assertThat(resp.getToken())
                .isEqualTo(user.getToken());
        assertThat(resp.getExpiration())
                .isCloseTo(user.getExpiration(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getNotBefore())
                .isCloseTo(user.getNotBefore(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
    }

    @Test
    public void createUser() {
        log.info("Test create user");

        String encodedPassword = "encoded";

        MUser user = new MUser();
        user.setEmail("some@email.com");
        user.setPassword("password");
        user.setRole(Role.USER);
        user.setZoneId("UTC");

        Mockito.when(userRepository.findByEmail(any()))
                .thenReturn(Optional.empty());
        Mockito.when(userRepository.save(any()))
                .then(new ReturnsArgumentAt(0));
        Mockito.when(passwordEncoder.encode(any()))
                .thenReturn(encodedPassword);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.create(user);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getRole())
                .isEqualTo(user.getRole());
        assertThat(resp.getEmail())
                .isEqualTo(user.getEmail());
        assertThat(resp.getPassword())
                .isEqualTo(encodedPassword);
        assertThat(resp.getZoneId())
                .isEqualTo("UTC");
    }

    @Test
    public void createClient() {
        log.info("Test create client");

        String encodedPassword = "encoded";

        MUser user = new MUser();
        user.setEmail("some@email.com");
        user.setPassword("password");
        user.setRole(Role.CLIENT);
        user.setZoneId("invalid");

        Mockito.when(userRepository.findByEmail(any()))
                .thenReturn(Optional.empty());
        Mockito.when(userRepository.save(any()))
                .then(new ReturnsArgumentAt(0));
        Mockito.when(passwordEncoder.encode(any()))
                .thenReturn(encodedPassword);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.create(user);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getRole())
                .isEqualTo(user.getRole());
        assertThat(resp.getEmail())
                .isEqualTo(user.getEmail());
        assertThat(resp.getPassword())
                .isNull();
        assertThat(resp.getZoneId())
                .isEqualTo("Z");
    }

    @Test
    public void createSuper() {
        log.info("Test create super");

        MUser user = new MUser();
        user.setEmail("some@email.com");
        user.setPassword("password");
        user.setRole(Role.SUPER);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.create(user);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void createWithoutRole() {
        log.info("Test create user without role");

        MUser user = new MUser();
        user.setEmail("some@email.com");
        user.setPassword("password");
        user.setRole(null);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.create(user);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void createWithoutEmail() {
        log.info("Test create user without email");

        MUser user = new MUser();
        user.setEmail(null);
        user.setPassword("password");
        user.setRole(Role.USER);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.create(user);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void createUserWithoutPassword() {
        log.info("Test create user without password");

        MUser user = new MUser();
        user.setEmail("some@email.com");
        user.setPassword(null);
        user.setRole(Role.USER);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.create(user);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void createExistingUser() {
        log.info("Test create already existing user");

        MUser user = new MUser();
        user.setEmail("some@email.com");
        user.setPassword("password");
        user.setRole(Role.USER);

        User u = new User();
        u.setId(1L);

        Mockito.when(userRepository.findByEmail(any()))
                .thenReturn(Optional.of(u));

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.create(user);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void createDeletedUser() {
        log.info("Test create deleted user");

        String encodedPassword = "encoded";

        MUser user = new MUser();
        user.setEmail("some@email.com");
        user.setPassword("password");
        user.setRole(Role.USER);

        User u = new User();
        u.setId(1L);
        u.setCreated(offset(0, -24));
        u.setDeleted(offset(0, -12));
        u.setEmail("some@email.com");
        u.setRole(Role.USER);

        Mockito.when(userRepository.findByEmail(any()))
                .thenReturn(Optional.of(u));
        Mockito.when(userRepository.save(any()))
                .then(new ReturnsArgumentAt(0));
        Mockito.when(passwordEncoder.encode(any()))
                .thenReturn(encodedPassword);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.create(user);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(u.getCreated(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getRole())
                .isEqualTo(user.getRole());
        assertThat(resp.getEmail())
                .isEqualTo(user.getEmail());
        assertThat(resp.getPassword())
                .isEqualTo(encodedPassword);
    }

    @Test
    public void updateUser() {
        log.info("Test update user");

        User u = new User();
        u.setId(1L);
        u.setCreated(offset(0, -24));
        u.setEmail("some@email.com");
        u.setRole(Role.USER);

        MUser user = new MUser();
        user.setZoneId("UTC-03");

        Mockito.when(userRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.update(u, user);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(u.getCreated(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getZoneId())
                .isEqualTo("UTC-03:00");
    }

    @Test
    public void updateDeletedUser() {
        log.info("Test update deleted user");

        User u = new User();
        u.setId(1L);
        u.setCreated(offset(0, -24));
        u.setDeleted(offset(0, -1));
        u.setEmail("some@email.com");
        u.setRole(Role.USER);

        MUser user = new MUser();

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.update(u, user);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void deleteUser() {
        log.info("Test delete user");

        User u = new User();
        u.setId(1L);
        u.setCreated(offset(0, -24));
        u.setEmail("some@email.com");
        u.setRole(Role.USER);

        Mockito.when(userRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.delete(u);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(u.getCreated(), new TemporalUnitWithinOffset(10, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isCloseTo(Instant.now(), new TemporalUnitWithinOffset(100, ChronoUnit.MILLIS));
    }

    @Test
    public void deleteSuperUser() {
        log.info("Test delete super user");

        User u = new User();
        u.setId(1L);
        u.setCreated(offset(0, -24));
        u.setEmail("some@email.com");
        u.setRole(Role.SUPER);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.delete(u);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void deleteDeletedUser() {
        log.info("Test delete super user");

        User u = new User();
        u.setId(1L);
        u.setCreated(offset(0, -24));
        u.setDeleted(offset(0, -1));
        u.setEmail("some@email.com");
        u.setRole(Role.USER);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.delete(u);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void getUser() {
        log.info("Test get user by id");

        User u = new User();
        u.setId(1L);
        u.setCreated(offset(0, -24));
        u.setEmail("some@email.com");
        u.setRole(Role.USER);

        Mockito.when(userRepository.findById(any()))
                .thenReturn(Optional.of(u));

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        User resp = service.get(1L);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo(u.getId());
    }

    @Test
    public void getUserWithoutId() {
        log.info("Test get user without id");

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.get(null);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void getUserNotFound() {
        log.info("Test get user not found");

        Mockito.when(userRepository.findById(any()))
                .thenReturn(Optional.empty());

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Exception resp = null;
        try {
            service.get(1L);
        } catch (Exception e) {
            resp = e;
        }
        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    public void searchAllUsers() {
        log.info("Test search all users");

        User u1 = new User();
        u1.setId(1L);
        u1.setEmail("some@email.com");
        User u2 = new User();
        u2.setId(2L);
        u2.setEmail("other@email.com");
        List<User> users = Arrays.asList(u1, u2);
        Pageable pageReq = PageRequest.of(0, 10);
        Page<User> page = new PageImpl<>(users, pageReq, 2);

        Mockito.when(userRepository.findByDeletedIsNull(any()))
                .thenReturn(page);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Page<User> resp = service.search(null, pageReq);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(2);
        assertThat(resp)
                .contains(u1, u2);
    }

    @Test
    public void searchSomeUser() {
        log.info("Test search some user");

        User u1 = new User();
        u1.setId(1L);
        u1.setEmail("some@email.com");
        List<User> users = Collections.singletonList(u1);
        Pageable pageReq = PageRequest.of(0, 10);
        Page<User> page = new PageImpl<>(users, pageReq, 2);

        Mockito.when(userRepository.searchUsers(any(), any()))
                .thenReturn(page);

        UserService service = new UserService(securityCheck, passwordEncoder, userRepository);
        Page<User> resp = service.search("some", pageReq);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(1);
        assertThat(resp)
                .containsExactly(u1);
    }

    private Instant offset(int offsetMinutes, int offsetHours) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, offsetMinutes);
        c.add(Calendar.HOUR, offsetHours);
        return c.getTime().toInstant();
    }

}
