package com.fsquiroz.expenses.security;

import com.fsquiroz.expenses.entity.db.Business;
import com.fsquiroz.expenses.entity.db.Role;
import com.fsquiroz.expenses.entity.db.User;
import com.fsquiroz.expenses.exception.NotFoundException;
import com.fsquiroz.expenses.exception.UnauthorizedException;
import com.fsquiroz.expenses.repository.UserRepository;
import io.jsonwebtoken.JwtException;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.util.Calendar;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@TestPropertySource(locations = "classpath:application-test.properties")
@Slf4j
public class SecurityCheckTest {

    public static final String JWT_SECRET = "JWT_SECRET";

    @Test
    public void getUser() {
        log.info("Test get User");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");

        UserRepository repository = Mockito.mock(UserRepository.class);
        Mockito.when(repository.findById(u.getId())).thenReturn(Optional.of(u));

        SecurityCheck sc = new SecurityCheck(repository, JWT_SECRET);
        User response = sc.get(1L);

        Assert.assertNotNull(response);
        Assert.assertEquals(u, response);
    }

    @Test(expected = NotFoundException.class)
    public void getUserNotFound() {
        log.info("Test get User not found");


        UserRepository repository = Mockito.mock(UserRepository.class);
        Mockito.when(repository.findById(1L)).thenReturn(Optional.empty());

        SecurityCheck sc = new SecurityCheck(repository, JWT_SECRET);
        sc.get(1L);
    }

    @Test(expected = UnauthorizedException.class)
    public void getUserDeleted() {
        log.info("Test get User deleted");

        User u = new User();
        u.setId(1L);
        u.setDeleted(Instant.now());
        u.setEmail("some@email.com");

        UserRepository repository = Mockito.mock(UserRepository.class);
        Mockito.when(repository.findById(u.getId())).thenReturn(Optional.of(u));

        SecurityCheck sc = new SecurityCheck(repository, JWT_SECRET);
        sc.get(1L);
    }

    @Test
    public void authorized() {
        log.info("Test authorized with token");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");

        User issuer = new User();
        issuer.setId(2L);
        issuer.setEmail("issuer@email.com");

        UserRepository repository = Mockito.mock(UserRepository.class);
        Mockito.when(repository.findById(u.getId())).thenReturn(Optional.of(u));

        SecurityCheck sc = new SecurityCheck(repository, JWT_SECRET);
        String token = sc.generateToken(issuer, null, null, u);
        User response = sc.authenticate(token);

        Assert.assertNotNull(response);
        Assert.assertEquals(u, response);
    }

    @Test
    public void authorizedNotExpired() {
        log.info("Test authorized with token not expired");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        Instant future = date(1);

        UserRepository repository = Mockito.mock(UserRepository.class);
        Mockito.when(repository.findById(u.getId())).thenReturn(Optional.of(u));

        SecurityCheck sc = new SecurityCheck(repository, JWT_SECRET);
        String token = sc.generateToken(null, future, null, u);
        User response = sc.authenticate(token);

        Assert.assertNotNull(response);
        Assert.assertEquals(u, response);
    }

    @Test(expected = JwtException.class)
    public void authorizedExpired() {
        log.info("Test authorized with token expired");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        Instant past = date(-1);

        UserRepository repository = Mockito.mock(UserRepository.class);
        Mockito.when(repository.findById(u.getId())).thenReturn(Optional.of(u));

        SecurityCheck sc = new SecurityCheck(repository, JWT_SECRET);
        String token = sc.generateToken(null, past, null, u);
        sc.authenticate(token);
    }

    @Test(expected = JwtException.class)
    public void authorizedNotReady() {
        log.info("Test authorized with token not ready");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        Instant future = date(1);

        UserRepository repository = Mockito.mock(UserRepository.class);
        Mockito.when(repository.findById(u.getId())).thenReturn(Optional.of(u));

        SecurityCheck sc = new SecurityCheck(repository, JWT_SECRET);
        String token = sc.generateToken(null, null, future, u);
        User response = sc.authenticate(token);

        Assert.assertNotNull(response);
        Assert.assertEquals(u, response);
    }

    @Test
    public void authorizedReady() {
        log.info("Test authorized with token ready");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        Instant past = date(-1);

        UserRepository repository = Mockito.mock(UserRepository.class);
        Mockito.when(repository.findById(u.getId())).thenReturn(Optional.of(u));

        SecurityCheck sc = new SecurityCheck(repository, JWT_SECRET);
        String token = sc.generateToken(null, null, past, u);
        sc.authenticate(token);
    }

    @Test
    public void getAuthentication() {
        log.info("Test get authenticated user");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(u, null, u.getAuthorities()));

        SecurityCheck sc = new SecurityCheck(null, null);
        User response = sc.getAuthentication();

        Assert.assertNotNull(response);
        Assert.assertEquals(u, response);
    }

    @Test(expected = UnauthorizedException.class)
    public void getNotAuthenticationUser() {
        log.info("Test get anonymous user");

        SecurityContextHolder.clearContext();

        SecurityCheck sc = new SecurityCheck(null, null);
        sc.getAuthentication();
    }

    @Test
    public void canAccessAsOwner() {
        log.info("Test can access a filterable object as owner");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(u, null, u.getAuthorities()));

        Business b = new Business();
        b.setId(1L);
        b.setCreated(Instant.now());
        b.setName("Test name");
        b.setOwner(u);

        SecurityCheck sc = new SecurityCheck(null, null);
        boolean resp = sc.canAccess(b);
        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canAccessAsSuper() {
        log.info("Test can access a filterable object as super");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        u.setRole(Role.SUPER);
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(u, null, u.getAuthorities()));

        User owner = new User();
        owner.setId(2L);
        owner.setCreated(Instant.now());
        owner.setEmail("owner@email.com");
        owner.setRole(Role.USER);

        Business b = new Business();
        b.setId(1L);
        b.setCreated(Instant.now());
        b.setName("Test name");
        b.setOwner(owner);

        SecurityCheck sc = new SecurityCheck(null, null);
        boolean resp = sc.canAccess(b);
        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canAccessAsOther() {
        log.info("Test can access a filterable object as other");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        u.setRole(Role.USER);
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(u, null, u.getAuthorities()));

        User owner = new User();
        owner.setId(2L);
        owner.setCreated(Instant.now());
        owner.setEmail("owner@email.com");
        owner.setRole(Role.USER);

        Business b = new Business();
        b.setId(1L);
        b.setCreated(Instant.now());
        b.setName("Test name");
        b.setOwner(owner);

        SecurityCheck sc = new SecurityCheck(null, null);
        boolean resp = sc.canAccess(b);
        assertThat(resp)
                .isFalse();
    }

    @Test
    public void canAccessAsAnonymous() {
        log.info("Test can access a filterable object as anonymous");

        User owner = new User();
        owner.setId(2L);
        owner.setCreated(Instant.now());
        owner.setEmail("owner@email.com");
        owner.setRole(Role.USER);

        Business b = new Business();
        b.setId(1L);
        b.setCreated(Instant.now());
        b.setName("Test name");
        b.setOwner(owner);

        SecurityCheck sc = new SecurityCheck(null, null);
        boolean resp = sc.canAccess(b);
        assertThat(resp)
                .isFalse();
    }

    @Test
    public void canAccessToNull() {
        log.info("Test can access to a null filterable object as other");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        u.setRole(Role.USER);
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(u, null, u.getAuthorities()));

        SecurityCheck sc = new SecurityCheck(null, null);
        boolean resp = sc.canAccess(null);
        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canAccessToOrphan() {
        log.info("Test can access to an orphan filterable object");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        u.setRole(Role.USER);
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(u, null, u.getAuthorities()));

        Business b = new Business();
        b.setId(1L);
        b.setCreated(Instant.now());
        b.setName("Test name");
        b.setOwner(null);

        SecurityCheck sc = new SecurityCheck(null, null);
        boolean resp = sc.canAccess(b);
        assertThat(resp)
                .isFalse();
    }

    @Test
    public void canCreateAsSuper() {
        log.info("Test can create as super");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        u.setRole(Role.SUPER);
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(u, null, u.getAuthorities()));

        SecurityCheck sc = new SecurityCheck(null, null);
        boolean resp = sc.canCreate("resource");
        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canCreateAsUser() {
        log.info("Test can create as user");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        u.setRole(Role.USER);
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(u, null, u.getAuthorities()));

        SecurityCheck sc = new SecurityCheck(null, null);
        boolean resp = sc.canCreate("resource");
        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canCreateAsClient() {
        log.info("Test can create as client");

        User u = new User();
        u.setId(1L);
        u.setEmail("some@email.com");
        u.setRole(Role.CLIENT);
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(u, null, u.getAuthorities()));

        SecurityCheck sc = new SecurityCheck(null, null);
        boolean resp = sc.canCreate("resource");
        assertThat(resp)
                .isFalse();
    }

    @Test
    public void canCreateAsAnonymous() {
        log.info("Test can create as anonymous");

        SecurityCheck sc = new SecurityCheck(null, null);
        boolean resp = sc.canCreate("resource");
        assertThat(resp)
                .isFalse();
    }

    private Instant date(int dayVariation) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_YEAR, dayVariation);
        return c.getTime().toInstant();
    }

}
