# Expenses

## Introduction

Application intended to help keep tracking of expenses

Implemented as a REST service, exposes functionality through http endpoints based on JSON format interchange.

## Database

This project use MySQL for runtime and an embedded H2 database for test.

## Documentation

This project uses SwaggerUI to facilitate endpoints. The documentation can be seen while project is running in `/swagger-ui.html`

## Getting started

First, we need the application deployed in some environment, we will assume is under `http://localhost:8080`.
We can use the embedded documentation in `http://localhost:8080/swagger-ui.html` or some other http tool such as cUrl in order to perform the actions.

### Dependencies

It is required for the normal execution of this project the placement of the application properties file. On startup, the file is search under `${user.dir}/.expenses/application.properties`, for example: `/home/name/.expenses/application.properties`. The fields required in such file are:

    - spring.jpa.hibernate.ddl-auto [create-drop|create|none|update|valdiate]: Indicates if on each startup the database schema shuld be created from scrach, validated, updated, etc. Recomended: update
    - spring.datasource.url [jdbc:url]: Url in jdbc format to the database, with protocol, address, port and schema name
    - spring.datasource.username [string]: Username for the database connection
    - spring.datasource.password [string]: Password for the database connection
    - security.jwt.secret [string]: Secret key to encrypt the authentication token

Example of `application.properties` file:

```
spring.jpa.hibernate.ddl-auto=none
spring.datasource.url=jdbc:mysql://localhost:3306/expenses
spring.datasource.username=expenses
spring.datasource.password=some-strong-password
security.jwt.secret=some-strong-secret-key
```

### Super User

On first execution, a user with SUPER role is created with the following credentials:

    - email: super@expenses.com
    - password: 123456

This user is intended to bootstrap the use of this application